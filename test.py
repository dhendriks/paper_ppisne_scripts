import subprocess

download_command = "pip freeze".split(" ")
p = subprocess.run(download_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
stdout = p.stdout  # stdout = normal output
stderr = p.stderr  # stderr = error output

if p.returncode != 0:
    print("Something went wrong when executing the makefile:")
    print(stderr.decode("utf-8"))
    print("Aborting")
    sys.exit(-1)

else:
    print(stdout.decode("utf-8"))
