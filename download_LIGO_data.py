"""
Routine to download the file from the LIGO dataset to plot the primary mass distribution data
https://zenodo.org/records/5655785
https://github.com/dvolgyes/zenodo_get

STEPS:
- download whole tar file

- remove rest of the tar file
"""

import os
import subprocess
import tarfile
import shutil
import sys

def extract_from_tar(tar_filename, source_filename, target_dir):
    """
    Function to extract specific file from tar file
    """

    with tarfile.open(tar_filename, "r") as tar:
        try:

            # for member in tar.getmembers():
            #     if member.isreg() and os.path.basename(member.name) == source_filename:  # skip if the TarInfo is not files
            #         member.name = os.path.basename(member.name) # remove the path by reset it
            #         print(member.name)
            #         tar.extract(member, output_dir) # extract 

            tar.extract(source_filename, target_filename)
        except KeyError:
            print(f"Warning: File '{source_filename}' not found in the tar archive.")


#
GW_DATA_ROOT = os.path.join(os.getenv("paper_PPISNe_Hendriks2023_data_dir"), 'GW_LIGO_OBS_DATA')
os.makedirs(GW_DATA_ROOT, exist_ok=True)

#
download_tar = True
extract_file = True
cleanup_after = True

#
tar_filename = "/home/david/data_projects/datafiles/GW/GWTC-3-population-data.tar.gz"
source_filename = "o1o2o3_mass_c_iid_mag_iid_tilt_powerlaw_redshift_mass_data.h5"
target_filename = os.path.join(GW_DATA_ROOT, source_filename)

#
if os.path.isfile(target_filename):
    print("Target file {} already exists!".format(target_filename))
    exit()

######################
# Handle downloading tar if it doesnt exist
if download_tar:
    print("Downloading GW data tar. This likely will take long so please be patient")

    #
    download_command = "zenodo_get --record=5655785 --output-dir={GW_DATA_ROOT}".format(
        GW_DATA_ROOT=GW_DATA_ROOT
    ).split()

    #
    p = subprocess.run(download_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    stdout = p.stdout  # stdout = normal output
    stderr = p.stderr  # stderr = error output

    if p.returncode != 0:
        print("Something went wrong when executing the makefile:")
        print(stderr.decode("utf-8"))
        print("Aborting")
        sys.exit(-1)

    else:
        print(stdout.decode("utf-8"))


######################
# Handle extracting the file from the tar
if extract_file:
    #
    print("Extracting {} from tar file {}".format(source_filename, tar_filename))
    extract_from_tar(
        tar_filename,
        source_filename="GWTC-3-population-data/analyses/PowerLawPeak/"
        + source_filename,
        target_dir=GW_DATA_ROOT,
    )
    print(
        "Extracted {} from tar file {} to {}".format(
            source_filename, tar_filename, target_filename
        )
    )

    # rename dir
    os.rename(os.path.join(GW_DATA_ROOT, source_filename), os.path.join(GW_DATA_ROOT, source_filename+"_tmp"))

    #
    os.rename(os.path.join(GW_DATA_ROOT, source_filename+"_tmp", "GWTC-3-population-data/analyses/PowerLawPeak", source_filename), os.path.join(GW_DATA_ROOT, source_filename))

    # 
    shutil.rmtree(os.path.join(GW_DATA_ROOT, source_filename+"_tmp"))

    # # delete directory with same name
    # for filename in os.listdir(GW_DATA_ROOT):
    #     if (filename == source_filename+"_tmp") and os.path.isdir(os.path.join(GW_DATA_ROOT, source_filename+"_tmp")):
    #         shutil.rmtree(os.path.join(GW_DATA_ROOT, source_filename+"_tmp"))

######################
# Handle deleting the tar
if cleanup_after:
    for filename in os.listdir(GW_DATA_ROOT):
        if not filename == source_filename:
            full_filename = os.path.join(GW_DATA_ROOT, filename)
            print("Removing {}".format(full_filename))
            os.remove(full_filename)
