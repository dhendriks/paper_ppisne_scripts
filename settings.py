"""
Global configuration script for the routines to generate the data and figures
"""

COshift = r"$\Delta \it{M}_{\mathrm{PPI,\,CO\,shift}}$"
extraML = r"$\Delta \it{M}_{\mathrm{PPI,\,extra\,ML}}$"
Msun = r"$\mathrm{M}_{\odot}$"
Farmer_citation = r"Farmer et al. (2019)"


def format_value(value):
    """
    Function to format the value
    """

    #
    if value == 0:
        raise ValueError("not a valid value (this is the fiducial model!)")

    # Check if the number is actually integer
    if value % 1.0 == 0.0:
        value = int(value)

    # set up format for the number
    if isinstance(value, float):
        value_format = ("{0:+.1f}" if value > 0 else "{0:.1f}").format(value)
    elif isinstance(value, int):
        value_format = ("{0:+d}" if value > 0 else "{0:d}").format(value)
    else:
        raise ValueError("Cant handle current type ()", type(value))

    #
    value_string = r"$=\,{value_format}\,$".format(value_format=value_format)

    return value_string


def COshift_variation_text(value, post_string="", pre_string=""):
    """
    Function to return the COshift variation text
    """

    value_string = format_value(value=value)

    #
    COshift_variation_string = COshift + value_string + Msun

    return COshift_variation_string


def format_variation(value, variation, pre_string="", post_string=""):
    """
    General Function to handle the formatting of the variations
    """

    # Get value string
    value_string = format_value(value=value)

    #
    variation_string = variation + value_string + Msun

    # append stuff
    variation_string = pre_string + variation_string + post_string

    return variation_string


from functools import partial

format_COshift = partial(format_variation, variation=COshift)
format_extraML = partial(format_variation, variation=extraML)


SN_type_dict = {
    0: "None",
    1: "IA_He",
    2: "IA_ELD",
    3: "IA_CHAND",
    4: "AIC",
    5: "ECAP",
    6: "IA_He_Coal",
    7: "IA_CHAND_Coal",
    8: "NS_NS",
    9: "GRB_COLLAPSAR",
    10: "HeStarIa",
    11: "IBC",
    12: "II",
    13: "IIa",
    14: "WDKICK",
    15: "TZ",
    16: "AIC_BH",
    17: "BH_BH",
    18: "BH_NS",
    19: "IA_Hybrid_HeCOWD",
    20: "IA_Hybrid_HeCOWD_subluminous",
    21: "PPISN",
    22: "PISN",
    23: "PHDIS",
}


SN_type_dict_new = {
    0: "None",
    1: "IA_He",
    2: "IA_ELD",
    3: "IA_CHAND",
    4: "SubChandIa_CO",
    5: "SubChandIa_He",
    6: "AIC",
    7: "ECAP",
    8: "IA_He_Coal",
    9: "IA_CHAND_Coal",
    10: "NS_NS",
    11: "GRB_COLLAPSAR",
    12: "HeStarIa",
    13: "IBC",
    14: "II",
    15: "IIa",
    16: "WDKICK",
    17: "TZ",
    18: "AIC_BH",
    19: "BH_BH",
    20: "BH_NS",
    21: "IA_Hybrid_HeCOWD",
    22: "IA_Hybrid_HeCOWD_subluminous",
    23: "IA_Violent",
    24: "PPISN",
    25: "PISN",
    26: "PHDIS",
}
