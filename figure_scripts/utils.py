import inspect
import os

import astropy.units as u
import h5py
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
import numpy as np

#
LINESTYLE_TUPLE = [
    ("solid", "solid"),  # Same as (0, ()) or '-'
    ("dotted", "dotted"),  # Same as (0, (1, 1)) or '.'
    ("dashed", "dashed"),  # Same as '--'
    ("dashdot", "dashdot"),  # Same as '-.'
    # ('loosely dotted',        (0, (1, 10))),
    # ('dotted',                (0, (1, 1))),
    ("densely dotted", (0, (1, 1))),
    ("loosely dashed", (0, (5, 10))),
    # ('dashed',                (0, (5, 5))),
    ("densely dashed", (0, (5, 1))),
    ("loosely dashdotted", (0, (3, 10, 1, 10))),
    # ('dashdotted',            (0, (3, 5, 1, 5))),
    ("densely dashdotted", (0, (3, 1, 1, 1))),
    ("dashdotdotted", (0, (3, 5, 1, 5, 1, 5))),
    ("loosely dashdotdotted", (0, (3, 10, 1, 10, 1, 10))),
    ("densely dashdotdotted", (0, (3, 1, 1, 1, 1, 1))),
    ("solid", "solid"),  # Same as (0, ()) or '-'
    ("dotted", "dotted"),  # Same as (0, (1, 1)) or '.'
    ("dashed", "dashed"),  # Same as '--'
    ("dashdot", "dashdot"),  # Same as '-.'
]

color_list = ["blue", "orange", "green", "yellow", "pink", "red"]
hatch_list = ["/", "\\", "|", "-", "+", "x", "o", "O", ".", "*"]
linestyle_list = [linestyle_el[1] for linestyle_el in LINESTYLE_TUPLE]


def general_handle_figure_name_function(
    output_dir,
    basename,
    figure_counter,
    figure_format,
    omit_basename,
    prepend_figure_counter,
):
    """
    General function to handle the naming of the figure
    """

    ###################
    # Handle checks
    if omit_basename and (not prepend_figure_counter):
        raise ValueError("Can't omit basename and not prepend figure counter!")
    allowed_formats = ["pdf", "eps", "tiff", "png", "jpg"]
    if figure_format not in allowed_formats:
        raise ValueError(
            "figure_format has to be one of the following: {}".format(allowed_formats)
        )

    ###################
    # Construct new basename
    figure_counter_string = "Fig_{}{}".format(
        figure_counter, "_" if not omit_basename else ""
    )

    new_basename = ""

    if prepend_figure_counter:
        new_basename += figure_counter_string
    if not omit_basename:
        new_basename += basename
    new_basename += ".{}".format(figure_format)
    print("Handling {}".format(new_basename))

    # Construct full path
    figure_output_name = os.path.join(output_dir, new_basename)

    return figure_output_name


def show_and_save_plot(fig, plot_settings, verbose=1):
    """
    Wrapper to handle the saving and the showing of the plots
    """

    if plot_settings.get("show_plot", False):
        plt.show(block=plot_settings.get("block", True))
    else:
        plt.ioff()

    if plot_settings.get("output_name", None):
        if os.path.dirname(plot_settings["output_name"]):
            if verbose:
                print(
                    "creating output directory {}".format(
                        os.path.dirname(plot_settings["output_name"])
                    )
                )
            os.makedirs(os.path.dirname(plot_settings["output_name"]), exist_ok=True)
        fig.savefig(plot_settings["output_name"], bbox_inches="tight")

        try:
            caller_name = inspect.currentframe().f_back.f_code.co_name
        except:
            caller_name = ""
        if verbose:
            print(
                "{}wrote plot to {}".format(
                    "{}: ".format(caller_name) if caller_name else "",
                    plot_settings["output_name"],
                )
            )

    # Stuff to close and handle the cleaning of the garbage
    plt.close(fig)
    plt.close("all")


def add_labels_subplots(fig, axes_list, label_function_kwargs, custom_locs=None):
    """
    Function to loop over a list of axes of a figure and add subplots
    """

    # Add subplot labels
    for ax_i, ax in enumerate(axes_list):
        panel_label_function_kwargs = label_function_kwargs
        # Custom per-panel placement
        if custom_locs is not None:
            panel_label_function_kwargs = {
                **panel_label_function_kwargs,
                "x_loc": custom_locs[ax_i]["x"],
                "y_loc": custom_locs[ax_i]["y"],
            }

        add_label_subplot(fig=fig, ax=ax, label_i=ax_i, **panel_label_function_kwargs)


def add_label_subplot(
    fig,
    ax,
    label_i,
    x_loc=0.95,
    y_loc=0.95,
    fontsize=24,
    verticalalignment="top",
    bbox_dict={"pad": 0.5, "facecolor": "None", "boxstyle": "round"},
):
    """
    Function to add a label to the subplot
    """

    subplot_labels = "abcdefghijklmnopqrstuvwxyz"

    trans = mtransforms.ScaledTranslation(10 / 72, -5 / 72, fig.dpi_scale_trans)
    ax.text(
        x_loc,
        y_loc,
        subplot_labels[label_i],
        transform=ax.transAxes + trans,
        fontsize=fontsize,
        verticalalignment=verticalalignment,
        bbox=dict(**bbox_dict),
        zorder=500,
    )

    return fig, ax


def load_mpl_rc():
    import matplotlib as mpl

    # https://matplotlib.org/users/customizing.html
    mpl.rc(
        "axes",
        labelweight="normal",
        linewidth=2,
        labelsize=30,
        grid=True,
        titlesize=40,
        facecolor="white",
    )

    mpl.rc("savefig", dpi=100)

    mpl.rc("lines", linewidth=4, color="g", markeredgewidth=2)

    mpl.rc(
        "ytick",
        **{
            "labelsize": 30,
            "color": "k",
            "left": True,
            "right": True,
            "major.size": 12,
            "major.width": 2,
            "minor.size": 6,
            "minor.width": 2,
            "major.pad": 12,
            "minor.visible": True,
            "direction": "inout",
        }
    )

    mpl.rc(
        "xtick",
        **{
            "labelsize": 30,
            "top": True,
            "bottom": True,
            "major.size": 12,
            "major.width": 2,
            "minor.size": 6,
            "minor.width": 2,
            "major.pad": 12,
            "minor.visible": True,
            "direction": "inout",
        }
    )

    mpl.rc("legend", frameon=False, fontsize=30, title_fontsize=30)

    mpl.rc("contour", negative_linestyle="solid")

    mpl.rc(
        "figure",
        figsize=[16, 16],
        titlesize=30,
        dpi=100,
        facecolor="white",
        edgecolor="white",
        frameon=True,
        max_open_warning=10,
        # autolayout=True
    )

    mpl.rc(
        "legend",
        fontsize=20,
        handlelength=2,
        loc="best",
        fancybox=False,
        numpoints=2,
        framealpha=None,
        scatterpoints=3,
        edgecolor="inherit",
    )

    mpl.rc("savefig", dpi="figure", facecolor="white", edgecolor="white")

    mpl.rc("grid", color="b0b0b0", alpha=0.5)

    mpl.rc("image", cmap="viridis")

    mpl.rc(
        "font",
        # weight='bold',
        serif="Palatino",
        size=20,
    )

    mpl.rc("errorbar", capsize=2)

    mpl.rc("mathtext", default="sf")


def create_bins_from_centers(centers):
    """
    Function to create a set of bin edges from a set of bin centers. Assumes the two endpoints have the same binwidth as their neighbours
    """

    # Create bin edges minus the outer two
    bin_edges = (centers[1:] + centers[:-1]) / 2

    # Add to left
    bin_edges = np.append(np.array(bin_edges[0] - np.diff(centers)[0]), bin_edges)

    # Add to right
    bin_edges = np.append(bin_edges, np.array(bin_edges[-1] + np.diff(centers)[-1]))

    return bin_edges


def handle_columns(combined_df, quantity, querylist, extra_columns=[]):
    """
    Function to handle adding the columns
    """

    used_columns = (
        [quantity] + extract_columns_from_querylist(querylist) + extra_columns
    )
    columns_to_add = [
        column for column in used_columns if column not in combined_df.columns
    ]
    combined_df = add_columns_to_df(combined_df, columns_to_add)

    return combined_df


def extract_columns_from_query(query):
    """
    Function to extract column names from the query
    """

    # dict with old and new values
    replace_dict = {
        "=": "",
        ")": "",
        "(": "",
        "&": "",
        "<": "",
        ">": "",
    }

    # Return empty list if there is no query
    if query is None:
        return []

    # replace values
    for old, new in replace_dict.items():
        query = query.replace(old, new)

    # remove the numbers
    columns = [el for el in query.split() if not isfloat(el)]

    #
    return columns


def extract_columns_from_querylist(querylist):
    """
    Wrapper function for extract_columns_from_query to handle a list of queries
    """

    #
    columns = []

    #
    if querylist is None:
        return columns

    #
    for querydict in querylist:
        if querydict  is not None:
            columns += extract_columns_from_query(querydict["query"])

    #
    return columns


###############
# Functions to add columns to the dataframes
def add_columns_to_df(df, columns):
    """
    Function to add columns to the dataframe

    requires several columns to be present already
    """

    # loop over all columns
    for column in columns:
        if column is not None:
            if column in column_add_function_dict.keys():
                df = column_add_function_dict[column](df)
            else:
                raise ValueError(
                    "No function available to add the column {}".format(column)
                )

    return df


# specific functions
def add_primary_mass(df):
    """
    Function to add the primary mass column
    """

    if "primary_mass" in df.columns:
        return df

    df["primary_mass"] = df[["mass_1", "mass_2"]].max(axis=1)

    return df


def add_secondary_mass(df):
    """
    Function to add the secondary mass column
    """

    if "secondary_mass" in df.columns:
        return df

    df["secondary_mass"] = df[["mass_1", "mass_2"]].min(axis=1)

    return df


def add_total_mass(df):
    """
    Function to add total mass
    """

    if "total_mass" in df.columns:
        return df

    df["total_mass"] = df["mass_1"] + df["mass_2"]

    return df


def add_mass_ratio(df):
    """
    Function to add the mass ratio (m2/m1) to df

    requires primary_mass and secondary_mass
    """

    if "mass_ratio" in df.columns:
        return df

    # Add required columns
    df = add_primary_mass(df)
    df = add_secondary_mass(df)

    #
    df["mass_ratio"] = df["secondary_mass"] / df["primary_mass"]

    return df

def add_chirpmass_column(df, m1_name='m1', m2_name='m2', chirpmass_name='chirpmass'):
    """
    Function to add the column of chirp mass
    """

    df[chirpmass_name] = np.where(
        np.logical_and(
            df[m1_name] > 0,
            df[m2_name] > 0),
        calc_chirpmass(
            df[m1_name],
            df[m2_name]),
        0)

    return df

def add_chirp_mass(df):
    """
    Function to add the chirp mass column
    """

    if "chirp_mass" in df.columns:
        return df

    df = add_chirpmass_column(
        df, m1_name="mass_1", m2_name="mass_2", chirpmass_name="chirp_mass"
    )

    return df


def add_zams_primary_mass(df):
    """
    Function to add the ZAMS mass of the current primary mass
    """

    # Get indices of primary and secondary
    indices_star_1_is_primary = np.where(df["mass_1"] >= df["mass_2"], True, False)
    indices_star_2_is_primary = np.where(df["mass_1"] < df["mass_2"], True, False)

    # Set masses
    df["zams_primary_mass"] = -1
    df["zams_primary_mass"][indices_star_1_is_primary] = df["zams_mass_1"][
        indices_star_1_is_primary
    ]
    df["zams_primary_mass"][indices_star_2_is_primary] = df["zams_mass_2"][
        indices_star_2_is_primary
    ]

    return df


def add_zams_secondary_mass(df):
    """
    Function to add the ZAMS mass of the current secondary mass
    """

    # Get indices of primary and secondary
    indices_star_1_is_primary = np.where(df["mass_1"] >= df["mass_2"], True, False)
    indices_star_2_is_primary = np.where(df["mass_1"] < df["mass_2"], True, False)

    #
    df["zams_secondary_mass"] = -1
    df["zams_secondary_mass"][indices_star_1_is_primary] = df["zams_mass_2"][
        indices_star_1_is_primary
    ]
    df["zams_secondary_mass"][indices_star_2_is_primary] = df["zams_mass_1"][
        indices_star_2_is_primary
    ]

    return df


def add_primary_undergone_ppisn(df):
    """
    Function to add primary_undergone_ppisn to df

    We use the undergone_ppisn_1 and undergone_ppisn_2 and set the values
    """

    # Get indices of primary and secondary
    indices_star_1_is_primary = np.where(df["mass_1"] >= df["mass_2"], True, False)
    indices_star_2_is_primary = np.where(df["mass_1"] < df["mass_2"], True, False)

    df.loc[:, "primary_undergone_ppisn"] = -1
    df.loc[indices_star_1_is_primary, "primary_undergone_ppisn"] = df[
        "undergone_ppisn_1"
    ][indices_star_1_is_primary]
    df.loc[indices_star_2_is_primary, "primary_undergone_ppisn"] = df[
        "undergone_ppisn_2"
    ][indices_star_2_is_primary]

    # # add to df
    # df['primary_undergone_ppisn'] = -1
    # df['primary_undergone_ppisn'][indices_star_1_is_primary] = df['undergone_ppisn_1'][indices_star_1_is_primary]
    # df['primary_undergone_ppisn'][indices_star_2_is_primary] = df['undergone_ppisn_2'][indices_star_2_is_primary]

    return df


def add_secondary_undergone_ppisn(df):
    """
    Function to add primary_undergone_ppisn to df

    We use the undergone_ppisn_1 and undergone_ppisn_2 and set the values
    """

    # Get indices of primary and secondary
    indices_star_1_is_primary = np.where(df["mass_1"] >= df["mass_2"], True, False)
    indices_star_2_is_primary = np.where(df["mass_1"] < df["mass_2"], True, False)

    # add to df
    df["secondary_undergone_ppisn"] = -1
    df["secondary_undergone_ppisn"][indices_star_1_is_primary] = df[
        "undergone_ppisn_2"
    ][indices_star_1_is_primary]
    df["secondary_undergone_ppisn"][indices_star_2_is_primary] = df[
        "undergone_ppisn_1"
    ][indices_star_2_is_primary]

    return df


def add_dummy(df):
    """
    DUmmy function, doesnt add
    """

    return df


# Dict containing the functions for the column adding
column_add_function_dict = {
    "primary_mass": add_primary_mass,
    "secondary_mass": add_secondary_mass,
    "total_mass": add_total_mass,
    "mass_ratio": add_mass_ratio,
    "chirp_mass": add_chirp_mass,
    "zams_primary_mass": add_zams_primary_mass,
    "zams_secondary_mass": add_zams_secondary_mass,
    "primary_undergone_ppisn": add_primary_undergone_ppisn,
    "secondary_undergone_ppisn": add_secondary_undergone_ppisn,
    "birth_redshift": add_dummy,
}


def get_rate_data(filename, rate_type, redshift_value, mask):
    """
    Function to get rate data
    """

    datafile = h5py.File(filename)
    all_redshift_keys = sorted(list(datafile["data/{}".format(rate_type)].keys()))
    closest_redshift_key = get_closest_redshift_key(redshift_value, all_redshift_keys)
    rate_array, _ = readout_rate_array_specific_keys(
        datafile=datafile,
        rate_key=rate_type,
        mask=mask,
        redshift_keys=[closest_redshift_key],
    )
    datafile.close()

    return rate_array


def get_closest_redshift_key(redshift_value, all_redshift_keys):
    """
    Function to get the redshift key that lies closest to the desired redshift
    """

    #
    all_redshift_floats = np.array(
        [float(redshift_key) for redshift_key in all_redshift_keys]
    )

    # calculate diff
    diff_redshift_floats = all_redshift_floats - redshift_value

    # min index
    argmin_arr = np.argmin(np.abs(diff_redshift_floats))

    closest_key = all_redshift_keys[argmin_arr]

    #
    return all_redshift_keys[argmin_arr]


def readout_rate_array_specific_keys(datafile, rate_key, mask, redshift_keys):
    """
    Function to read out the rate array

    input:
        datafile has to be an open HDF5 filehandle
    """

    # Turn redshift_keys into float_values
    redshifts_floats = [float(redshift_key) for redshift_key in redshift_keys]

    # Read out redshifts and systems to set up the array
    all_redshifts = np.array(sorted(list(datafile["data/{}".format(rate_key)].keys())))

    # Readout systems and mask to get the shape
    systems = datafile["data/{}/{}".format(rate_key, all_redshifts[0])][()]
    systems = systems[mask]

    # Set up merger rate array
    rate_array = np.zeros(shape=(len(redshift_keys), len(systems)))

    # Fill the array
    for redshift_i, redshift in enumerate(redshift_keys):
        # Readout rate data
        rate_data = datafile["data/{}/{}".format(rate_key, redshift)][()]

        # Mask data
        masked_rate_data = rate_data[mask]

        # Write to array
        rate_array[redshift_i, :] = masked_rate_data

    return rate_array, redshifts_floats


def merge_with_event_dataframe(
    main_dataframe, event_dataframe, event_dataframe_prefix=None
):
    """
    Function to merge an event dataframe to the main dataframe
    """

    on_col = "uuid"

    # Add initial indices of main df to main_df
    main_dataframe["initial_indices"] = list(range(len(main_dataframe.index)))

    if event_dataframe_prefix is not None:
        event_dataframe = event_dataframe.add_prefix(event_dataframe_prefix)
        event_dataframe = event_dataframe.rename(
            columns={event_dataframe_prefix + on_col: on_col}
        )

    # Merge tables
    merged_dataframe = main_dataframe.merge(event_dataframe, how="inner", on=on_col)

    return merged_dataframe


dimensionless_unit = u.m / u.m

# Dictionary containing names for the quantities
quantity_name_dict = {
    "primary_mass": "Primary mass",
    "secondary_mass": "Secondary mass",
    "chirp_mass": "Chirp mass",
    "total_mass": "Total mass",
    "any_mass": "Mass",
    "mass_ratio": "Mass ratio",
    "metallicity": "Metallicity",
    "birth_redshift": "Birth redshift",
    "eccentricity": "Eccentricity",
    # ZAMS mass quantity
    "zams_chirpmass": "ZAMS chirp mass",
    "zams_total_mass": "ZAMS total mass",
    "zams_primary_mass": "ZAMS primary mass",
    "zams_secondary_mass": "ZAMS secondary mass",
    "zams_any_mass": "ZAMS mass",
}

# Dictionary containing units for the quantities
quantity_unit_dict = {
    "primary_mass": u.Msun,
    "secondary_mass": u.Msun,
    "total_mass": u.Msun,
    "mass_ratio": dimensionless_unit,
    "chirp_mass": u.Msun,
    "metallicity": dimensionless_unit,
    "birth_redshift": dimensionless_unit,
    "eccentricity": dimensionless_unit,
    # ZAMS mass quantity
    "zams_chirpmass": u.Msun,
    "zams_total_mass": u.Msun,
    "zams_primary_mass": u.Msun,
    "zams_secondary_mass": u.Msun,
    "zams_any_mass": u.Msun,
}
