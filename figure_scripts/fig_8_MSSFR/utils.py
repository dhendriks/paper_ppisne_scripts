import astropy.units as u
import numpy as np
from scipy.stats import norm as NormDist


def starformation_rate_neijssel19(z, a, b, c, d):
    """
    Function from Neijsel et al 2019 (https://ui.adsabs.harvard.edu/abs/2019MNRAS.490.3740N/abstract)
    """

    rate = a * np.power(1 + z, b) / (1 + np.power((1 + z) / c, d))

    return rate * (u.Msun / (u.yr * (u.Mpc**3)))


def starformation_rate(z, cosmology_configuration, verbosity=0):
    """
    Function to determine the star formation rate.

    Input:
        z: redshift value
        cosmology_configuration: dict containing the choice of star formation rate function ('star_formation_rate_function') and a set of arguments ('star_formation_rate_args')

    Output:
        starformation rate in u.Msun/(u.yr * (u.Gpc**3))
    """

    if cosmology_configuration["star_formation_rate_function"] == "madau_dickinson_sfr":
        return starformation_rate_neijssel19(
            z, **cosmology_configuration["star_formation_rate_args"]
        ).to(u.Msun / (u.yr * (u.Gpc**3)))
    elif cosmology_configuration["star_formation_rate_function"] == "dummy":
        return starformation_rate_dummy().to(u.Msun / (u.yr * (u.Gpc**3)))
    else:
        raise ValueError("Unknown SFR function")


def find_metallicity_distribution(
    redshifts,
    min_logZ_COMPAS,
    max_logZ_COMPAS,
    mu0=0.035,
    muz=-0.23,
    sigma_0=0.39,
    sigma_z=0.0,
    alpha=0.0,
    min_logZ=-12.0,
    max_logZ=0.0,
    step_logZ=0.01,
):
    """
    COMPAS Z- distribution from https://ui.adsabs.harvard.edu/abs/2022ApJ...931...17V/abstract

    Calculate the distribution of metallicities at different redshifts using a log skew normal distribution
    the log-normal distribution is a special case of this log skew normal distribution distribution, and is retrieved by setting
    the skewness to zero (alpha = 0).
    Based on the method in Neijssel+19. Default values of mu0=0.035, muz=-0.23, sigma_0=0.39, sigma_z=0.0, alpha =0.0,
    retrieve the dP/dZ distribution used in Neijssel+19

    NOTE: This assumes that metallicities in COMPAS are drawn from a flat in log distribution!

    Args:
        max_redshift       --> [float]          max redshift for calculation
        redshift_step      --> [float]          step used in redshift calculation
        min_logZ_COMPAS    --> [float]          Minimum logZ value that COMPAS samples
        max_logZ_COMPAS    --> [float]          Maximum logZ value that COMPAS samples

        mu0    =  0.035    --> [float]           location (mean in normal) at redshift 0
        muz    = -0.25    --> [float]           redshift scaling/evolution of the location
        sigma_0 = 0.39     --> [float]          Scale (variance in normal) at redshift 0
        sigma_z = 0.00     --> [float]          redshift scaling of the scale (variance in normal)
        alpha   = 0.00    --> [float]          shape (skewness, alpha = 0 retrieves normal dist)

        min_logZ           --> [float]          Minimum logZ at which to calculate dPdlogZ (influences normalization)
        max_logZ           --> [float]          Maximum logZ at which to calculate dPdlogZ (influences normalization)
        step_logZ          --> [float]          Size of logZ steps to take in finding a Z range

    Returns:
        dPdlogZ            --> [2D float array] Probability of getting a particular logZ at a certain redshift
        metallicities      --> [list of floats] Metallicities at which dPdlogZ is evaluated
        p_draw_metallicity --> float            Probability of drawing a certain metallicity in COMPAS (float because assuming uniform)
    """

    ##################################
    # Log-Linear redshift dependence of sigma
    sigma = sigma_0 * 10 ** (sigma_z * redshifts)

    ##################################
    # Follow Langer & Norman 2007? in assuming that mean metallicities evolve in z as:
    mean_metallicities = mu0 * 10 ** (muz * redshifts)

    # Now we re-write the expected value of ou log-skew-normal to retrieve mu
    beta = alpha / (np.sqrt(1 + (alpha) ** 2))
    PHI = NormDist.cdf(beta * sigma)
    mu_metallicities = np.log(
        mean_metallicities / 2.0 * 1.0 / (np.exp(0.5 * sigma**2) * PHI)
    )

    ##################################
    # create a range of metallicities (thex-values, or random variables)
    log_metallicities = np.arange(min_logZ, max_logZ + step_logZ, step_logZ)
    metallicities = np.exp(log_metallicities)

    ##################################
    # probabilities of log-skew-normal (without the factor of 1/Z since this is dp/dlogZ not dp/dZ)
    dPdlogZ = (
        2.0
        / (sigma[:, np.newaxis])
        * NormDist.pdf(
            (log_metallicities - mu_metallicities[:, np.newaxis]) / sigma[:, np.newaxis]
        )
        * NormDist.cdf(
            alpha
            * (log_metallicities - mu_metallicities[:, np.newaxis])
            / sigma[:, np.newaxis]
        )
    )

    ##################################
    # normalise the distribution over al metallicities
    norm = dPdlogZ.sum(axis=-1) * step_logZ
    dPdlogZ = dPdlogZ / norm[:, np.newaxis]

    ##################################
    # assume a flat in log distribution in metallicity to find probability of drawing Z in COMPAS
    p_draw_metallicity = 1 / (max_logZ_COMPAS - min_logZ_COMPAS)

    return dPdlogZ, metallicities, p_draw_metallicity
