"""
Main script to plot all the paper figures.

this function does not call the plot_XXX functions directly, but rather calls handle_paper_plot_XXX_<> functions that contain the correct configuration for each plot.
This is to keep things close to the source and not to clog this function
"""

import os
from functools import partial

# Fig 1
from paper_ppisne_scripts.figure_scripts.fig_1_variation_schematic.handle_paper_plot_variation_schematic import (
    handle_paper_plot_variation_schematic,
)

# Fig 2
from paper_ppisne_scripts.figure_scripts.fig_2_primary_mass_distribution_CO_shift_variation.handle_paper_plot_primary_mass_distribution_CO_core_variation import (
    handle_paper_plot_primary_mass_distribution_CO_core_variation,
)

# Fig 3
from paper_ppisne_scripts.figure_scripts.fig_3_primary_mass_distribution_extra_ML_variation.handle_paper_plot_primary_mass_distribution_extra_ML_variation import (
    handle_paper_plot_primary_mass_distribution_extra_ML_variation,
)

# Fig 4
from paper_ppisne_scripts.figure_scripts.fig_4_transients_vs_redshift.handle_paper_plot_total_rates_individual_sn_types import (
    handle_paper_plot_total_rates_individual_sn_types,
)

# Fig 5
from paper_ppisne_scripts.figure_scripts.fig_5_grid_single_mass_metallicity.handle_paper_plot_grid_single_mass_metallicity import (
    handle_paper_plot_grid_single_mass_metallicity,
)

# Fig 6
from paper_ppisne_scripts.figure_scripts.fig_6_primary_mass_distribution_fiducial_CE_comparison.handle_paper_plot_primary_mass_distribution_fiducial_CE_comparison import (
    handle_paper_plot_primary_mass_distribution_fiducial_CE_comparison,
)

# Fig 8
from paper_ppisne_scripts.figure_scripts.fig_7_ratio_kick_scaling_factors_merging_bhbh_systems_fiducial.handle_paper_plot_ratio_kick_scaling_factors_merging_bhbh_systems_fiducial import (
    handle_paper_plot_ratio_kick_scaling_factors_merging_bhbh_systems_fiducial,
)

# Fig 7
from paper_ppisne_scripts.figure_scripts.fig_8_MSSFR.handle_paper_plot_MSSFR import (
    handle_paper_plot_MSSFR,
)
from paper_ppisne_scripts.figure_scripts.utils import (
    general_handle_figure_name_function,
)

# from grav_waves.paper_scripts.sn_rate_plots.total_rates_individual_sn_types.paper_plot_handlers import (
#     handle_paper_plot_total_rates_individual_sn_types,
#     handle_paper_plot_total_rates_individual_sn_types_SN_ONLY,
# )

# from grav_waves.paper_scripts.primary_mass_distribution.paper_plot_handlers import (
#     ,
#     handle_paper_plot_primary_mass_distribution_extra_ML_variation,
#     handle_paper_plot_primary_mass_distribution_fiducial_CE_comparison,
#     handle_paper_plot_primary_mass_distribution_fiducial_with_panels,
#     handle_paper_plot_primary_mass_distribution_downward_shift_variation_with_panels,
#     handle_paper_plot_primary_mass_distribution_upward_shift_variation_with_panels,
# )

# from grav_waves.paper_scripts.sn_rate_plots.mass_ejected_vs_redshift_plot.paper_plot_handlers import (
#     handle_paper_plot_ejecta_analysis_fiducial,
#     handle_paper_plot_ejecta_analysis_downward_shift,
#     handle_paper_plot_ejecta_analysis_upward_shift,
# )

# from grav_waves.paper_scripts.black_hole_kick_scaling_factor_comparison.secondary_bh_formations_fallback_vs_scaling import (
#     handle_paper_plot_ratio_kick_scaling_factors_merging_bhbh_systems_fiducial,
#     handle_paper_plot_separate_kick_scaling_factors_merging_bhbh_systems_fiducial,
# )

#
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def plot_all_paper_figures(
    schematic_data_result_root,
    MSSFR_data_result_root,
    grid_single_data_result_root,
    primary_mass_distribution_result_root,
    GW_ligo_obs_data_root,
    transient_result_root,
    plot_output_dir,
    figure_flags=None,
):
    """
    Function to plot all the relevant plots.
    """

    if figure_flags is None:
        figure_flags = {}

    #
    omit_basename = False
    prepend_figure_counter = True
    show_plot = False
    figure_counter = 1

    # Construct new function
    handle_figure_name = partial(
        general_handle_figure_name_function,
        output_dir=plot_output_dir,
        figure_format=figure_flags['figure_format'],
        omit_basename=omit_basename,
        prepend_figure_counter=prepend_figure_counter,
    )

    ###################
    # Variation schematic
    if figure_flags.get("fig_1_variation_schematic", True):
        output_name = handle_figure_name(
            basename="variation_schematic", figure_counter=figure_counter
        )
        handle_paper_plot_variation_schematic(
            result_dir=schematic_data_result_root,
            output_name=output_name,
            show_plot=show_plot,
        )
    figure_counter += 1

    ###################
    # primary mass distribution CO shift
    if figure_flags.get("fig_2_primary_mass_distribution_CO_shift_variation", True):
        output_name = handle_figure_name(
            basename="primary_mass_distribution_CO_shift_variation",
            figure_counter=figure_counter,
        )
        handle_paper_plot_primary_mass_distribution_CO_core_variation(
            result_root=primary_mass_distribution_result_root,
            GW_ligo_obs_data_root=GW_ligo_obs_data_root,
            output_name=output_name,
            show_plot=show_plot,
        )
    figure_counter += 1

    ###################
    # primary mass distribution extra ML
    if figure_flags.get("fig_3_primary_mass_distribution_extra_ML_variation", True):
        output_name = handle_figure_name(
            basename="primary_mass_distribution_extra_ML_variation",
            figure_counter=figure_counter,
        )
        handle_paper_plot_primary_mass_distribution_extra_ML_variation(
            result_root=primary_mass_distribution_result_root,
            GW_ligo_obs_data_root=GW_ligo_obs_data_root,
            output_name=output_name,
            show_plot=show_plot,
        )
    figure_counter += 1

    ###################
    # transients_vs_redshift
    if figure_flags.get("fig_4_transients_vs_redshift", True):
        output_name = handle_figure_name(
            basename="transients_vs_redshift", figure_counter=figure_counter
        )
        handle_paper_plot_total_rates_individual_sn_types(
            result_root=transient_result_root,
            output_name=output_name,
            show_plot=show_plot,
        )
    figure_counter += 1

    ###################
    # single star mass metallicity grid
    if figure_flags.get("fig_5_grid_single_mass_metallicity", True):
        output_name = handle_figure_name(
            basename="grid_single_mass_metallicity", figure_counter=figure_counter
        )
        handle_paper_plot_grid_single_mass_metallicity(
            result_dir=grid_single_data_result_root,
            output_name=output_name,
            show_plot=show_plot,
        )
    figure_counter += 1

    ###################
    # primary_mass_distribution_fiducial_CE_comparison
    if figure_flags.get("fig_6_primary_mass_distribution_fiducial_CE_comparison", True):
        output_name = handle_figure_name(
            basename="primary_mass_distribution_fiducial_CE_comparison",
            figure_counter=figure_counter,
        )
        handle_paper_plot_primary_mass_distribution_fiducial_CE_comparison(
            result_root=primary_mass_distribution_result_root,
            GW_ligo_obs_data_root=GW_ligo_obs_data_root,
            output_name=output_name,
            show_plot=show_plot,
        )
    figure_counter += 1

    ###################
    # ratio_kick_scaling_factors_merging_bhbh_systems_fiducial
    if figure_flags.get(
        "fig_7_ratio_kick_scaling_factors_merging_bhbh_systems_fiducial", True
    ):
        output_name = handle_figure_name(
            basename="ratio_kick_scaling_factors_merging_bhbh_systems_fiducial",
            figure_counter=figure_counter,
        )
        handle_paper_plot_ratio_kick_scaling_factors_merging_bhbh_systems_fiducial(
            result_root=primary_mass_distribution_result_root,
            output_name=output_name,
            show_plot=show_plot,
        )
    figure_counter += 1

    ###################
    # MSSFR
    if figure_flags.get("fig_8_MSSFR", True):
        output_name = handle_figure_name(
            basename="MSSFR", figure_counter=figure_counter
        )
        handle_paper_plot_MSSFR(
            result_dir=MSSFR_data_result_root,
            output_name=output_name,
            show_plot=show_plot,
        )
    figure_counter += 1


if __name__ == "__main__":
    ################
    # NOTE: make sure 'paper_PPISNe_Hendriks2023_data_dir' environment variable is set or change this line
    RESULT_ROOT = os.getenv("paper_PPISNe_Hendriks2023_data_dir")

    if RESULT_ROOT is None:
        raise ValueError(
            "Please configure the environment variable 'paper_PPISNe_Hendriks2023_data_dir'"
        )

    #################
    # Set primary mass merger rate result root
    primary_mass_distribution_result_root = os.path.join(
        RESULT_ROOT, "population_data/EVENTS_V2.2.2_SEMI_HIGH_RES"
    )

    #################
    # Set transient data result root
    transient_result_root = os.path.join(
        RESULT_ROOT, "population_data/EVENTS_V2.2.2_MID_RES"
    )

    #################
    # Set MSSFR data result root
    MSSFR_data_result_root = os.path.join(
        RESULT_ROOT, "population_data/EVENTS_V2.2.2_SEMI_HIGH_RES"
    )

    #################
    # Set schematic data result root
    schematic_data_result_root = os.path.join(RESULT_ROOT, "schematic_overview_data")

    #################
    # Set grid single data result root
    grid_single_data_result_root = os.path.join(
        RESULT_ROOT, "grid_single_mass_metallicity_data"
    )

    #################
    # Set data root for GW data
    GW_ligo_obs_data_root = os.path.join(RESULT_ROOT, "GW_LIGO_OBS_DATA")

    #########
    # Output dir
    plot_output_dir = os.path.join(this_file_dir, "all_paper_plots")

    #
    plot_all_paper_figures(
        schematic_data_result_root=schematic_data_result_root,
        MSSFR_data_result_root=MSSFR_data_result_root,
        GW_ligo_obs_data_root=GW_ligo_obs_data_root,
        grid_single_data_result_root=grid_single_data_result_root,
        primary_mass_distribution_result_root=primary_mass_distribution_result_root,
        transient_result_root=transient_result_root,
        plot_output_dir=plot_output_dir,
        figure_flags={
            "fig_1_variation_schematic": True,
            "fig_2_primary_mass_distribution_CO_shift_variation": True,
            "fig_3_primary_mass_distribution_extra_ML_variation": True,
            "fig_4_transients_vs_redshift": True,
            "fig_5_grid_single_mass_metallicity": True,
            "fig_6_primary_mass_distribution_fiducial_CE_comparison": True,
            "fig_7_ratio_kick_scaling_factors_merging_bhbh_systems_fiducial": True,
            "fig_8_MSSFR": True,
            "figure_format": "pdf"
        },
    )

    # #
    # plot_all_paper_figures(
    #     schematic_data_result_root=schematic_data_result_root,
    #     MSSFR_data_result_root=MSSFR_data_result_root,
    #     GW_ligo_obs_data_root=GW_ligo_obs_data_root,
    #     grid_single_data_result_root=grid_single_data_result_root,
    #     primary_mass_distribution_result_root=primary_mass_distribution_result_root,
    #     transient_result_root=transient_result_root,
    #     plot_output_dir=plot_output_dir,
    #     figure_flags={
    #         "fig_1_variation_schematic": True,
    #         "fig_2_primary_mass_distribution_CO_shift_variation": True,
    #         "fig_3_primary_mass_distribution_extra_ML_variation": True,
    #         "fig_4_transients_vs_redshift": True,
    #         "fig_5_grid_single_mass_metallicity": True,
    #         "fig_6_primary_mass_distribution_fiducial_CE_comparison": True,
    #         "fig_7_ratio_kick_scaling_factors_merging_bhbh_systems_fiducial": True,
    #         "fig_8_MSSFR": True,
    #         "figure_format": "png"
    #     },
    # )
