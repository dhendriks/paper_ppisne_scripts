"""
Functions to compare (1-fb) and Mns/Mbh
"""

import copy
import os
import warnings

import h5py
import matplotlib
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import colors
from pandas.errors import SettingWithCopyWarning

from paper_ppisne_scripts.figure_scripts.utils import (
    get_rate_data,
    handle_columns,
    load_mpl_rc,
    merge_with_event_dataframe,
    quantity_name_dict,
    quantity_unit_dict,
    show_and_save_plot,
)
from paper_ppisne_scripts.settings import SN_type_dict_new as SN_type_dict

inverse_sn_type_dict = {value: key for key, value in SN_type_dict.items()}

load_mpl_rc()
mpl.rc(
    "axes",
    grid=False,
)


warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
warnings.simplefilter(action="ignore", category=FutureWarning)


def merging_bhbh_systems(dco_file, mass_range=None, plot_settings=None):
    """
    Function to find the
    """

    if plot_settings is None:
        plot_settings = {}

    # Load DCO dataframe
    combined_dataframes = pd.read_hdf(dco_file, key="data/combined_dataframes")

    # add extra columns to database
    handle_columns(
        combined_dataframes,
        "primary_mass",
        [],
        extra_columns=["secondary_mass"],
    )

    # Load rate array for merging at Z=0
    rate_array = get_rate_data(
        dco_file,
        rate_type="merger_rate",
        redshift_value=0.2,
        mask=np.ones(len(combined_dataframes.index), dtype=bool),
    )[0]

    # Select Supernovae
    sn_event_dataframe = pd.read_hdf(dco_file, key="data/events/SN_BINARY")

    ###########
    # Handle data
    merged_sn_df = merge_with_event_dataframe(
        main_dataframe=combined_dataframes, event_dataframe=sn_event_dataframe
    )

    # add rates
    merged_sn_df["rates"] = rate_array[merged_sn_df["initial_indices"].to_numpy()]

    # select only those that have fallback and become BHs
    merged_sn_df = merged_sn_df.query("post_SN_st == 14")
    merged_sn_df = merged_sn_df.query("fallback_fraction < 1")

    # select those supernovae that form the secondary BH
    secondary_supernovae = merged_sn_df.query("secondary_mass == post_SN_mass")
    secondary_supernovae = secondary_supernovae.query(
        "SN_type not in [{}, {}]".format(
            inverse_sn_type_dict["PPISN"], inverse_sn_type_dict["PISN"]
        )
    )

    # Add ratio Mns/Mbh
    secondary_supernovae["ratio_mns_mbh"] = 1.4 / secondary_supernovae["post_SN_mass"]

    # Add (1-fb)
    secondary_supernovae["one_minus_fallback_fraction"] = (
        1 - secondary_supernovae["fallback_fraction"]
    )

    # Add ratio of scalings
    secondary_supernovae["ratio_two_scalings"] = (
        secondary_supernovae["ratio_mns_mbh"]
        / secondary_supernovae["one_minus_fallback_fraction"]
    )

    # Set up bins
    primary_mass_bins = np.arange(0, 70)
    ratio_bins = 10 ** np.arange(-1, 1.7, 0.1)

    # Calculate hist
    hist = np.histogram2d(
        secondary_supernovae["primary_mass"].to_numpy(),
        secondary_supernovae["ratio_two_scalings"],
        bins=[primary_mass_bins, ratio_bins],
        weights=secondary_supernovae["rates"],
    )

    # make bincenters
    x_bincenters = (primary_mass_bins[1:] + primary_mass_bins[:-1]) / 2
    y_bincenters = (ratio_bins[1:] + ratio_bins[:-1]) / 2

    X, Y = np.meshgrid(x_bincenters, y_bincenters)

    ######
    #
    vmax = hist[0].max()
    vmin = 10 ** (np.log10(vmax) - 4)

    # Set norm
    norm = colors.LogNorm(vmin=vmin, vmax=vmax)

    # Set CMAP
    cmap = copy.copy(plt.cm.viridis)
    cmap.set_under(color="white")

    #
    fig = plt.figure(figsize=(15, 15))
    gs = fig.add_gridspec(nrows=1, ncols=10)
    ax = fig.add_subplot(gs[:, :-2])
    ax_cb = fig.add_subplot(gs[:, -1])

    # All SN
    _ = ax.pcolormesh(
        X,
        Y,
        hist[0].T,
        norm=norm,
        cmap=cmap,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(ax_cb, norm=norm, cmap=cmap, extend="min")
    cbar.ax.set_ylabel(r"Merger rate")

    # plot
    ax.set_yscale("log")
    ax.set_ylabel(
        "Ratio kick-scaling factors secondary BH\n"
        + r"$(M_{\mathrm{NS}}/M_{\mathrm{BH}})/(1-f_{\mathrm{fallback}})$"
    )
    ax.set_xlabel(
        "{} [{}]".format(
            quantity_name_dict["primary_mass"],
            quantity_unit_dict["primary_mass"].to_string("latex"),
        ),
        fontsize=plot_settings.get("axislabel_fontsize", 26),
    )

    # Handle saving etc
    show_and_save_plot(fig, plot_settings)


def handle_paper_plot_ratio_kick_scaling_factors_merging_bhbh_systems_fiducial(
    result_root, output_name, show_plot
):
    """
    Function to handle the paper plot on the ratio of the kick scaling factors for all our merging BHBH systems.
    """

    #
    files = (
        result_root
        + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5"
    )

    #
    merging_bhbh_systems(
        dco_file=files,
        mass_range=None,
        plot_settings={
            "show_plot": show_plot,
            "output_name": output_name,
        },
    )


if __name__ == "__main__":
    this_file = os.path.abspath(__file__)
    this_file_dir = os.path.dirname(this_file)

    # NOTE: make sure 'paper_PPISNe_Hendriks2023_data_dir' environment variable is set or change this line
    result_dir = os.path.join(
        os.getenv("paper_PPISNe_Hendriks2023_data_dir"),
        "population_data/EVENTS_V2.2.2_LOW_RES",
    )

    handle_paper_plot_ratio_kick_scaling_factors_merging_bhbh_systems_fiducial(
        result_root=result_dir,
        output_name=os.path.join(this_file_dir, "plots/test.pdf"),
        show_plot=False,
    )
