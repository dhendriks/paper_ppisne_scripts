"""
Script that contains the main routine that generates the variation schematic
"""

import os
import warnings

import numpy as np

from paper_ppisne_scripts.figure_scripts.fig_1_variation_schematic.utils import (
    get_df,
    place_arrows,
    plot_co_core_one_variation,
)
from paper_ppisne_scripts.figure_scripts.utils import load_mpl_rc, show_and_save_plot
from paper_ppisne_scripts.settings import format_COshift, format_extraML

load_mpl_rc()

warnings.simplefilter(action="ignore", category=UserWarning)
PPISN_SN_NUMBER = 24


def customise_schematic_plot_one_panel(
    result_dir,
    simname,
    simname_old,
    plot_settings,
    add_upward_shift=True,
    add_extra_ML=True,
    add_downward_shift=True,
    add_region_onset_PPISN=True,
):
    """
    Function to customize the ZAMS schematic plot
    """

    x_param = "pre_sn_co_core_mass"

    # set up directories
    result_dir_simname_mass_removal = os.path.join(result_dir, "mass_removal", simname)
    result_dir_simname_core_mass_shift = os.path.join(
        result_dir, "core_mass_shift", simname
    )
    result_dir_simname_old = os.path.join(result_dir, "old_prescription", simname_old)

    ####################
    # Read out core mass shift
    if add_downward_shift:
        co_shift_data = os.path.join(result_dir_simname_core_mass_shift, "-5")
        co_shift_df = get_df(co_shift_data)

    ####################
    # Read out the custom data
    if add_extra_ML:
        extra_ML_data = os.path.join(result_dir_simname_mass_removal, "5")
        extra_ML_df = get_df(extra_ML_data)
        nonzero_extra_ML_df = extra_ML_df[extra_ML_df.mass != 0]

    ####################
    # fiducial
    fiducial_data = os.path.join(result_dir_simname_mass_removal, "0")
    fiducial_df = get_df(fiducial_data)

    ####################
    # old prescription
    old_prescription_data = os.path.join(result_dir_simname_old, "0")
    old_prescription_df = get_df(old_prescription_data)

    ######################
    # Set up call to the routine
    plot_func = plot_co_core_one_variation
    fig, axes_list = plot_func(
        variation_name="core_mass_shift",
        result_directory_variation=result_dir_simname_core_mass_shift,
        old_prescription_result_directory_root=result_dir_simname_old,
        plot_settings={
            "axislabel_fontsize": 42,
            "legend_fontsize": 24,
            "supernova_lines_fontsize": 28,
        },
        extra_variation_configuration={
            "dataset_used_for_regions": "variation",
            "add_variation_bracket": False,
            "add_shading_regions": False,
            "add_supernova_lines": False,
            "add_variation_line": add_downward_shift,
        },
        return_fig=True,
    )

    ##################
    # resize etc

    # Loop over axes
    for axis in axes_list:
        # Resize to higher top mass
        cur_ylim = list(axis.get_ylim())
        cur_ylim[-1] = 90
        axis.set_ylim(cur_ylim)

        # Shift and remove low-x
        cur_xlim = list(axis.get_xlim())
        cur_xlim[0] = 50 if x_param == "zams_mass" else 12.5
        axis.set_xlim(cur_xlim)

        axis.xaxis.set_tick_params(labelsize=34)

    ###################
    # Inject extra data into the top plot
    if add_upward_shift:
        #
        extra_co_shift_data = os.path.join(result_dir, "custom", "5")
        extra_co_shift_df = get_df(extra_co_shift_data)
        nonzero_extra_co_shift_df = extra_co_shift_df[extra_co_shift_df.mass != 0]

        #
        extra_CO_shift_line = axes_list[0].plot(
            nonzero_extra_co_shift_df[x_param],
            nonzero_extra_co_shift_df["mass"],
            zorder=-199,
            linestyle=(0, (5, 5)),
        )
        # extra_CO_shift_label = "$\\Delta M_{\\mathrm{CO,\\ PPI}} = +5.0$"
        extra_CO_shift_label = format_COshift(5)

    ###################
    # Inject mass loss data into the top plot
    if add_extra_ML:
        extra_ML_line = axes_list[0].plot(
            nonzero_extra_ML_df[x_param],
            nonzero_extra_ML_df["mass"],
            zorder=-199,
            linestyle=(5, (10, 2.5)),
            color="red",
        )
        # extra_ML_label = "$\\Delta M_{\\mathrm{PPI,\\ Extra}} = +5.0$"
        extra_ML_label = format_extraML(5)

    ##################
    # Add anchor points
    anchorpoint_mass = "pre_sn_mass"
    anchorpoint_size = 30
    anchorpoint_edgewidth = 4
    df_list = []

    # old prescription
    first_ppisn_mass_old_prescription = old_prescription_df[
        old_prescription_df.sn_type == PPISN_SN_NUMBER
    ].iloc[0]
    _ = axes_list[0].plot(
        first_ppisn_mass_old_prescription[x_param],
        first_ppisn_mass_old_prescription[anchorpoint_mass],
        marker="o",
        linestyle="None",
        markeredgewidth=anchorpoint_edgewidth,
        markersize=anchorpoint_size,
        markerfacecolor="None",
        markeredgecolor="#666699",
    )
    df_list.append([first_ppisn_mass_old_prescription, old_prescription_df])

    # extra ML
    if add_extra_ML:
        first_ppisn_mass_extra_ML = extra_ML_df[
            extra_ML_df.sn_type == PPISN_SN_NUMBER
        ].iloc[0]
        _ = axes_list[0].plot(
            first_ppisn_mass_extra_ML[x_param],
            first_ppisn_mass_extra_ML[anchorpoint_mass],
            marker="o",
            linestyle="None",
            markeredgewidth=anchorpoint_edgewidth,
            markersize=anchorpoint_size,
            markerfacecolor="None",
            markeredgecolor="red",
        )
        df_list.append([first_ppisn_mass_extra_ML, extra_ML_df])

    # fiducial
    first_ppisn_mass_fiducial = fiducial_df[
        fiducial_df.sn_type == PPISN_SN_NUMBER
    ].iloc[0]
    _ = axes_list[0].plot(
        first_ppisn_mass_fiducial[x_param],
        first_ppisn_mass_fiducial[anchorpoint_mass],
        marker="o",
        linestyle="None",
        markeredgewidth=anchorpoint_edgewidth,
        markersize=anchorpoint_size,
        markerfacecolor="None",
        markeredgecolor="black",
    )
    df_list.append([first_ppisn_mass_fiducial, fiducial_df])

    # core mass shift lower
    if add_downward_shift:
        first_ppisn_mass_co_shift = co_shift_df[
            co_shift_df.sn_type == PPISN_SN_NUMBER
        ].iloc[0]
        _ = axes_list[0].plot(
            first_ppisn_mass_co_shift[x_param],
            first_ppisn_mass_co_shift[anchorpoint_mass],
            marker="o",
            linestyle="None",
            markeredgewidth=anchorpoint_edgewidth,
            markersize=anchorpoint_size,
            markerfacecolor="None",
            markeredgecolor="orange",
        )
        df_list.append([first_ppisn_mass_co_shift, co_shift_df])

    # extra co core shift (upper)
    if add_upward_shift:
        first_ppisn_mass_extra_co_shift = nonzero_extra_co_shift_df[
            nonzero_extra_co_shift_df.sn_type == PPISN_SN_NUMBER
        ].iloc[0]
        _ = axes_list[0].plot(
            first_ppisn_mass_extra_co_shift[x_param],
            first_ppisn_mass_extra_co_shift[anchorpoint_mass],
            marker="o",
            linestyle="None",
            markeredgewidth=anchorpoint_edgewidth,
            markersize=anchorpoint_size,
            markerfacecolor="None",
            markeredgecolor=extra_CO_shift_line[0].get_color(),
        )
        df_list.append([first_ppisn_mass_extra_co_shift, nonzero_extra_co_shift_df])

    ##################
    # add region
    df_min_set = sorted(df_list, key=lambda x: x[0][x_param])[0]
    df_max_set = sorted(df_list, key=lambda x: x[0][x_param], reverse=True)[0]

    df_min = df_min_set[1]
    df_max = df_max_set[1]

    if add_region_onset_PPISN:
        alpha_regions = 0.1

        left_first_ppi = df_min_set[0][x_param]
        right_first_ppi = df_max_set[0][x_param]
        midpoint_first_ppisn = (right_first_ppi + left_first_ppi) / 2

        # fill region with pink for onset first PPI
        axes_list[0].fill_between(
            df_max[(df_max[x_param] >= left_first_ppi)][
                (df_max[x_param] <= right_first_ppi)
            ][x_param],
            df_max[(df_max[x_param] >= left_first_ppi)][
                (df_max[x_param] <= right_first_ppi)
            ]["mass"],
            y2=0,
            color="green",
            alpha=alpha_regions,
        )

        # add arrows
        fig, axes_list[0] = place_arrows(
            fig,
            axes_list[0],
            midpoint_first_ppisn,
            20,
            right_first_ppi - left_first_ppi,
            text="onset first PPISN",
            fontsize=plot_settings.get("supernova_lines_fontsize", 24),
        )

    ######################
    # Add arrows and text
    fiducial_max_mass_ppisn_id = fiducial_df[
        fiducial_df.sn_type == PPISN_SN_NUMBER
    ].idxmax()["mass"]
    fiducial_max_mass_ppisn = fiducial_df.iloc[fiducial_max_mass_ppisn_id]["mass"]
    y_value_sn_mechanism_line = fiducial_max_mass_ppisn + 29

    non_ppisn = df_min[df_min.sn_type < PPISN_SN_NUMBER]
    min_below_ppisn_zams = non_ppisn[x_param].to_numpy().min()
    max_below_ppisn_zams = non_ppisn[x_param].to_numpy().max()
    midpoint_below_ppisn = (max_below_ppisn_zams + min_below_ppisn_zams) / 2

    #
    fig, axes_list[0] = place_arrows(
        fig,
        axes_list[0],
        midpoint_below_ppisn,
        y_value_sn_mechanism_line,
        np.abs(max_below_ppisn_zams - min_below_ppisn_zams),
        text="CC",
        fontsize=plot_settings.get("supernova_lines_fontsize", 24),
    )

    #################
    # Plot the PPI SN
    min_ppisn_zams = df_min[df_min.sn_type == PPISN_SN_NUMBER][x_param].to_numpy().min()
    max_ppisn_zams = df_max[df_max.sn_type == PPISN_SN_NUMBER][x_param].to_numpy().max()

    # zams_mass_of_max_remnant_mass = ppisn_area["zams_mass"]
    size_between_ppisn = max_ppisn_zams - min_ppisn_zams
    midpoint_ppisn = (max_ppisn_zams + min_ppisn_zams) / 2

    #
    fig, ax = place_arrows(
        fig,
        axes_list[0],
        midpoint_ppisn,
        y_value_sn_mechanism_line,
        size_between_ppisn,
        text="PPISN + CC",
        fontsize=plot_settings.get("supernova_lines_fontsize", 24),
    )

    ###############
    # update legend
    handles, labels = axes_list[0].get_legend_handles_labels()

    # construct new handles
    new_handles = handles[:2]
    if add_extra_ML:
        new_handles += extra_ML_line
    if add_upward_shift:
        new_handles += extra_CO_shift_line
    new_handles += handles[2:]

    # construct new labels
    new_labels = labels[:2]
    if add_extra_ML:
        new_labels += [extra_ML_label]
    if add_upward_shift:
        new_labels += [extra_CO_shift_label]
    new_labels += labels[2:]

    # construct legend
    legend = axes_list[0].get_legend()
    legend._legend_box = None
    legend._init_legend_box(new_handles[::-1], new_labels[::-1])
    legend._set_loc(loc=legend._loc)
    legend.set_title(legend.get_title().get_text())

    ############
    # Handle the plotting
    show_and_save_plot(fig, plot_settings)


def handle_paper_plot_variation_schematic(result_dir, output_name, show_plot):
    """
    Function to handle the paper version of the variation schematic plot
    """

    simname = "WIND_ALGORITHM_HENDRIKS_2021_PPISN_NEW_FIT_21_Z0.001"
    simname_old = "WIND_ALGORITHM_HENDRIKS_2021_PPISN_FARMER19_Z0.001"

    customise_schematic_plot_one_panel(
        result_dir=result_dir,
        simname=simname,
        simname_old=simname_old,
        plot_settings={
            "supernova_lines_fontsize": 22,
            "show_plot": show_plot,
            "output_name": output_name,
        },
        add_upward_shift=True,
        add_extra_ML=True,
        add_region_onset_PPISN=True,
        add_downward_shift=True,
    )


if __name__ == "__main__":
    show_plot = False

    #
    this_file = os.path.abspath(__file__)
    this_file_dir = os.path.dirname(this_file)

    # NOTE: make sure 'paper_PPISNe_Hendriks2023_data_dir' environment variable is set or change this line
    result_dir = os.path.join(
        os.getenv("paper_PPISNe_Hendriks2023_data_dir"), "schematic_overview_data"
    )

    #
    handle_paper_plot_variation_schematic(
        result_dir=result_dir,
        output_name=os.path.join(this_file_dir, "plots/test.pdf"),
        show_plot=show_plot,
    )
