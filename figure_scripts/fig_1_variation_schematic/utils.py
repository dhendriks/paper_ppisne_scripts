import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from paper_ppisne_scripts.settings import (
    Farmer_citation,
    format_COshift,
    format_extraML,
)

PPISN_SN_NUMBER = 24


def get_df(result_dir):
    """
    Function to get the dataframes
    """

    datafile = os.path.join(result_dir, "output.dat")
    df = pd.read_table(datafile, sep="\s+")
    df = df.sort_values(by="zams_mass")

    return df


def plot_co_core_one_variation(
    variation_name,
    result_directory_variation,
    old_prescription_result_directory_root,
    extra_variation_configuration={},
    plot_settings=None,
    return_fig=False,
    add_variation_line=True,
):
    """
    Function to plot the CO core to remnant mass with a variation type
    """

    # Set up figure
    fig = plt.figure(figsize=(16, 10))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=8, ncols=1)
    ax = fig.add_subplot(gs[:, 0])
    axes_list = [ax]

    #######
    # Add mass removal plot
    fig, ax = add_co_core_variation_to_axis(
        fig=fig,
        ax=ax,
        plot_settings=plot_settings,
        result_directory_variation=result_directory_variation,
        old_prescription_result_directory_root=old_prescription_result_directory_root,
        #
        variation_name=variation_name,
        #
        **extra_variation_configuration,
    )

    # Updated the axes limits if user provides update
    handle_overriding_xlim(
        fig=fig,
        axes_list=axes_list,
        plot_settings=plot_settings,
    )

    #
    if return_fig:
        return fig, axes_list

    # Handle the plotting
    show_and_save_plot(fig, plot_settings)


def add_co_core_variation_to_axis(
    fig,
    ax,
    plot_settings,
    #
    result_directory_variation,
    old_prescription_result_directory_root,
    no_prescription_result_directory_root=None,
    #
    variation_name="mass_removal",
    dataset_used_for_regions="variation",
    #
    add_pre_SN_mass_line=True,
    add_no_prescription_line=False,
    add_old_prescription_line=True,
    add_fiducial_line=True,
    add_variation_line=True,
    #
    add_variation_bracket=True,
    add_shading_regions=True,
    add_supernova_lines=True,
):
    """
    Function to add the ZAMS mass to remnant mass with extra ppisn mass removal plot to an axis
    """

    # Settings
    min_zams_mass = 0
    max_zams_mass = 70
    x_parameter = "pre_sn_co_core_mass"

    ########################
    # Get metallicity values
    metallicity_value = float(
        os.path.basename(result_directory_variation).split("_Z")[-1]
    )

    ########################
    # Get the dataframes
    (
        old_prescription_df,
        fiducial_df,
        no_prescription_df,
        variation_df,
        df_dict,
        variation_value,
    ) = get_dataframes(
        old_prescription_result_directory_root,
        result_directory_variation,
        no_prescription_result_directory_root,
        variation_name=variation_name,
    )

    ########################
    # Get some masses
    (
        min_ppisn_zams,
        max_ppisn_zams,
        zams_mass_for_max_ppisn,
        fiducial_max_mass_ppisn,
        variation_max_mass_ppisn,
    ) = calculate_min_max_zams_ppisn_masses(fiducial_df, variation_df, x_parameter)

    #############################################
    # Plot masses

    ################
    # Pre-SN mass plot
    if add_pre_SN_mass_line:
        add_pre_SN_mass_plot(ax, fiducial_df, x_parameter)

    ################
    # No ppisn prescription plot
    if add_no_prescription_line:
        add_no_prescription_plot(ax, no_prescription_df, x_parameter)

    ################
    # old prescription mass plot
    if add_old_prescription_line:
        add_old_prescription_plot(ax, old_prescription_df, x_parameter)

    ################
    # variation plot
    if add_variation_line:
        if variation_name == "mass_removal":
            # Add plot for mass removal
            add_mass_removal_plot(ax, variation_df, x_parameter, value=variation_value)

            ################
            # Add bracket indicating the extra PPISN loss
            if add_variation_bracket:
                add_bracket_extra_mass_loss_plot(
                    fig,
                    ax,
                    zams_mass_for_max_ppisn,
                    fiducial_max_mass_ppisn,
                    variation_max_mass_ppisn,
                )

        ################
        # Core mass shift plot
        if variation_name == "core_mass_shift":
            add_core_mass_shift_plot(
                ax, variation_df, x_parameter, value=variation_value
            )

        ################
        # Core mass shift plot
        if variation_name == "mass_removal_multiplier":
            add_mass_removal_multiplier_plot(ax, variation_df, x_parameter)

    ################
    # fiducial mass plot
    if add_fiducial_line:
        add_fiducial_plot(ax, fiducial_df, x_parameter)

    ################
    # Create shaded region under the main line that indicates which prescription
    if add_shading_regions:
        add_shaded_regions_plot(
            ax=ax,
            dataset_used_for_regions=dataset_used_for_regions,
            max_zams_mass=max_zams_mass,
            df_dict=df_dict,
            x_parameter=x_parameter,
        )

    ################
    # Add lines on the top to indicate which supernova type
    if add_supernova_lines:
        add_supernova_lines_plot(
            fig=fig,
            ax=ax,
            fiducial_max_mass_ppisn=fiducial_max_mass_ppisn,
            dataset_used_for_regions=dataset_used_for_regions,
            max_zams_mass=max_zams_mass,
            df_dict=df_dict,
            x_parameter=x_parameter,
            plot_settings=plot_settings,
        )

    ################
    # Add makeup to plot
    add_makeup_plot(
        ax,
        min_zams_mass,
        max_zams_mass,
        fiducial_max_mass_ppisn,
        metallicity_value,
        plot_type="co",
        plot_settings=plot_settings,
    )

    return fig, ax


def get_dataframes(
    old_prescription_result_directory_root,
    result_directory_variation,
    no_prescription_result_directory_root,
    variation_name,
):
    """
    Function to get the dataframes
    """

    ########################
    # Get old prescription df and select BHs
    old_prescription_df = get_df_and_filter_bh(
        os.path.join(old_prescription_result_directory_root, "0")
    )

    ########################
    # get fiducial df
    fiducial_name = "0" if variation_name != "mass_removal_multiplier" else "1"
    fiducial_df = get_df_and_filter_bh(
        os.path.join(result_directory_variation, fiducial_name)
    )

    ########################
    # get no-prescription df
    if no_prescription_result_directory_root is not None:
        no_prescription_df = get_df_and_filter_bh(
            os.path.join(no_prescription_result_directory_root, "0")
        )
    else:
        no_prescription_df = None

    ########################
    # Load mass removal data
    variation_dirs = {
        subdir: os.path.join(result_directory_variation, subdir)
        for subdir in os.listdir(result_directory_variation)
        if not subdir == fiducial_name
    }
    variation_df = get_df_and_filter_bh(variation_dirs[list(variation_dirs.keys())[0]])
    variation_value = float(list(variation_dirs.keys())[0])

    df_dict = {
        "old_prescription_df": old_prescription_df,
        "fiducial_df": fiducial_df,
        "no_prescription_df": no_prescription_df,
        "variation_df": variation_df,
    }

    return (
        old_prescription_df,
        fiducial_df,
        no_prescription_df,
        variation_df,
        df_dict,
        variation_value,
    )


def get_df_and_filter_bh(result_dir):
    """
    Function to get the dataframe and filter on bh only
    """

    df = get_df(result_dir)
    df_bh = df[df.stellar_type == 14]

    return df_bh


def calculate_min_max_zams_ppisn_masses(fiducial_df, variation_df, x_parameter):
    """
    Function to calculate min and max ppisn masses
    """

    ########################
    # Calculate the min and max zams that undergoes ppisn for both the dataframes
    min_ppisn_zams = np.min(
        [
            fiducial_df[fiducial_df.sn_type == PPISN_SN_NUMBER][x_parameter].min(),
            variation_df[variation_df.sn_type == PPISN_SN_NUMBER][x_parameter].min(),
        ]
    )

    max_ppisn_zams = np.max(
        [
            fiducial_df[fiducial_df.sn_type == PPISN_SN_NUMBER][x_parameter].max(),
            variation_df[variation_df.sn_type == PPISN_SN_NUMBER][x_parameter].max(),
        ]
    )

    ########################
    # Get max mass and corresponding ZAMS mass
    zams_mass_for_max_ppisn = fiducial_df.loc[fiducial_df["mass"].idxmax()][x_parameter]
    fiducial_max_mass_ppisn = fiducial_df[
        fiducial_df[x_parameter] == zams_mass_for_max_ppisn
    ]["mass"].to_numpy()[0]
    variation_max_mass_ppisn = variation_df[
        variation_df[x_parameter] == zams_mass_for_max_ppisn
    ]["mass"].to_numpy()[0]

    return (
        min_ppisn_zams,
        max_ppisn_zams,
        zams_mass_for_max_ppisn,
        fiducial_max_mass_ppisn,
        variation_max_mass_ppisn,
    )


def add_pre_SN_mass_plot(ax, fiducial_df, x_parameter):
    """
    Function to add plot for data where the new PPISN prescription is used
    """

    ax.plot(
        fiducial_df[x_parameter],
        fiducial_df["pre_sn_mass"],
        color="black",
        linestyle=":",
        label="Pre-SN mass",
        alpha=0.5,
    )


def add_old_prescription_plot(ax, old_prescription_df, x_parameter):
    """
    Function to add plot for data from farmer+19
    """

    ax.plot(
        old_prescription_df[x_parameter],
        old_prescription_df["mass"],
        linestyle="-.",
        # alpha=0.5,
        label=Farmer_citation,
        color="#944dff",
        zorder=-200,
    )


def add_core_mass_shift_plot(ax, core_mass_shift_df, x_parameter, value=""):
    """
    Function to add the results with extra mass removed
    """

    # Create label
    label = format_COshift(float(value))

    #
    ax.plot(
        core_mass_shift_df[x_parameter],
        core_mass_shift_df["mass"],
        linestyle="--",
        alpha=0.8,
        label=label,
        color="orange",
    )


def add_fiducial_plot(ax, fiducial_df, x_parameter):
    """
    Function to add the fiducial ppisn rresutls
    """

    ax.plot(
        fiducial_df[x_parameter],
        fiducial_df["mass"],
        color="black",
        label="Fiducial",
    )


def add_makeup_plot(
    ax,
    min_zams_mass,
    max_zams_mass,
    fiducial_max_mass_ppisn,
    metallicity_value,
    plot_type,
    plot_settings,
):
    """
    Function to standard makeup to the plot
    """

    # Set limits
    ax.set_xlim(min_zams_mass, max_zams_mass)
    ax.set_ylim(0, fiducial_max_mass_ppisn + 10)

    #################
    # Configure axes etc
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(
        handles[::-1],
        labels[::-1],
        loc="upper left",
        bbox_to_anchor=(-0.01, 0.9),
        fontsize=plot_settings.get("legend_fontsize", 20),
        handlelength=3,
    )

    # Set axis labels
    ax.set_ylabel(
        r"Remnant mass [M$_{\odot}$]",
        fontsize=plot_settings.get("axislabel_fontsize", 20),
    )
    ax.set_xlabel(
        r"ZAMS mass [M$_{\odot}$]"
        if not plot_type == "co"
        else r"Pre-SN CO core mass [M$_{\odot}$]",
        fontsize=plot_settings.get("axislabel_fontsize", 20),
    )

    # #
    # ax.set_title(
    #     "Black hole remnant mass distribution for single star evolution\nat Z={}".format(
    #         metallicity_value
    #     ),
    #     fontsize=plot_settings.get("title_fontsize", 20),
    # )


def handle_overriding_xlim(fig, axes_list, plot_settings):
    """
    function to handle overriding the xlims
    """

    # Loop over axes
    for ax in axes_list:
        # Override the limits if we have input:
        if plot_settings.get("min_xlim", None) is not None:
            ax.set_xlim(
                [
                    plot_settings["min_xlim"],
                    ax.get_xlim()[1],
                ]
            )
        if plot_settings.get("max_xlim", None) is not None:
            ax.set_xlim(
                [
                    ax.get_xlim()[0],
                    plot_settings["max_xlim"],
                ]
            )
        if plot_settings.get("min_ylim", None) is not None:
            ax.set_ylim(
                [
                    plot_settings["min_ylim"],
                    ax.get_ylim()[1],
                ]
            )
        if plot_settings.get("max_ylim", None) is not None:
            ax.set_ylim(
                [
                    ax.get_ylim()[0],
                    plot_settings["max_ylim"],
                ]
            )


def place_arrows(
    fig, ax, midpoint, y_value, size_between, label=None, text="", fontsize=20
):
    headlength = 1
    width = 0.5
    head_width = 1.5

    # red arrow
    right_arrow = ax.arrow(
        midpoint,
        y_value,
        ((size_between / 2) - 1),
        0,
        head_width=head_width,
        head_length=headlength,
        # linewidth=4,
        linewidth=0.0,
        width=width,
        color="black",
        length_includes_head=False,
    )
    left_arrow = ax.arrow(
        midpoint,
        y_value,
        -((size_between / 2) - 1),
        0,
        head_width=head_width,
        head_length=headlength,
        # linewidth=4,
        linewidth=0.0,
        width=width,
        color="black",
        length_includes_head=False,
    )

    #
    if text:
        ax.text(
            midpoint,
            y_value + 2.5,
            text,
            ha="center",
            # va="center",
            color="blue",
            weight="bold",
            bbox=dict(
                facecolor=".9", alpha=1, edgecolor="black", boxstyle="round,pad=0.15"
            ),
            fontsize=fontsize,
        )

    return fig, ax


def add_bracket(fig, axis, endpoint_1, endpoint_2, startpoint, width, **kwargs):
    """
    requires 3 locations and a width:
    - endpoint 1 and 2: locations of the endpoints
    - startpoint: location where the line stars
    - width: distance between the end points and its corner points
    """

    vec_endpoint_1 = np.array(endpoint_1)
    vec_endpoint_2 = np.array(endpoint_2)

    line_between = vec_endpoint_2 - vec_endpoint_1

    perp_vector = np.array([line_between[1], line_between[0]])
    perp_unit_vector = perp_vector / np.linalg.norm(perp_vector)

    cornerpoint_shift = perp_unit_vector * width

    vec_cornerpoint_1 = vec_endpoint_1 + cornerpoint_shift
    vec_cornerpoint_2 = vec_endpoint_2 + cornerpoint_shift

    #
    vec_midpoint = (vec_cornerpoint_1 + vec_cornerpoint_2) / 2

    # vec_startpoint
    vec_startpoint = np.array(startpoint)

    # # Plot endpoints
    # endpoint_list = [vec_endpoint_1, vec_endpoint_2]
    # x, y = zip(*endpoint_list)
    # plt.scatter(x, y, label='endpoints')

    # # Plot cornerpoints:
    # cornerpoint_list = [vec_cornerpoint_1, vec_cornerpoint_2]
    # x, y = zip(*cornerpoint_list)
    # plt.scatter(x, y, label='cornerpoints')

    # # plot dots at midpoint
    # plt.scatter(vec_midpoint[0], vec_midpoint[1], label='midpoint')

    # # Plot dot at startpoint
    # plt.scatter(vec_startpoint[0], vec_startpoint[1])
    # plt.legend()

    # Plot line between endpoints and cornerpoints
    axis.plot(
        [vec_endpoint_1[0], vec_cornerpoint_1[0]],
        [vec_endpoint_1[1], vec_cornerpoint_1[1]],
        **kwargs,
    )
    axis.plot(
        [vec_endpoint_2[0], vec_cornerpoint_2[0]],
        [vec_endpoint_2[1], vec_cornerpoint_2[1]],
        **kwargs,
    )

    # Cornerpoints to midpoints
    axis.plot(
        [vec_cornerpoint_1[0], vec_midpoint[0]],
        [vec_cornerpoint_1[1], vec_midpoint[1]],
        **kwargs,
    )
    axis.plot(
        [vec_cornerpoint_2[0], vec_midpoint[0]],
        [vec_cornerpoint_2[1], vec_midpoint[1]],
        **kwargs,
    )

    # Midpoint to starting point
    axis.plot(
        [vec_midpoint[0], vec_startpoint[0]],
        [vec_midpoint[1], vec_startpoint[1]],
        **kwargs,
    )

    return fig, axis
