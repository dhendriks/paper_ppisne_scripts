"""
Function to plot the
"""


import os

import numpy as np

from paper_ppisne_scripts.figure_scripts.fig_2_primary_mass_distribution_CO_shift_variation.utils import (
    generate_primary_mass_at_redshift_zero_plot,
)
from paper_ppisne_scripts.figure_scripts.utils import load_mpl_rc
from paper_ppisne_scripts.settings import (
    Farmer_citation,
    format_COshift,
    format_extraML,
)

load_mpl_rc()

#
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


#
def handle_paper_plot_primary_mass_distribution_fiducial_CE_comparison(
    result_root, GW_ligo_obs_data_root, output_name, show_plot
):
    """
    Function to handle the paper version of the fiducial primary mass distribution including extra panels.
    """

    import warnings

    # from pd.core.common import SettingWithCopyWarning
    from pandas.errors import SettingWithCopyWarning

    warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
    warnings.simplefilter(action="ignore", category=FutureWarning)

    fig_scale = 15

    #
    convolved_datasets = {
        r"Fiducial (excluding CE)": {
            "filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
            "query": "comenv_counter == 0",
            "label": r"Fiducial (excluding CE)",
        },
        r"Fiducial (including CE)": {
            "filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
            "label": r"Fiducial (including CE)",
        },
    }

    # Call to figure
    generate_primary_mass_at_redshift_zero_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=np.arange(0, 100, 2.5),
        divide_by_binsize=True,
        add_rates_plots=True,
        add_comparison_to_observations=False,
        add_fraction_primary_underwent_ppisn_plot=False,
        redshift_value=0.2,
        bootstraps=50,
        plot_settings={
            "output_name": output_name,
            "show_plot": show_plot,
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 28,
            "legend_kwarg_dict": {"handlelength": 4},
            "observations_fill_between_kwargs": {"color": "grey"},
            "subplot_label_kwargs": {"x_loc": 0.01, "y_loc": 0.9},
            "include_hist_step": True,
            "hist_step_alpha": 0.35,
            "insert_text_fontsize": 32,
            "custom_xlims": [2, 70],
            "colormap_variations": "plasma",
            "show_data": True,
            "add_GW_text": True,
        },
        verbose=0,
        figsize=(1.5 * fig_scale, (2.0 / 3) * fig_scale),
        GW_ligo_obs_data_root=GW_ligo_obs_data_root,
    )


if __name__ == "__main__":
    #########
    # NOTE: make sure 'paper_PPISNe_Hendriks2023_data_dir' environment variable is set or change this line
    data_root = os.path.join(
        os.getenv("paper_PPISNe_Hendriks2023_data_dir"),
        "population_data/EVENTS_V2.2.2_LOW_RES",
    )
    GW_ligo_obs_data_root = os.path.join(
        os.getenv("paper_PPISNe_Hendriks2023_data_dir"), "GW_LIGO_OBS_DATA"
    )

    #########
    #
    plot_dir = "plots"

    #
    handle_paper_plot_primary_mass_distribution_fiducial_CE_comparison(
        result_root=data_root,
        GW_ligo_obs_data_root=GW_ligo_obs_data_root,
        output_name=os.path.join(this_file_dir, "plots/test.pdf"),
        show_plot=False,
    )
