"""
Function to plot the
"""

import os

import numpy as np

from paper_ppisne_scripts.figure_scripts.fig_2_primary_mass_distribution_CO_shift_variation.utils import (
    generate_primary_mass_at_redshift_zero_plot,
)
from paper_ppisne_scripts.figure_scripts.utils import load_mpl_rc
from paper_ppisne_scripts.settings import (
    Farmer_citation,
    format_COshift,
    format_extraML,
)

load_mpl_rc()


#
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def handle_paper_plot_primary_mass_distribution_CO_core_variation(
    result_root, GW_ligo_obs_data_root, output_name, show_plot
):
    """
    Function to handle the paper version of the primary mass distribution with CO core mass range variation.
    """

    import warnings

    # from pd.core.common import SettingWithCopyWarning
    from pandas.errors import SettingWithCopyWarning

    warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
    warnings.simplefilter(action="ignore", category=FutureWarning)

    #
    convolved_datasets = {
        r"Fiducial": {
            "filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
            "label": r"Fiducial",
        },
        r"Farmer+19": {
            "filename": result_root
            + "_SCHNEIDER_WIND_PPISN_FARMER19_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
            "label": Farmer_citation,
        },
        format_COshift(10): {
            "filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_10_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
            "label": format_COshift(+10),
        },
        format_COshift(5): {
            "filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_5_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
            "label": format_COshift(5),
        },
        format_COshift(-5): {
            "filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-5_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
            "label": format_COshift(-5),
        },
        format_COshift(-10): {
            "filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-10_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
            "label": format_COshift(-10),
        },
        format_COshift(-15): {
            "filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
            "label": format_COshift(-15),
        },
        format_COshift(-20): {
            "filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-20_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
            "label": format_COshift(-20),
        },
    }

    # Call to figure
    generate_primary_mass_at_redshift_zero_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=np.arange(0, 100, 2.5),
        divide_by_binsize=True,
        add_rates_plots=True,
        add_fraction_primary_underwent_ppisn_plot=True,
        redshift_value=0.2,
        bootstraps=50,
        general_query="comenv_counter == 0",
        plot_settings={
            "output_name": output_name,
            "insert_text_main_panel": "excluding CE",
            "show_plot": show_plot,
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 28,
            "legend_kwarg_dict": {"handlelength": 4},
            "observations_fill_between_kwargs": {"color": "grey"},
            "subplot_label_kwargs": {"x_loc": 0.01, "y_loc": 0.9},
            "include_hist_step": True,
            "hist_step_alpha": 0.35,
            "insert_text_fontsize": 32,
            "custom_xlims": [2, 70],
            # "figsize": (1.5 * 15, 1.5 * 15),
            "colormap_variations": "viridis",
            "show_data": True,
            "add_GW_text": True,
        },
        verbose=0,
        title="Shifted range of CO cores that undergo PPISN",
        GW_ligo_obs_data_root=GW_ligo_obs_data_root,
    )


def handle_paper_plot_primary_mass_distribution_CO_core_variation_local(
    result_root, GW_ligo_obs_data_root, output_name, show_plot
):
    """
    Function to handle the paper version of the primary mass distribution with CO core mass range variation.
    """

    import warnings

    # from pd.core.common import SettingWithCopyWarning
    from pandas.errors import SettingWithCopyWarning

    warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
    warnings.simplefilter(action="ignore", category=FutureWarning)

    #
    convolved_datasets = {
        r"Fiducial": {
            "filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
            "label": r"Fiducial",
        },
    }

    # Call to figure
    generate_primary_mass_at_redshift_zero_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=np.arange(0, 100, 2.5),
        divide_by_binsize=True,
        add_rates_plots=True,
        add_fraction_primary_underwent_ppisn_plot=True,
        redshift_value=0.2,
        bootstraps=50,
        general_query="comenv_counter == 0",
        plot_settings={
            "output_name": output_name,
            "insert_text_main_panel": "excluding CE",
            "show_plot": show_plot,
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 28,
            "legend_kwarg_dict": {"handlelength": 4},
            "observations_fill_between_kwargs": {"color": "grey"},
            "subplot_label_kwargs": {"x_loc": 0.01, "y_loc": 0.9},
            "include_hist_step": True,
            "hist_step_alpha": 0.35,
            "insert_text_fontsize": 32,
            "custom_xlims": [2, 70],
            # "figsize": (1.5 * 15, 1.5 * 15),
            "colormap_variations": "viridis",
            "show_data": True,
            "add_GW_text": True,
        },
        verbose=0,
        title="Shifted range of CO cores that undergo PPISN",
        GW_ligo_obs_data_root=GW_ligo_obs_data_root,
    )


if __name__ == "__main__":
    #########
    # NOTE: make sure 'paper_PPISNe_Hendriks2023_data_dir' environment variable is set or change this line
    data_root = os.path.join(
        os.getenv("paper_PPISNe_Hendriks2023_data_dir"),
        "population_data/EVENTS_V2.2.2_LOW_RES",
    )
    GW_ligo_obs_data_root = os.path.join(
        os.getenv("paper_PPISNe_Hendriks2023_data_dir"), "GW_LIGO_OBS_DATA"
    )

    #########
    #
    plot_dir = "plots"

    #
    # handle_paper_plot_primary_mass_distribution_CO_core_variation(
    handle_paper_plot_primary_mass_distribution_CO_core_variation_local(
        result_root=data_root,
        GW_ligo_obs_data_root=GW_ligo_obs_data_root,
        output_name=os.path.join(this_file_dir, "plots/test.pdf"),
        show_plot=False,
    )
