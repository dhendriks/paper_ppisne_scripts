import os

import deepdish as dd
import h5py
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from paper_ppisne_scripts.figure_scripts.utils import (
    add_labels_subplots,
    handle_columns,
    hatch_list,
    linestyle_list,
    load_mpl_rc,
    merge_with_event_dataframe,
    quantity_name_dict,
    quantity_unit_dict,
    show_and_save_plot,
)
from paper_ppisne_scripts.settings import SN_type_dict as SN_type_dict_default
from paper_ppisne_scripts.settings import SN_type_dict_new as SN_type_dict_new

load_mpl_rc()

KAPPA_DEFAULT = 2.9
REDSHIFT_DEFAULT = 0.2


def add_text_to_primary_mass_distribution_plot(fig, ax, redshift, kappa):
    """
    Function to add text to the primary mass distribution plot
    """

    # Set redshift scaling multiplication factor
    redshift_scaling_factor = (1 + redshift) ** (kappa)

    # with plt.xkcd():
    ax.text(
        20,
        0.5 * 10**-1 * redshift_scaling_factor,
        "GWTC-3\nPL+Peak model",
        # "GWTC-3\nPL+Peak model\n(z={} \kappa={})".format(redshift, kappa),
        rotation=-30,
        horizontalalignment="right",
        verticalalignment="center",
        fontsize=20,
    )

    #
    a = patches.FancyArrowPatch(
        (20, 0.5 * 10**-1 * redshift_scaling_factor),
        (22, 0.65 * 10**-1 * redshift_scaling_factor),
        connectionstyle="arc3,rad=.4",
        arrowstyle=patches.ArrowStyle.Fancy(head_length=8, head_width=12, tail_width=2),
        color="k",
    )
    ax.add_patch(a)

    return fig, ax


def add_primary_mass_distribution_to_figure(
    fig, ax, mass_1, mass_1_ppd, CI_down, CI_up, label=None, fill_between_kwargs=None
):
    """
    Function to add the distribution to the figure
    """

    if fill_between_kwargs is None:
        fill_between_kwargs = {}

    # plot the PPD as a solid line
    ax.semilogy(
        mass_1,
        mass_1_ppd,
        label=label,
        alpha=0.75,
        **fill_between_kwargs,
    )

    # plot the CIs as a filled interval
    ax.fill_between(
        mass_1,
        CI_down,
        CI_up,
        alpha=0.5,
        # label=label,
        **fill_between_kwargs,
    )

    return fig, ax


#######################
# primary mass
def get_data_powerlaw_peak_primary_mass(data_root, redshift, limits, kappa):
    """
    Routine to get the data for the powerlaw peak estimates
    """

    # Get file and set limits
    mass_PP_path = os.path.join(
        data_root, "o1o2o3_mass_c_iid_mag_iid_tilt_powerlaw_redshift_mass_data.h5"
    )

    # Create mass grid
    mass_1 = np.linspace(2, 100, 1000)
    mass_ratio = np.linspace(0.1, 1, 500)

    # load in the traces.
    # Each entry in lines is p(m1 | Lambda_i) or p(q | Lambda_i)
    # where Lambda_i is a single draw from the hyperposterior
    # The ppd is a 2D object defined in m1 and q
    with open(mass_PP_path, "r") as _data:
        _data = dd.io.load(mass_PP_path)
        lines = _data["lines"]
        ppd = _data["ppd"]

    # Set redshift scaling multiplication factor
    redshift_scaling_factor = (1 + redshift) ** (kappa)

    # marginalize over q to get the ppd in terms of m1 only
    mass_1_ppd = np.trapz(ppd, mass_ratio, axis=0) * redshift_scaling_factor
    CI_down = (
        np.percentile(lines["mass_1"], limits[0], axis=0) * redshift_scaling_factor
    )
    CI_up = np.percentile(lines["mass_1"], limits[1], axis=0) * redshift_scaling_factor

    return_dict = {
        "mass_1": mass_1,
        "mass_1_ppd": mass_1_ppd,
        "mass_1_lines": lines["mass_1"],
        "CI_up": CI_up,
        "CI_down": CI_down,
    }

    return return_dict


def add_fraction_primary_underwent_ppisn_to_plot_function(
    fig,
    ax,
    convolved_dataset_results,
    bootstraps,
    linewidth,
    color_i,
    linestyle_i,
    show_ppisn_fractions_in_log,
):
    """
    Function to add the fraction of primary masses that underwent PPISNe to the plot
    """

    # Plot histograms
    if not bootstraps:
        ax.plot(
            convolved_dataset_results["primary_fraction_results"]["centers"],
            convolved_dataset_results["primary_fraction_results"]["rates"],
            lw=linewidth,
            c=color_i,
            zorder=200,
            alpha=1,
            linestyle=linestyle_i,
        )
    else:
        ax.plot(
            convolved_dataset_results["primary_fraction_results"]["centers"],
            convolved_dataset_results["primary_fraction_results"]["median_percentiles"][
                "median"
            ][0],
            lw=linewidth,
            c=color_i,
            zorder=13,
            linestyle=linestyle_i,
        )

        ax.fill_between(
            convolved_dataset_results["primary_fraction_results"]["centers"],
            convolved_dataset_results["primary_fraction_results"]["median_percentiles"][
                "90%_CI"
            ][0],
            convolved_dataset_results["primary_fraction_results"]["median_percentiles"][
                "90%_CI"
            ][1],
            alpha=0.4,
            zorder=11,
            color=color_i,
        )  # 90% confidence interval

    # Normal fraction plot:
    ax.set_ylim(-0.05, 1.05)

    # Display in log;
    if show_ppisn_fractions_in_log:
        values = convolved_dataset_results["primary_fraction_results"]["rates"]
        min_non_zero_value = np.min(values[values > 0])
        ax.set_ylim(min_non_zero_value, 1)
        ax.set_yscale("log")

    return fig, ax


def add_rates_to_plot_function(
    fig,
    ax,
    convolved_dataset,
    bootstraps,
    linewidth,
    color_i,
    linestyle_i,
    plot_settings,
):
    """
    Function to add the rates plot
    """

    convolved_dataset_results = convolved_dataset["data"]
    convolved_dataset_label = convolved_dataset["label"]
    convolved_dataset_annotation = convolved_dataset.get("annotation", None)

    # Add annotations
    if convolved_dataset_annotation is not None:
        ax.text(
            convolved_dataset_annotation["x"],
            convolved_dataset_annotation["y"],
            convolved_dataset_annotation["annotation"],
            color="black",
            horizontalalignment="center",
            verticalalignment="bottom",
            rotation=convolved_dataset_annotation["rotation"],
            **convolved_dataset_annotation["annotation_kwargs"],
            bbox=dict(
                facecolor="none",
                edgecolor=color_i,
                linewidth=5,
                boxstyle="round,pad=0.5",
            ),
        )

    # Add rate data
    if not bootstraps:
        # Plot histograms
        ax.plot(
            convolved_dataset_results["rates_results"]["centers"],
            convolved_dataset_results["rates_results"]["rates"],
            lw=linewidth,
            c=color_i,
            zorder=200,
            alpha=1,
            linestyle=linestyle_i,
            label=convolved_dataset_label,
        )

    else:
        # Plot median and bootstrap
        ax.plot(
            convolved_dataset_results["rates_results"]["centers"],
            convolved_dataset_results["rates_results"]["median_percentiles"]["median"][
                0
            ],
            lw=linewidth,
            c=color_i,
            zorder=13,
            linestyle=linestyle_i,
            label=convolved_dataset_label,
        )

        # fill between for the bounds
        ax.fill_between(
            convolved_dataset_results["rates_results"]["centers"],
            convolved_dataset_results["rates_results"]["median_percentiles"]["90%_CI"][
                0
            ],
            convolved_dataset_results["rates_results"]["median_percentiles"]["90%_CI"][
                1
            ],
            alpha=0.4,
            zorder=11,
            color=color_i,
        )  # 1-sigma

        # Plot step histogram
        if plot_settings.get("include_hist_step", False):
            ax.hist(
                convolved_dataset_results["rates_results"]["centers"],
                weights=convolved_dataset_results["rates_results"][
                    "median_percentiles"
                ]["median"][0],
                bins=convolved_dataset_results["rates_results"]["bins"],
                histtype="step",
                lw=linewidth,
                color=color_i,
                zorder=200,
                alpha=plot_settings.get("hist_step_alpha", 0.5),
                linestyle=linestyle_i,
            )

    # set ylims
    ax.set_ylim([1e-3, 1e1])

    # Make up
    ax.set_yscale("log")

    #
    return fig, ax


def get_median_percentiles(value_array):
    """
    Function to get the median and the percentiles from the data
    """

    result_dict = {}

    result_dict["median"] = np.percentile(value_array, [50], axis=0)

    result_dict["90%_CI"] = np.percentile(value_array, [5, 95], axis=0)

    # result_dict["1_sigma"] = np.percentile(value_array, [15.89, 84.1], axis=0)

    # result_dict["2_sigma"] = np.percentile(value_array, [2.27, 97.725], axis=0)

    return result_dict


def get_histogram_data(bins, data_array, weight_array):
    """
    Function to get the histogram data.

    Also returns the truncated bins where the ends containin only zeros are chopped off
    """

    # Determine the mass bins
    bin_size = np.diff(bins)
    bincenter = (bins[1:] + bins[:-1]) / 2

    # bin and take into account the divison by mass
    hist = np.histogram(data_array, bins=bins, weights=weight_array)[0]

    # Select the non-zero bins and split off the empty ones
    # NOTE: without this, toms method does not work
    non_zero_bins_indices = np.nonzero(hist)[0]

    if non_zero_bins_indices.size != 0:
        truncated_bins = bins[
            non_zero_bins_indices.min() : non_zero_bins_indices.max() + 1
        ]
    else:
        truncated_bins = bins
    return hist, bincenter, truncated_bins


def readout_rate_array_specific_keys(datafile, rate_key, mask, redshift_keys):
    """
    Function to read out the rate array

    input:
        datafile has to be an open HDF5 filehandle
    """

    # Turn redshift_keys into float_values
    redshifts_floats = [float(redshift_key) for redshift_key in redshift_keys]

    # Read out redshifts and systems to set up the array
    all_redshifts = np.array(sorted(list(datafile["data/{}".format(rate_key)].keys())))

    # Readout systems and mask to get the shape
    systems = datafile["data/{}/{}".format(rate_key, all_redshifts[0])][()]
    systems = systems[mask]

    # Set up merger rate array
    rate_array = np.zeros(shape=(len(redshift_keys), len(systems)))

    # Fill the array
    for redshift_i, redshift in enumerate(redshift_keys):
        # Readout rate data
        rate_data = datafile["data/{}/{}".format(rate_key, redshift)][()]

        # Mask data
        masked_rate_data = rate_data[mask]

        # Write to array
        rate_array[redshift_i, :] = masked_rate_data

    return rate_array, redshifts_floats


def get_closest_redshift_key(redshift_value, all_redshift_keys):
    """
    Function to get the redshift key that lies closest to the desired redshift
    """

    #
    all_redshift_floats = np.array(
        [float(redshift_key) for redshift_key in all_redshift_keys]
    )

    # calculate diff
    diff_redshift_floats = all_redshift_floats - redshift_value

    # min index
    argmin_arr = np.argmin(np.abs(diff_redshift_floats))

    closest_key = all_redshift_keys[argmin_arr]

    #
    return all_redshift_keys[argmin_arr]


def get_rate_data(filename, rate_type, redshift_value, mask):
    """
    Function to get rate data
    """

    datafile = h5py.File(filename)
    all_redshift_keys = sorted(list(datafile["data/{}".format(rate_type)].keys()))
    closest_redshift_key = get_closest_redshift_key(redshift_value, all_redshift_keys)
    rate_array, _ = readout_rate_array_specific_keys(
        datafile=datafile,
        rate_key=rate_type,
        mask=mask,
        redshift_keys=[closest_redshift_key],
    )
    datafile.close()

    return rate_array


def dco_type_query(dco_type):
    """
    Function to filter the correct dco type in a dataframe

    dco_type can be 'bhbh', 'bhns', 'nsns', 'combined_dco'
    """

    if dco_type == "bhbh":
        return "stellar_type_1 == 14 & stellar_type_2 == 14"
    if dco_type == "nsns":
        return "stellar_type_1 == 13 & stellar_type_2 == 13"
    if dco_type == "bhns":
        return "(stellar_type_1 == 14 & stellar_type_2 == 13) | (stellar_type_1 == 13 & stellar_type_2 == 14)"
    if dco_type == "combined_dco":
        return "mass_1==mass_1"
    else:
        msg = "dco_type '{}' is unknown. Abort".format(dco_type)
        raise ValueError(msg)


def get_mask(combined_df, dco_type, general_query, query):
    """
    function to make the mask
    """

    # NOTE: Combine the queries ?

    # Get masks
    dco_mask = combined_df.eval(dco_type_query(dco_type)).to_numpy()
    general_query_mask = (
        combined_df.eval(general_query)
        if general_query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    query_mask = (
        combined_df.eval(query)
        if query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    mask = dco_mask * general_query_mask * query_mask

    return mask


def weighted_avg_and_std(values, weights):
    """
    Return the weighted average and standard deviation.

    values, weights -- NumPy ndarrays with the same shape.
    """

    if np.sum(weights) > 0:
        average = np.average(values, weights=weights)
        # Fast and numerically precise:
        variance = np.average((values - average) ** 2, weights=weights)
    else:
        average = 0
        variance = 0

    return (average, math.sqrt(variance))


def get_data(
    filename,
    rate_type,
    dco_type,
    quantity,
    quantity_bins,
    divide_by_binsize,
    redshift_value,
    add_rates_plots=True,
    add_secondary_2d_plot=True,
    second_quantity=None,
    second_quantity_bins=None,
    add_third_2d_plot=False,
    third_quantity=None,
    third_quantity_bins=None,
    bootstraps=0,
    general_query=None,
    specific_query=None,
    querylist=None,
    add_eccentricity_1d=False,
    add_fraction_primary_underwent_ppisn_plot=False,
    add_fraction_secondary_underwent_ppisn_plot=False,
    add_comparison_to_observations=False,
    add_rlof_types_to_plot=False,
    # sn_dataset_filename=None,
    GW_ligo_obs_data_root="",
    sn_type_dict=SN_type_dict_default,
    verbose=0,
):
    """
    Function to read out the data from the datafile
    """

    # Invert SN type translation dict
    inverse_sn_type_dict = {value: key for key, value in sn_type_dict.items()}

    ####
    # Read out dataframe
    combined_df = pd.read_hdf(filename, "/data/combined_dataframes")

    print(combined_df.columns)

    # Add columns
    handle_columns(
        combined_df,
        quantity,
        querylist,  # TODO: properly pass the general query and the specific query of each dataset into this
        extra_columns=[
            "primary_undergone_ppisn",
            "secondary_undergone_ppisn",
            "secondary_mass",
            second_quantity,
            third_quantity,
        ],
    )

    # Create masks and apply, taking into account that the query and general query can be None
    # TODO: provide the correct querylist?
    mask = get_mask(combined_df, dco_type, general_query, query=specific_query)
    masked_combined_df = combined_df[mask]
    # print_rows_and_cols(masked_combined_df, "masked_combined_df")

    # Get the quantity data from the dataframe
    quantity_data = combined_df[quantity]
    primary_undergone_ppisn = combined_df["primary_undergone_ppisn"]
    secondary_undergone_ppisn = combined_df["secondary_undergone_ppisn"]

    cee_data = combined_df[mask]["comenv_counter"].to_numpy()
    smt_data = combined_df[mask]["stable_rlof_counter"].to_numpy()

    # mask the quantity data
    masked_quantity_data = quantity_data[mask].to_numpy()
    masked_primary_undergone_ppisn = primary_undergone_ppisn[mask].to_numpy()
    masked_secondary_undergone_ppisn = secondary_undergone_ppisn[mask].to_numpy()

    ################
    # Read out rate data for this specific redshift
    rate_array = get_rate_data(filename, rate_type, redshift_value, mask)

    ################
    # Construct information about the supernovae of the systems where the primary underwent a pulsational pair-instability supernova.
    #   We then select the supernovae that MADE the primary and get some statistics
    #
    #   We take the following steps to do this:
    #   - Load the supernovae dataframe from the hdf5 file
    #   - Merge the main dataframe with the supernovae dataframe. to access all information
    #   - Load the merger rates of those systems as weights
    #   - Select systems where the primary underwent a pulsational pair-instability supernova
    #   - Of these, select the supernovae that actually made the primary mass (by taking the entry in each uuid group that has the largest post-SN mass)
    #       TODO: this can be changed to selecting by starnum. We can get the starnum of the primary
    #   - For sanity we check some numbers (SN type, SN number etc)

    # load the SN df
    sn_df = pd.read_hdf(filename, "/data/events/SN_BINARY")

    # Merge the sn df with the masked combined df
    merged_sn_df = merge_with_event_dataframe(
        main_dataframe=masked_combined_df,
        event_dataframe=sn_df,
        event_dataframe_prefix="SN_EVENT_",
    )

    # Add the rates to the dataframe
    merged_sn_df["rates"] = rate_array[0][merged_sn_df["initial_indices"].to_numpy()]

    # Select systems where the primary black hole underwent PPISN
    merged_sn_df_where_primary_undergone_ppisn = merged_sn_df.query(
        "primary_undergone_ppisn == 1"
    )

    ###########
    # Create column to store whether the SN was the one that made the currently primary mass
    # NOTE: something is off with the id's. primary_sn_ids and secondary_sn_ids do not combine to all the ids? we only have two supernovae per system right?
    primary_sn_ids = merged_sn_df_where_primary_undergone_ppisn.groupby("uuid")[
        "SN_EVENT_post_SN_mass"
    ].idxmax()

    # Select the supernovae that lead to the primary mass
    supernovae_that_made_primary_black_hole_through_ppisne = (
        merged_sn_df_where_primary_undergone_ppisn.loc[primary_sn_ids]
    )

    # Get some general statistics for the SNe that created the primary mass. NOTE: this is as a sanity check to see if the queries we did are sensible.
    sn_numbers_that_made_primary = (
        supernovae_that_made_primary_black_hole_through_ppisne.groupby(
            ["SN_EVENT_SN_counter", "SN_EVENT_SN_type"]
        )["rates"].sum()
    )
    # print(sn_numbers_that_made_primary)

    ################
    # Calculate the rate histogram
    # if add_rates_plots:
    hist, bincenter, truncated_bins = get_histogram_data(
        bins=quantity_bins,
        data_array=masked_quantity_data,
        weight_array=rate_array[0],
    )

    # Divide by binsize
    if divide_by_binsize:
        hist = hist / np.diff(quantity_bins)

    #
    rates_return_dict = {
        "centers": bincenter,
        "rates": hist,
        "bins": quantity_bins,
    }

    ############
    # Handle comparison to observations setup
    if add_comparison_to_observations:
        # NOTE: This functionality is not available anymore
        raise NotImplementedError

        # Get all indices
        all_indices = np.arange(len(bincenter))

        # Select those that have rates above 0
        non_zero_indices = all_indices[hist > 0]
        non_zero_centers = bincenter[hist > 0]

        # Select those above 10
        indices_above_ten = non_zero_indices[non_zero_centers > 10]
        centers_above_ten = non_zero_centers[non_zero_centers > 10]

        #
        data_powerlaw_peak_primary_mass = get_data_powerlaw_peak_primary_mass(
            # data_root=os.path.join(os.environ["DATAFILES_ROOT"], "GW"),
            data_root=GW_ligo_obs_data_root,
        )

        (rates_comparison_values) = handle_comparison_to_observations(
            indices=indices_above_ten,
            centers=centers_above_ten,
            rates=hist[indices_above_ten],
            data_powerlaw_peak_primary_mass=data_powerlaw_peak_primary_mass,
        )

        # Set up return dict
        observation_comparison_return_dict = {
            "centers": centers_above_ten,
            "rates": rates_comparison_values,
        }

    ############
    # Handle fraction primary undergoing PPISN setup
    if add_fraction_primary_underwent_ppisn_plot:
        #####
        # Calculate the quantity and rate data where the primary underwent ppisn
        masked_quantity_data_where_primary_undergone_ppisn = masked_quantity_data[
            masked_primary_undergone_ppisn == 1
        ]
        rate_array_where_primary_undergone_ppisn = rate_array[
            :, masked_primary_undergone_ppisn == 1
        ]

        # Calculate the rate histogram for the primary having undergone ppisn
        (
            hist_where_primary_undergone_ppisn,
            bincenter_where_primary_undergone_ppisn,
            truncated_bins_where_primary_undergone_ppisn,
        ) = get_histogram_data(
            bins=quantity_bins,
            data_array=masked_quantity_data_where_primary_undergone_ppisn,
            weight_array=rate_array_where_primary_undergone_ppisn[0],
        )

        # divide by binsize
        if divide_by_binsize:
            hist_where_primary_undergone_ppisn = (
                hist_where_primary_undergone_ppisn / np.diff(quantity_bins)
            )

        # Calculate the ratio between the two
        ratio_to_total_hist_where_primary_undergone_ppisn = np.divide(
            hist_where_primary_undergone_ppisn,
            hist,
            out=hist_where_primary_undergone_ppisn,
            where=hist != 0,
        )  # only divide nonzeros else 1

        #
        primary_fraction_return_dict = {
            "centers": bincenter_where_primary_undergone_ppisn,
            "rates": ratio_to_total_hist_where_primary_undergone_ppisn,
        }

    ############
    # Handle secondary quantity setup
    if add_secondary_2d_plot:
        # Get the quantity data from the dataframe
        second_quantity_data = combined_df[second_quantity]

        # mask the quantity data
        second_masked_quantity_data = second_quantity_data[mask].to_numpy()

        # create histogram
        (second_quantity_2d_hist, _, _) = np.histogram2d(
            masked_quantity_data,
            second_masked_quantity_data,
            bins=[quantity_bins, second_quantity_bins],
            weights=rate_array[0],
        )

        # TODO: handle binsize division

        # Divide by the rates array to normalise
        second_quantity_2d_hist = (
            second_quantity_2d_hist
            / np.sum(second_quantity_2d_hist, axis=1)[:, np.newaxis]
        )

        #
        secondary_2d_plot_return_dict = {
            "rates": second_quantity_2d_hist,
        }

    ############
    # Handle secondary quantity setup
    if add_third_2d_plot:
        # Get the quantity data from the dataframe
        third_quantity_data = combined_df[third_quantity]

        # mask the quantity data
        third_masked_quantity_data = third_quantity_data[mask].to_numpy()

        # create histogram
        (third_quantity_2d_hist, _, _) = np.histogram2d(
            masked_quantity_data,
            third_masked_quantity_data,
            bins=[quantity_bins, third_quantity_bins],
            weights=rate_array[0],
        )

        # TODO: handle binsize division

        # Divide by the rates array to normalise
        third_quantity_2d_hist = (
            third_quantity_2d_hist
            / np.sum(third_quantity_2d_hist, axis=1)[:, np.newaxis]
        )

        #
        third_2d_plot_return_dict = {
            "rates": third_quantity_2d_hist,
        }

    ############
    # Handle rlof type quantity setupcolor_li
    if add_rlof_types_to_plot:
        # Set up
        rlof_types_return_dict = {}

        query_list = [
            {
                "query_indices": ((cee_data > 0) & (smt_data == 0)),
                "name": "cee",
                "longname": "CEE only",
            },
            {
                "query_indices": ((smt_data > 0) & (cee_data == 0)),
                "name": "smt",
                "longname": "SMT only",
            },
            {
                "query_indices": ((smt_data > 0) & (cee_data > 0)),
                "name": "both",
                "longname": "CEE & SMT",
            },
            {
                "query_indices": ((smt_data == 0) & (cee_data == 0)),
                "name": "neither",
                "longname": "No MT",
            },
        ]

        # Loop over different types
        for query in query_list:
            query_indices = query["query_indices"]

            masked_quantity_data_query = masked_quantity_data[query_indices]
            rate_array_data_query = rate_array[:, query_indices]

            # Calculate the rate histogram for the primary having undergone ppisn
            (
                hist_query,
                bincenter_hist_query,
                truncated_bins_query,
            ) = get_histogram_data(
                bins=quantity_bins,
                data_array=masked_quantity_data_query,
                weight_array=rate_array_data_query[0],
            )

            # divide by binsize
            if divide_by_binsize:
                hist_query = hist_query / np.diff(quantity_bins)

            rlof_types_return_dict[query["name"]] = {
                "rates": hist_query,
                "centers": bincenter_hist_query,
                "name": query["longname"],
            }

    ############
    # Handle secondary PPISN in primary mass bin
    if add_fraction_secondary_underwent_ppisn_plot:
        #####
        # Calculate the quantity and rate data where the secondary underwent ppisn
        masked_quantity_data_where_secondary_undergone_ppisn = masked_quantity_data[
            masked_secondary_undergone_ppisn == 1
        ]
        rate_array_where_secondary_undergone_ppisn = rate_array[
            :, masked_secondary_undergone_ppisn == 1
        ]

        # Calculate the rate histogram for the secondary having undergone ppisn
        (
            hist_where_secondary_undergone_ppisn,
            bincenter_where_secondary_undergone_ppisn,
            truncated_bins_where_secondary_undergone_ppisn,
        ) = get_histogram_data(
            bins=quantity_bins,
            data_array=masked_quantity_data_where_secondary_undergone_ppisn,
            weight_array=rate_array_where_secondary_undergone_ppisn[0],
        )

        # divide by binsize
        if divide_by_binsize:
            hist_where_secondary_undergone_ppisn = (
                hist_where_secondary_undergone_ppisn / np.diff(quantity_bins)
            )

        # Calculate the ratio between the two
        ratio_to_total_hist_where_secondary_undergone_ppisn = np.divide(
            hist_where_secondary_undergone_ppisn,
            hist,
            out=hist_where_secondary_undergone_ppisn,
            where=hist != 0,
        )  # only divide nonzeros else 1

        #
        secondary_fraction_return_dict = {
            "centers": bincenter_where_secondary_undergone_ppisn,
            "rates": ratio_to_total_hist_where_secondary_undergone_ppisn,
        }

    ############
    # Handle mean and stddev of eccentricity in each primary mass bin
    if add_eccentricity_1d:
        # Here we calculate the following things:
        #   - Eccentricity upon DCO formation
        #   - Eccentricity after the supernova that made the primary star
        #
        # For each of these we calculate the mean and standard deviation. Which will be used later.

        #############
        # get eccentricity data
        eccentricity_data = combined_df["eccentricity"]
        masked_eccentricity_data = eccentricity_data[mask].to_numpy()
        ecc_return_dict = {}

        mean_list = []
        stddev_list = []
        primary_sn_post_sn_eccentricities = []

        primary_sn_post_sn_mean_list = []
        primary_sn_post_sn_stddev_list = []

        # loop over bins
        # NOTE: this solution is not as efficient as I would like. preferably, i'd use a series of numpy operations but weighted statistics with things other than count are a bit iff. I need to write my own for that with digitize.
        for quantity_bin_i in range(len(quantity_bins) - 1):
            # select indices of primary mass fitting in this
            binned_quantity_indices = np.logical_and(
                masked_quantity_data > quantity_bins[quantity_bin_i],
                masked_quantity_data <= quantity_bins[quantity_bin_i + 1],
            )

            #
            mean, stddev = weighted_avg_and_std(
                values=masked_eccentricity_data[binned_quantity_indices],
                weights=rate_array[0][binned_quantity_indices],
            )

            # Store
            mean_list.append(mean)
            stddev_list.append(stddev)

            ######
            # Calculate the properties of the supernova that made the primary
            supernovae_that_made_primary_black_hole_in_bin = (
                supernovae_that_made_primary_black_hole_through_ppisne.query(
                    "{} > {} & {} <= {}".format(
                        quantity,
                        quantity_bins[quantity_bin_i],
                        quantity,
                        quantity_bins[quantity_bin_i + 1],
                    )
                )
            )

            # Calculate the weighted mean and stddev
            primary_sn_post_sn_eccentricities.append(
                supernovae_that_made_primary_black_hole_in_bin[
                    "SN_EVENT_post_SN_ecc"
                ].to_numpy()
            )
            primary_sn_post_sn_mean, primary_sn_post_sn_stddev = weighted_avg_and_std(
                values=supernovae_that_made_primary_black_hole_in_bin[
                    "SN_EVENT_post_SN_ecc"
                ].to_numpy(),
                weights=supernovae_that_made_primary_black_hole_in_bin[
                    "rates"
                ].to_numpy(),
            )

            # Store
            primary_sn_post_sn_mean_list.append(primary_sn_post_sn_mean)
            primary_sn_post_sn_stddev_list.append(primary_sn_post_sn_stddev)

        #####
        #
        ecc_return_dict = {
            "centers": bincenter,
            "bins": quantity_bins,
            "means": np.array(mean_list),
            "stddevs": np.array(stddev_list),
            "primary_sn_post_sn_means": np.array(primary_sn_post_sn_mean_list),
            "primary_sn_post_sn_stddevs": np.array(primary_sn_post_sn_stddev_list),
        }

    #########
    # Handle Bootstrap
    if bootstraps:
        # Get a list of indices
        indices = np.arange(len(masked_quantity_data))

        #########
        # Set up bootstrap array for rates:
        # if add_rates_plots:
        bootstrapped_rates_hist_vals = np.zeros(
            (bootstraps, len(rates_return_dict["centers"]))
        )  # center_bins

        #########
        # Set up bootstrap array for fraction undergoing PPISN:
        if add_fraction_primary_underwent_ppisn_plot:
            # Set up array that stores all the boostrapped results
            bootstrapped_fraction_primary_hist_vals = np.zeros(
                (bootstraps, len(primary_fraction_return_dict["centers"]))
            )  # center_bins

        #########
        # Set up bootstrap array for fraction secondary undergoing PPISN:
        if add_fraction_secondary_underwent_ppisn_plot:
            # Set up array that stores all the boostrapped results
            bootstrapped_fraction_secondary_hist_vals = np.zeros(
                (bootstraps, len(secondary_fraction_return_dict["centers"]))
            )  # center_bins

        #########
        # Set up bootstrap array for comparison to observation
        if add_comparison_to_observations:
            # Set up array that stores all the boostrapped results
            bootstrapped_observation_comparison_hist_vals = np.zeros(
                (bootstraps, len(observation_comparison_return_dict["centers"]))
            )  # center_bins

        ##########
        # Run bootstrap loop
        for bootstrap_i in range(bootstraps):
            if verbose:
                print("Bootstrap {}".format(bootstrap_i))
            ##############################
            # Get bootstrap indices
            boot_index = np.random.choice(
                indices,
                size=len(indices),
                replace=True,
                # p=rate_array[0][indices] / np.sum(rate_array[0][indices]),
            )

            #########
            # Calculate rates data with the bootstrapped set of indices
            # if add_rates_plots:
            # Select the quantity values with these indices
            bootstrapped_masked_quantity_data = masked_quantity_data[boot_index]

            # Select the rate values with these indices
            bootstrapped_rate_array = rate_array[:, boot_index]

            ##############################
            # Calculate the rate histogram
            (
                bootstrapped_hist,
                _,
                _,
            ) = get_histogram_data(
                bins=quantity_bins,
                data_array=bootstrapped_masked_quantity_data,
                weight_array=bootstrapped_rate_array[0],
            )

            # Store unfiltered rate in array
            bootstrapped_rates_hist_vals[bootstrap_i] = bootstrapped_hist

            #########
            # Calculate fraction undergoing PPISN data with the bootstrapped set of indices
            if add_fraction_primary_underwent_ppisn_plot:
                # Select the data for whether the primary underwent PPISN or not
                bootstrapped_masked_primary_undergone_ppisn = (
                    masked_primary_undergone_ppisn[boot_index]
                )

                # Calculate the quantity and rate data where the primary underwent ppisn
                bootstrapped_masked_quantity_data_where_primary_undergone_ppisn = (
                    bootstrapped_masked_quantity_data[
                        bootstrapped_masked_primary_undergone_ppisn == 1
                    ]
                )
                bootstrapped_rate_array_where_primary_undergone_ppisn = (
                    bootstrapped_rate_array[
                        :, bootstrapped_masked_primary_undergone_ppisn == 1
                    ]
                )

                # Calculate the rate histogram for the primary having undergone ppisn
                (
                    bootstrapped_hist_where_primary_undergone_ppisn,
                    _,
                    _,
                ) = get_histogram_data(
                    bins=quantity_bins,
                    data_array=bootstrapped_masked_quantity_data_where_primary_undergone_ppisn,
                    weight_array=bootstrapped_rate_array_where_primary_undergone_ppisn[
                        0
                    ],
                )

                # Calculate the ratio between the total rate and the one where primary underwent PPISN
                bootstrapped_ratio_to_total_hist_where_primary_undergone_ppisn = (
                    np.divide(
                        bootstrapped_hist_where_primary_undergone_ppisn,
                        bootstrapped_hist,
                        out=bootstrapped_hist_where_primary_undergone_ppisn,
                        where=bootstrapped_hist != 0,
                    )
                )  # only divide nonzeros else 1

                # Store in array
                bootstrapped_fraction_primary_hist_vals[
                    bootstrap_i
                ] = bootstrapped_ratio_to_total_hist_where_primary_undergone_ppisn

            #########
            # Calculate fraction undergoing secondary PPISN data with the bootstrapped set of indices
            if add_fraction_secondary_underwent_ppisn_plot:
                # Select the data for whether the secondary underwent PPISN or not
                bootstrapped_masked_secondary_undergone_ppisn = (
                    masked_secondary_undergone_ppisn[boot_index]
                )

                # Calculate the quantity and rate data where the secondary underwent ppisn
                bootstrapped_masked_quantity_data_where_secondary_undergone_ppisn = (
                    bootstrapped_masked_quantity_data[
                        bootstrapped_masked_secondary_undergone_ppisn == 1
                    ]
                )
                bootstrapped_rate_array_where_secondary_undergone_ppisn = (
                    bootstrapped_rate_array[
                        :, bootstrapped_masked_secondary_undergone_ppisn == 1
                    ]
                )

                # Calculate the rate histogram for the secondary having undergone ppisn
                (
                    bootstrapped_hist_where_secondary_undergone_ppisn,
                    _,
                    _,
                ) = get_histogram_data(
                    bins=quantity_bins,
                    data_array=bootstrapped_masked_quantity_data_where_secondary_undergone_ppisn,
                    weight_array=bootstrapped_rate_array_where_secondary_undergone_ppisn[
                        0
                    ],
                )

                # Calculate the ratio between the total rate and the one where secondary underwent PPISN
                bootstrapped_ratio_to_total_hist_where_secondary_undergone_ppisn = (
                    np.divide(
                        bootstrapped_hist_where_secondary_undergone_ppisn,
                        bootstrapped_hist,
                        out=bootstrapped_hist_where_secondary_undergone_ppisn,
                        where=bootstrapped_hist != 0,
                    )
                )  # only divide nonzeros else 1

                # Store in array
                bootstrapped_fraction_secondary_hist_vals[
                    bootstrap_i
                ] = bootstrapped_ratio_to_total_hist_where_secondary_undergone_ppisn

            #########
            # Calculate comparison to observation with the bootstrapped set of indices
            if add_comparison_to_observations:
                rates_comparison_values = handle_comparison_to_observations(
                    indices=indices_above_ten,
                    centers=centers_above_ten,
                    rates=bootstrapped_hist[indices_above_ten],
                    data_powerlaw_peak_primary_mass=data_powerlaw_peak_primary_mass,
                )
                bootstrapped_observation_comparison_hist_vals[
                    bootstrap_i
                ] = rates_comparison_values

        ###########
        # Calculate median and percentiles
        # if add_rates_plots:
        bootstrapped_median_percentiles_dict = get_median_percentiles(
            bootstrapped_rates_hist_vals
        )
        rates_return_dict["median_percentiles"] = bootstrapped_median_percentiles_dict

        ###########
        # Calculate median and percentiles for the ratio which undergoes ppisn
        if add_fraction_primary_underwent_ppisn_plot:
            bootstrapped_primary_fraction_median_percentiles_dict = (
                get_median_percentiles(bootstrapped_fraction_primary_hist_vals)
            )

            # Store in results
            primary_fraction_return_dict[
                "median_percentiles"
            ] = bootstrapped_primary_fraction_median_percentiles_dict

        ###########
        # Calculate median and percentiles for the ratio which undergoes ppisn
        if add_fraction_secondary_underwent_ppisn_plot:
            bootstrapped_fraction_secondary_median_percentiles_dict = (
                get_median_percentiles(bootstrapped_fraction_secondary_hist_vals)
            )

            # Store in results
            secondary_fraction_return_dict[
                "median_percentiles"
            ] = bootstrapped_fraction_secondary_median_percentiles_dict

        ###########
        # Calculate median and percentiles for the observation comparison
        if add_comparison_to_observations:
            bootstrapped_observation_comparison_median_percentiles_dict = (
                get_median_percentiles(bootstrapped_observation_comparison_hist_vals)
            )

            # Store in results
            observation_comparison_return_dict[
                "median_percentiles"
            ] = bootstrapped_observation_comparison_median_percentiles_dict

    ############
    # Set up return dict
    return_dict = {}

    if add_rates_plots:
        return_dict["rates_results"] = rates_return_dict
    if add_fraction_primary_underwent_ppisn_plot:
        return_dict["primary_fraction_results"] = primary_fraction_return_dict
    if add_comparison_to_observations:
        return_dict[
            "observation_comparison_results"
        ] = observation_comparison_return_dict
    if add_secondary_2d_plot:
        return_dict["secondary_2d_plot_results"] = secondary_2d_plot_return_dict
    if add_third_2d_plot:
        return_dict["third_2d_plot_results"] = third_2d_plot_return_dict
    if add_rlof_types_to_plot:
        return_dict["rlof_type_plot_results"] = rlof_types_return_dict
    if add_fraction_secondary_underwent_ppisn_plot:
        return_dict["secondary_fraction_results"] = secondary_fraction_return_dict
    if add_eccentricity_1d:
        return_dict["eccentricity_results"] = ecc_return_dict

    return return_dict


def generate_primary_mass_at_redshift_zero_plot(
    convolved_datasets,
    primary_mass_bins,
    divide_by_binsize,
    add_rates_plots=True,
    add_eccentricity_1d=False,
    add_ratio_to_fiducial_plot=False,
    add_residual_to_fiducial_plot=False,
    add_fraction_primary_underwent_ppisn_plot=False,
    add_fraction_secondary_underwent_ppisn_plot=False,
    add_LVK_observations=True,
    show_ppisn_fractions_in_log=False,
    add_comparison_to_observations=False,
    add_rlof_types_to_plot=False,
    add_secondary_2d_plot=False,
    second_quantity=None,
    second_quantity_bins=None,
    add_third_2d_plot=False,
    third_quantity=None,
    third_quantity_bins=None,
    fill_between_rate_plot=False,
    fill_between_rate_plot_names=None,
    bootstraps=0,
    general_query=None,
    redshift_value=0.2,
    # sn_datasets=None,
    plot_settings={},
    GW_ligo_obs_data_root="",
    verbose=1,
    title=None,
    figsize=None,
):
    """
    Function to plot the paper plot for the primary mass at zero redshift comparison between variations
    """

    # Handle custom color list
    if plot_settings.get("custom_color_list", None) is None:
        cmap = plt.cm.get_cmap(
            plot_settings.get("colormap_variations", "viridis"),
            max(len(convolved_datasets) - (2), 0),
        )

        # Add other color type at the end
        color_list = [plot_settings.get("color_first_distribution", "orange")]
        color_list.append("#669999")
        color_list += [cmap(i) for i in range(cmap.N)]
    else:
        color_list = plot_settings["custom_color_list"]

    #
    global linestyle_list
    if plot_settings.get("custom_linestyle_list", None) is not None:
        linestyle_list = plot_settings["custom_linestyle_list"]

    #####################################
    # Calculate the results
    for convolved_dataset_i, convolved_dataset_name in enumerate(
        convolved_datasets.keys()
    ):
        # Unpack a bit
        convolved_dataset = convolved_datasets[convolved_dataset_name]
        convolved_dataset_filename = convolved_dataset["filename"]
        convolved_dataset_specific_query = convolved_dataset.get("query", None)

        # Get data and store
        convolved_dataset["data"] = get_data(
            filename=convolved_dataset_filename,
            rate_type="merger_rate",
            dco_type="bhbh",
            quantity="primary_mass",
            quantity_bins=primary_mass_bins,
            divide_by_binsize=divide_by_binsize,
            redshift_value=redshift_value,
            bootstraps=bootstraps,
            general_query=general_query,
            specific_query=convolved_dataset_specific_query,
            add_rates_plots=add_rates_plots,
            add_eccentricity_1d=add_eccentricity_1d,
            add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
            add_fraction_secondary_underwent_ppisn_plot=add_fraction_secondary_underwent_ppisn_plot,
            add_comparison_to_observations=add_comparison_to_observations,
            add_rlof_types_to_plot=add_rlof_types_to_plot,
            add_secondary_2d_plot=add_secondary_2d_plot,
            second_quantity=second_quantity,
            second_quantity_bins=second_quantity_bins,
            add_third_2d_plot=add_third_2d_plot,
            third_quantity=third_quantity,
            third_quantity_bins=third_quantity_bins,
            GW_ligo_obs_data_root=GW_ligo_obs_data_root,
            # sn_dataset_filename=sn_datasets[convolved_dataset_key]
            # if add_eccentricity_1d
            # else None,
            verbose=verbose,
        )

    # Settings
    linewidth = 5
    fig_scale = 15
    gs_scale = 1
    levels = (
        (2 * add_rates_plots)
        + add_ratio_to_fiducial_plot
        + add_residual_to_fiducial_plot
        + add_fraction_primary_underwent_ppisn_plot
        + add_fraction_secondary_underwent_ppisn_plot
        + add_comparison_to_observations
        + add_rlof_types_to_plot
        + add_secondary_2d_plot
        + add_third_2d_plot
        + (add_eccentricity_1d * 4)
    )
    level = 0

    #########
    # Set up figure logic
    # TODO: please move this entirely to a different function
    if figsize is None:
        fig = plt.figure(figsize=(1.5 * fig_scale, 1 * fig_scale))
    else:
        fig = plt.figure(figsize=figsize)

    gs = fig.add_gridspec(nrows=levels * gs_scale, ncols=1)
    axes_list = []

    # Add rates plot
    if add_rates_plots:
        # set up the subplot
        rates_axis = fig.add_subplot(gs[level * gs_scale : (level + 2) * gs_scale, 0])
        level += 2
        axes_list.append(rates_axis)

        ylabel = r"$\mathrm{d}\mathcal{R}/\mathrm{d}m_{1} [\mathrm{Gpc}^{-3}\,\mathrm{yr}^{-1}\,\mathrm{M}_{\odot}^{-1}]$"  # ligo plot
        rates_axis.set_ylabel(
            ylabel,
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

    # Add eccentricity plot
    if add_eccentricity_1d:
        ecc_axis = fig.add_subplot(gs[level * gs_scale : (level + 2) * gs_scale, 0])
        level += 2
        axes_list.append(ecc_axis)

        if plot_settings.get("log_eccentricity_minus_one", False):
            ecc_axis.set_yscale("log")
            ylabel = r"$\log_{10}$(1-Eccentricity) at" + "\n" + r"DCO formation"
        else:
            ylabel = r"Eccentricity at" + "\n" + r"DCO formation"
            ecc_axis.set_yscale("linear")
            ecc_axis.set_ylim(0, 1)
        ecc_axis.set_ylabel(
            ylabel,
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

    # Add ratio to fiducial to plot
    if add_ratio_to_fiducial_plot:
        ratio_to_fiducial_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(ratio_to_fiducial_axis)

        ratio_to_fiducial_axis.set_yticks([0, 0.5, 1.0])
        ratio_to_fiducial_axis.set_ylabels(["0.0", "0.5", "1.0"])

    # add residual to fiducial plot:
    if add_residual_to_fiducial_plot:
        residual_to_fiducial_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(residual_to_fiducial_axis)

    # Add fraction of primary that underwent PPISN
    if add_fraction_primary_underwent_ppisn_plot:
        fraction_primary_underwent_ppisn_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(fraction_primary_underwent_ppisn_axis)

        #
        fraction_primary_underwent_ppisn_axis.set_ylabel(
            r"$\mathcal{F}_{\mathrm{PPISN,\ primary}}$",
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

        fraction_primary_underwent_ppisn_axis.set_yticks([0, 0.5, 1.0])
        fraction_primary_underwent_ppisn_axis.set_yticklabels(["0.0", "0.5", "1.0"])

    # Add eccentricity plot
    if add_eccentricity_1d:
        ecc_axis_2 = fig.add_subplot(gs[level * gs_scale : (level + 2) * gs_scale, 0])
        level += 2
        axes_list.append(ecc_axis_2)

        if plot_settings.get("log_eccentricity_minus_one", False):
            ecc_axis_2.set_yscale("log")
            ylabel = (
                r"Post-SN $\log_{10}$(1-eccentricity)" + "\n" + "(primary=PPISN)"
            )  # TODO: change this label
        else:
            ylabel = (
                r"Post-SN eccentricity" + "\n" + "(primary=PPISN)"
            )  # TODO: change this label
            ecc_axis_2.set_yscale("linear")
            ecc_axis_2.set_ylim(0, 1)
        ecc_axis_2.set_ylabel(
            ylabel,
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

    # Add fraction of secondary that underwent PPISN
    if add_fraction_secondary_underwent_ppisn_plot:
        fraction_secondary_underwent_ppisn_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(fraction_secondary_underwent_ppisn_axis)

        #
        fraction_secondary_underwent_ppisn_axis.set_ylabel(
            "Fraction secondary\n underwent PPISN",
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

    # Add comparison to observations axis
    if add_comparison_to_observations:
        comparison_to_observations_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(comparison_to_observations_axis)

    # Add secondary 2d plot axis
    if add_secondary_2d_plot:
        secondary_2d_plot_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(secondary_2d_plot_axis)

        #
        secondary_2d_plot_axis.set_ylabel(
            "{} {}".format(
                quantity_name_dict[second_quantity],
                "[{}]".format(quantity_unit_dict[second_quantity].to_string("latex"))
                if quantity_unit_dict[second_quantity] != dimensionless_unit
                else "",
            ),
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

    # Add secondary 2d plot axis
    if add_third_2d_plot:
        third_2d_plot_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(third_2d_plot_axis)

        #
        third_2d_plot_axis.set_ylabel(
            "{} {}".format(
                quantity_name_dict[third_quantity],
                "[{}]".format(quantity_unit_dict[third_quantity].to_string("latex"))
                if quantity_unit_dict[third_quantity] != dimensionless_unit
                else "",
            ),
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

    # Add plot containing RLOF types
    if add_rlof_types_to_plot:
        rlof_type_plot_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(rlof_type_plot_axis)

        #
        rlof_type_plot_axis.set_ylabel(
            "RLOF type fraction", fontsize=plot_settings.get("axislabel_fontsize", 26)
        )

    #####################################
    # Plot the results
    # TODO please move this entirely to a different function
    for convolved_dataset_i, convolved_dataset_name in enumerate(
        convolved_datasets.keys()
    ):
        # Unpack a bit
        convolved_dataset = convolved_datasets[convolved_dataset_name]
        convolved_dataset_filename = convolved_dataset["filename"]
        convolved_dataset_specific_query = convolved_dataset.get("query", None)
        convolved_dataset_results = convolved_dataset["data"]
        convolved_dataset_label = convolved_dataset["label"]

        #
        color_i = color_list[convolved_dataset_i]
        linestyle_i = linestyle_list[convolved_dataset_i]
        hatch_i = hatch_list[convolved_dataset_i]

        ######
        # Handle rates plots
        if add_rates_plots and plot_settings.get("show_data", True):
            fig, rates_axis = add_rates_to_plot_function(
                fig=fig,
                ax=rates_axis,
                convolved_dataset=convolved_dataset,
                #                convolved_dataset_results=convolved_dataset_results,
                #                convolved_dataset_label=convolved_dataset_label,
                bootstraps=bootstraps,
                linewidth=linewidth,
                color_i=color_i,
                linestyle_i=linestyle_i,
                plot_settings=plot_settings,
            )

        ######
        # Handle eccentricity plot
        if add_eccentricity_1d:
            #############
            # Here we plot the following:
            # - Eccentricity upon DCO formation
            #   - mean as a line
            #   - standard deviation as a filled-in region

            # Plot data linearly
            if not plot_settings.get("log_eccentricity_minus_one"):
                ecc_axis.plot(
                    convolved_dataset_results["eccentricity_results"]["centers"],
                    convolved_dataset_results["eccentricity_results"]["means"],
                    lw=linewidth,
                    c=color_i,
                    zorder=200,
                    alpha=1,
                    linestyle=linestyle_i,
                    label=result_name,
                )

                # Fill between the mean +- stddev
                ecc_axis.fill_between(
                    convolved_dataset_results["eccentricity_results"]["centers"],
                    convolved_dataset_results["eccentricity_results"]["means"]
                    - convolved_dataset_results["eccentricity_results"]["stddevs"],
                    convolved_dataset_results["eccentricity_results"]["means"]
                    + convolved_dataset_results["eccentricity_results"]["stddevs"],
                    alpha=0.25,
                    # zorder=11,
                    # color=color_i,
                    hatch=hatch_i,
                    facecolor=color_i,
                    edgecolor="#666666",
                    linewidth=0.0,
                )

                ## PRIMARY SN
                # Plot mean
                ecc_axis_2.errorbar(
                    convolved_dataset_results["eccentricity_results"]["centers"],
                    convolved_dataset_results["eccentricity_results"][
                        "primary_sn_post_sn_means"
                    ],
                    yerr=convolved_dataset_results["eccentricity_results"][
                        "primary_sn_post_sn_stddevs"
                    ],
                    lw=linewidth,
                    c=color_i,
                    zorder=200,
                    alpha=1,
                    linestyle=linestyle_i,
                    # label=result_name,
                )
            else:
                ecc_axis.plot(
                    convolved_dataset_results["eccentricity_results"]["centers"],
                    1 - convolved_dataset_results["eccentricity_results"]["means"],
                    lw=linewidth,
                    c=color_i,
                    zorder=200,
                    alpha=1,
                    linestyle=linestyle_i,
                    label=result_name,
                )

                # Fill between the mean +- stddev
                ecc_axis.fill_between(
                    convolved_dataset_results["eccentricity_results"]["centers"],
                    1
                    - (
                        convolved_dataset_results["eccentricity_results"]["means"]
                        - convolved_dataset_results["eccentricity_results"]["stddevs"]
                    ),
                    1
                    - (
                        convolved_dataset_results["eccentricity_results"]["means"]
                        + convolved_dataset_results["eccentricity_results"]["stddevs"]
                    ),
                    alpha=0.25,
                    # zorder=11,
                    # color=color_i,
                    hatch=hatch_i,
                    facecolor=color_i,
                    edgecolor="#666666",
                    linewidth=0.0,
                )

                ## PRIMARY SN
                # Plot mean
                ecc_axis_2.errorbar(
                    convolved_dataset_results["eccentricity_results"]["centers"],
                    1
                    - convolved_dataset_results["eccentricity_results"][
                        "primary_sn_post_sn_means"
                    ],
                    yerr=convolved_dataset_results["eccentricity_results"][
                        "primary_sn_post_sn_stddevs"
                    ],
                    lw=linewidth,
                    c=color_i,
                    zorder=200,
                    alpha=1,
                    linestyle=linestyle_i,
                )

        ######
        # Handle fraction primary that underwent PPISN plots
        if add_fraction_primary_underwent_ppisn_plot and plot_settings.get(
            "show_data", True
        ):
            (
                fig,
                fraction_primary_underwent_ppisn_axis,
            ) = add_fraction_primary_underwent_ppisn_to_plot_function(
                fig=fig,
                ax=fraction_primary_underwent_ppisn_axis,
                convolved_dataset_results=convolved_dataset_results,
                bootstraps=bootstraps,
                linewidth=linewidth,
                color_i=color_i,
                linestyle_i=linestyle_i,
                show_ppisn_fractions_in_log=show_ppisn_fractions_in_log,
            )

        ######
        # Handle fraction secondary that underwent PPISN plots
        if add_fraction_secondary_underwent_ppisn_plot and plot_settings.get(
            "show_data", True
        ):
            # Plot histograms
            fraction_secondary_underwent_ppisn_axis.plot(
                convolved_dataset_results["secondary_fraction_results"]["centers"],
                convolved_dataset_results["secondary_fraction_results"]["rates"],
                lw=linewidth,
                c=color_i,
                zorder=200,
                alpha=1,
                linestyle=linestyle_i,
            )

            if bootstraps:
                fraction_secondary_underwent_ppisn_axis.plot(
                    convolved_dataset_results["secondary_fraction_results"]["centers"],
                    convolved_dataset_results["secondary_fraction_results"][
                        "median_percentiles"
                    ]["median"][0],
                    lw=linewidth,
                    c=color_i,
                    zorder=13,
                )

                fraction_secondary_underwent_ppisn_axis.fill_between(
                    convolved_dataset_results["secondary_fraction_results"]["centers"],
                    convolved_dataset_results["secondary_fraction_results"][
                        "median_percentiles"
                    ]["90%_CI"][0],
                    convolved_dataset_results["secondary_fraction_results"][
                        "median_percentiles"
                    ]["90%_CI"][1],
                    alpha=0.4,
                    zorder=11,
                    color=color_i,
                )  # 90% confidence interval

            # Normal fraction plot:
            fraction_secondary_underwent_ppisn_axis.set_ylim(0, 1)

            # Display in log;
            if show_ppisn_fractions_in_log:
                values = convolved_dataset_results["secondary_fraction_results"][
                    "rates"
                ]
                min_non_zero_value = np.min(values[values > 0])
                fraction_secondary_underwent_ppisn_axis.set_ylim(min_non_zero_value, 1)
                fraction_secondary_underwent_ppisn_axis.set_yscale("log")

        ######
        # Add fraction of primary underwent PPISN
        if add_comparison_to_observations and plot_settings.get("show_data", True):
            #
            comparison_to_observations_axis.set_ylabel(
                r"$\mathrm{log10}\left(\|\frac{\mathrm{obs}-\mathrm{mod}}{\mathrm{obs}}\|\right)$",
                fontsize=plot_settings.get("axislabel_fontsize", 26),
            )

            if bootstraps:
                # Plot median and bootstrap
                comparison_to_observations_axis.plot(
                    convolved_dataset_results["observation_comparison_results"][
                        "centers"
                    ],
                    np.log10(
                        convolved_dataset_results["observation_comparison_results"][
                            "median_percentiles"
                        ]["median"][0]
                    ),
                    lw=linewidth,
                    c=color_i,
                    zorder=13,
                    linestyle=linestyle_i,
                    label=convolved_dataset_label,
                )

                #
                comparison_to_observations_axis.fill_between(
                    convolved_dataset_results["observation_comparison_results"][
                        "centers"
                    ],
                    np.log10(
                        convolved_dataset_results["observation_comparison_results"][
                            "median_percentiles"
                        ]["90%_CI"][0]
                    ),
                    np.log10(
                        convolved_dataset_results["observation_comparison_results"][
                            "median_percentiles"
                        ]["90%_CI"][1]
                    ),
                    alpha=0.4,
                    zorder=11,
                    color=color_i,
                )  # 1-sigma

        ######
        # Add plot containing secondary (2-d) plot
        if add_secondary_2d_plot and plot_settings.get("show_data", True):
            secondary_2d_plot_data = convolved_dataset_results[
                "secondary_2d_plot_results"
            ]["rates"]

            norm = colors.LogNorm(
                vmin=1e-2,
                vmax=1e3,
            )

            norm = colors.Normalize(
                vmin=np.min(secondary_2d_plot_data[secondary_2d_plot_data > 0]), vmax=1
            )

            cmap = copy.copy(plt.cm.viridis)
            cmap.set_under(color="white")

            quantity_bins_centers = convolved_dataset_results["rates_results"][
                "centers"
            ]
            second_quantity_bins_centers = (
                second_quantity_bins[1:] + second_quantity_bins[:-1]
            ) / 2

            quantity_bins_centers_X, second_quantity_bins_centers_Y = np.meshgrid(
                quantity_bins_centers, second_quantity_bins_centers
            )

            _ = secondary_2d_plot_axis.pcolormesh(
                quantity_bins_centers_X,
                second_quantity_bins_centers_Y,
                secondary_2d_plot_data.T,
                norm=norm,
                cmap=cmap,
                shading="auto",
                antialiased=plot_settings.get("antialiased", True),
                rasterized=plot_settings.get("rasterized", True),
            )

        ######
        # Add plot containing third (2-d) plot
        if add_third_2d_plot and plot_settings.get("show_data", True):
            third_2d_plot_data = convolved_dataset_results["third_2d_plot_results"][
                "rates"
            ]

            norm = colors.LogNorm(
                vmin=1e-2,
                vmax=1e3,
            )

            norm = colors.Normalize(
                vmin=np.min(third_2d_plot_data[third_2d_plot_data > 0]), vmax=1
            )

            cmap = copy.copy(plt.cm.viridis)
            cmap.set_under(color="white")

            quantity_bins_centers = convolved_dataset_results["rates_results"][
                "centers"
            ]
            third_quantity_bins_centers = (
                third_quantity_bins[1:] + third_quantity_bins[:-1]
            ) / 2

            quantity_bins_centers_X, third_quantity_bins_centers_Y = np.meshgrid(
                quantity_bins_centers, third_quantity_bins_centers
            )

            _ = third_2d_plot_axis.pcolormesh(
                quantity_bins_centers_X,
                third_quantity_bins_centers_Y,
                third_2d_plot_data.T,
                norm=norm,
                cmap=cmap,
                shading="auto",
                antialiased=plot_settings.get("antialiased", True),
                rasterized=plot_settings.get("rasterized", True),
            )

        ######
        # Add plot containing the RLOF type fractions
        if add_rlof_types_to_plot and plot_settings.get("show_data", True):
            fig, rlof_type_plot_axis = add_RLOF_types_to_plot_function(
                fig=fig,
                ax=rlof_type_plot_axis,
                convolved_dataset_results=convolved_dataset_results,
                primary_mass_bins=primary_mass_bins,
                color_list_rlofs=color_list_rlofs,
            )

    ######
    # Fill between
    if (
        fill_between_rate_plot
        and add_rates_plots
        and plot_settings.get("show_data", True)
    ):
        fig, rates_axis = add_fill_between_rates_to_plot_function(
            fig=fig,
            ax=rates_axis,
            convolved_datasets=convolved_datasets,
            fill_between_rate_plot_names=fill_between_rate_plot_names,
        )

    ####
    # Add confidence interval of observations
    if add_LVK_observations:
        fig, axes_list[0] = add_confidence_interval_powerlaw_peak_primary_mass(
            fig=fig,
            ax=axes_list[0],
            data_root=os.path.join(os.environ["DATAFILES_ROOT"], "GW"),
            fill_between_kwargs=plot_settings.get(
                "observations_fill_between_kwargs", {}
            ),
            add_text=plot_settings.get("add_GW_text", False),
            redshift=redshift_value,
        )

    ##############
    # General make up of the plots

    # handle inset text
    if "insert_text_main_panel" in plot_settings:
        axes_list[0].text(
            x=0.5,
            y=0.9,
            s=plot_settings["insert_text_main_panel"],
            horizontalalignment="center",
            verticalalignment="center",
            bbox=dict(facecolor="none", edgecolor="black", alpha=0.5),
            transform=axes_list[0].transAxes,
            fontsize=plot_settings.get("insert_text_fontsize", 26),
        )

    if add_eccentricity_1d:
        align_axes(fig=fig, axes_list=[ecc_axis, ecc_axis_2], which_axis="y")

    # limits
    for axes in axes_list:
        axes.set_xlim(2, 100)

    # Override with custom xlims
    if "custom_xlims" in plot_settings:
        for axis in axes_list:
            axis.set_xlim(
                [plot_settings["custom_xlims"][0], plot_settings["custom_xlims"][1]]
            )

    # set ylims
    if add_rates_plots:
        rates_axis.set_ylim([1e-3, 1e1])

    # labels
    for axes in axes_list[:-1]:
        axes.set_xticklabels([])

    # Extra makeup
    axes_list[-1].set_xlabel(
        "{} [{}]".format(
            quantity_name_dict["primary_mass"],
            quantity_unit_dict["primary_mass"].to_string("latex"),
        ),
        fontsize=plot_settings.get("axislabel_fontsize", 26),
    )
    legend = axes_list[0].legend(
        fontsize=plot_settings.get("legend_fontsize", 26),
        loc=plot_settings.get("legend_location", "best"),
        **plot_settings.get("legend_kwarg_dict", {}),
    )
    legend.set_zorder(500)

    #########
    # Add plot dependent makeup
    if add_comparison_to_observations:
        #
        region_of_interest_bounds = np.array([32, 38])

        # Add plot to band of interest
        comparison_to_observations_axis_ylims = (
            comparison_to_observations_axis.get_ylim()
        )
        comparison_to_observations_axis.axvspan(
            region_of_interest_bounds[0],
            region_of_interest_bounds[1],
            ymin=comparison_to_observations_axis_ylims[0],
            ymax=comparison_to_observations_axis_ylims[1],
            alpha=0.1,
            color="red",
        )

        # Add line of threshold
        comparison_to_observations_axis.axhline(-1, alpha=0.5, linestyle="--")

    ###############
    # Handle adding the subpanel indicators
    add_labels_subplots(
        fig,
        axes_list,
        label_function_kwargs={
            "x_loc": 0.01,
            "bbox_dict": {"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
            **plot_settings.get("subplot_label_kwargs", {}),
        },
    )

    if title is not None:
        axes_list[0].set_title(title)

    # Align y-labels
    fig.align_ylabels(axes_list)

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def add_confidence_interval_powerlaw_peak_primary_mass(
    fig,
    ax,
    data_root,
    add_text=False,
    label=None,
    fill_between_kwargs=None,
    limits=[5, 95],
    redshift=REDSHIFT_DEFAULT,
    kappa=KAPPA_DEFAULT,
):
    """
    Function to add the confidence interval for the powerlaw + peak model of the GWTC03b data release for the primary mass distribution

    data_root has to contain the file "o1o2o3_mass_c_iid_mag_iid_tilt_powerlaw_redshift_mass_data.h5"
    """

    #
    if fill_between_kwargs is None:
        fill_between_kwargs = {}

    # get data
    data_powerlaw_peak_primary_mass = get_data_powerlaw_peak_primary_mass(
        data_root=data_root, redshift=redshift, limits=limits, kappa=kappa
    )

    # unpack
    mass_1 = data_powerlaw_peak_primary_mass["mass_1"]
    mass_1_ppd = data_powerlaw_peak_primary_mass["mass_1_ppd"]
    CI_up = data_powerlaw_peak_primary_mass["CI_up"]
    CI_down = data_powerlaw_peak_primary_mass["CI_down"]

    fig, ax = add_primary_mass_distribution_to_figure(
        fig=fig,
        ax=ax,
        mass_1=mass_1,
        mass_1_ppd=mass_1_ppd,
        CI_down=CI_down,
        CI_up=CI_up,
        label=label,
        fill_between_kwargs=fill_between_kwargs,
    )

    # Add text to plot
    if add_text:
        fig, ax = add_text_to_primary_mass_distribution_plot(
            fig=fig, ax=ax, redshift=redshift, kappa=kappa
        )

    return fig, ax
