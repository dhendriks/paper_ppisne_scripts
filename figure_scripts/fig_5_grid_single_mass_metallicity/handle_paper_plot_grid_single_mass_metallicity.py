"""
Function to plot a multi-panel with different variations
"""

import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import colors

from paper_ppisne_scripts.figure_scripts.fig_5_grid_single_mass_metallicity.utils import (
    fill_missing_values,
)
from paper_ppisne_scripts.figure_scripts.utils import (
    add_labels_subplots,
    create_bins_from_centers,
    linestyle_list,
    load_mpl_rc,
    show_and_save_plot,
)
from paper_ppisne_scripts.settings import (
    Farmer_citation,
    format_COshift,
    format_extraML,
)

load_mpl_rc()

this_file_dir = os.path.dirname(os.path.abspath(__file__))

fontsizes = 32


def plot_initial_final(dataset_dict, custom_colorlimits=None, plot_settings=None):
    """
    Function to plot a 3 panel with the different variations
    """

    linestyles_list = ["solid", "dashed", "-."]

    #
    if plot_settings is None:
        plot_settings = {}

    # get number of datasets
    num_datasets = len(list(dataset_dict.keys()))

    if num_datasets != 4:
        raise ValueError("currently only working with 4 datasets")

    ####
    # create Figure and axes
    fig = plt.figure(figsize=(20, 18))
    gs = fig.add_gridspec(nrows=8, ncols=10)

    ax_fiducial = fig.add_subplot(gs[:4, 0:4])
    ax_farmer = fig.add_subplot(gs[:4, 4:8])

    ax_mass_loss_variation = fig.add_subplot(gs[4:, 0:4])
    ax_core_mass_shift_variation = fig.add_subplot(gs[4:, 4:8])

    axes_list = [
        ax_fiducial,
        ax_farmer,
        ax_mass_loss_variation,
        ax_core_mass_shift_variation,
    ]

    # Create invisible axis
    ax_invisible = fig.add_subplot(gs[:, :8], frame_on=False)
    ax_invisible.set_xticks([])
    ax_invisible.set_yticks([])

    #
    ax_cb = fig.add_subplot(gs[:, -1:])

    #
    global_zmin = 1e90
    global_zmax = -1e90
    res_dict = {}

    # Loop over the datasets and plot the info
    for i, key in enumerate(dataset_dict.keys()):
        # Open the data as a pandas dataframe
        df = pd.read_csv(dataset_dict[key]["filename"], header=0, sep="\s+")
        df["log10metallicity"] = np.log10(df["metallicity"])
        df = df.sort_values(by=["zams_mass", "log10metallicity"])
        other_columns = [
            col for col in df.columns if not col in ["zams_mass", "metallicity"]
        ]

        # Fill missing values
        df = fill_missing_values(df, other_columns)

        # Get unique values
        unique_zams_mass_values = df["zams_mass"].sort_values().unique()
        unique_metallicity_values = df["metallicity"].sort_values().unique()
        unique_log10unique_metallicity_values = np.log10(unique_metallicity_values)

        z_vals = (
            df["mass"]
            .to_numpy()
            .reshape(
                len(unique_zams_mass_values), len(unique_log10unique_metallicity_values)
            )
        )
        sn_type_vals = (
            df["sn_type"]
            .to_numpy()
            .reshape(
                len(unique_zams_mass_values), len(unique_log10unique_metallicity_values)
            )
        )

        # update global zmin and zmax
        global_zmin = np.min([global_zmin, z_vals[~np.isnan(z_vals)].min()])
        global_zmax = np.max([global_zmax, z_vals[~np.isnan(z_vals)].max()])

        # Store in result dict
        res_dict[i] = {
            "unique_metallicity_values": unique_metallicity_values,
            "unique_zams_mass_values": unique_zams_mass_values,
            "z_vals": z_vals,
            "sn_type_vals": sn_type_vals,
        }

    # Put the colorlimits and set the colorbar
    if not custom_colorlimits is None:
        norm = colors.Normalize(vmin=custom_colorlimits[0], vmax=custom_colorlimits[1])
    else:
        norm = colors.Normalize(vmin=global_zmin, vmax=global_zmax + 2)

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        norm=norm,
    )
    cbar.ax.set_ylabel(r"Remnant mass [M$_{\odot}$]", fontsize=fontsizes)

    # Loop over the datasets and plot the info
    linestyle_list_i = 0
    for i, key in enumerate(dataset_dict.keys()):
        ax = axes_list[i]
        # Set up the colormesh plot
        _ = ax.pcolormesh(
            res_dict[i]["unique_metallicity_values"],
            res_dict[i]["unique_zams_mass_values"],
            res_dict[i]["z_vals"],
            norm=norm,
            shading="auto",
            antialiased=True,
            rasterized=True,
        )

        # Get max value
        zvals = res_dict[i]["z_vals"]
        zvals_non_nan = zvals[~np.isnan(zvals)]
        zvals_max = zvals_non_nan.max()
        zvals_max_margin = 1

        # Put contour around PPISN
        _ = ax.contour(
            res_dict[i]["unique_metallicity_values"],
            res_dict[i]["unique_zams_mass_values"],
            res_dict[i]["sn_type_vals"],
            levels=[20, 21, 22],
            colors="red",
            linewidths=2,
        )

        # Put contour around max mass
        _ = ax.contour(
            res_dict[i]["unique_metallicity_values"],
            res_dict[i]["unique_zams_mass_values"],
            res_dict[i]["z_vals"],
            levels=[zvals_max - zvals_max_margin, zvals_max + zvals_max_margin],
            colors="black",
            linewidths=2,
            linestyles=["solid", "dashed", "dashdot", "dotted"][linestyle_list_i],
        )

        # Plot on colorbar
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [zvals_max] * 2,
            "black",
            linestyle=linestyle_list[linestyle_list_i],
            linewidth=4,
        )

        if i != 1:
            linestyle_list_i += 1

        # Set x-scale to log
        ax.set_xscale("log")
        ax.set_title(dataset_dict[key]["title"], fontsize=fontsizes)
        ax.set_ylim([50, ax.get_ylim()[-1]])

    # Manually remove and add some thins to the plot
    axes_list[0].text(2e-4, 250, "PISN", color="r")
    axes_list[0].text(2e-4, 160, "PPISN", color="r")
    axes_list[0].text(2e-4, 80, "CC", color="r")

    axes_list[0].set_xticklabels([])
    axes_list[1].set_xticklabels([])
    axes_list[1].set_yticklabels([])
    axes_list[3].set_yticklabels([])

    # add subplots
    add_labels_subplots(
        fig,
        axes_list,
        label_function_kwargs={
            "x_loc": 0.05,
            "bbox_dict": {"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
        },
    )

    #
    ax_invisible.set_xlabel(
        r"Metallicity $\mathrm{log}_{10}(Z)$", labelpad=50, fontsize=fontsizes
    )
    ax_invisible.set_ylabel(r"ZAMS mass [M$_{\odot}$]", labelpad=80, fontsize=fontsizes)

    fig.tight_layout()

    # Handle saving
    show_and_save_plot(fig, plot_settings)


def handle_paper_plot_grid_single_mass_metallicity(result_dir, output_name, show_plot):
    """
    Function to handle the paper version of the single star evolution mass metallicity grid plot
    """

    # #
    # data_dir = os.path.join(this_file_dir, "output")

    # Create dataset dictionary
    dataset_dict = {
        "fiducial": {
            "filename": os.path.join(result_dir, "output_vanilla/data/output.dat"),
            "title": "Fiducial",
        },
        "farmer19": {
            "filename": os.path.join(
                result_dir, "output_robs_prescription/data/output.dat"
            ),
            "title": Farmer_citation,
        },
        "extra_massloss": {
            "filename": os.path.join(
                result_dir, "output_ten_solarmass_extra_removed/data/output.dat"
            ),
            "title": format_extraML(10),
        },
        "core_mass_shift": {
            "filename": os.path.join(
                result_dir, "output_five_core_mass_shift/data/output.dat"
            ),
            "title": format_COshift(-5),
        },
    }

    # Actual call
    plot_initial_final(
        dataset_dict,
        plot_settings={
            "show_plot": show_plot,
            "output_name": output_name,
        },
    )


if __name__ == "__main__":
    #
    show_plot = False

    #
    this_file = os.path.abspath(__file__)
    this_file_dir = os.path.dirname(this_file)

    # NOTE: make sure 'paper_PPISNe_Hendriks2023_data_dir' environment variable is set or change this line
    result_dir = os.path.join(
        os.getenv("paper_PPISNe_Hendriks2023_data_dir"),
        "grid_single_mass_metallicity_data/",
    )

    ########
    # Output dir
    output_dir = os.path.join("plots")

    #
    base_output_name = "test.pdf"
    handle_paper_plot_grid_single_mass_metallicity(
        result_dir=result_dir,
        output_name=os.path.join(output_dir, base_output_name),
        show_plot=show_plot,
    )
