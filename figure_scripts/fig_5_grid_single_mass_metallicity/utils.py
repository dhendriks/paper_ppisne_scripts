import numpy as np
import pandas as pd


def fill_missing_values(df, other_columns, verbose=0):
    """
    Function to fill the missing values and return an updated dataframe

    In some situations the model fails, and we have a hole in our grid. We want to replace this hole by NaN values.
    """

    unique_zams_mass_values = df["zams_mass"].sort_values().unique()
    unique_metallicity_values = df["metallicity"].sort_values().unique()

    expected_lines = len(unique_zams_mass_values) * len(unique_metallicity_values)
    missing_lines = expected_lines - len(df.index)

    #
    if verbose:
        print(
            "The current df has {} entries, but we expect {} entries. We're missing {} lines.".format(
                len(df.index), expected_lines, missing_lines
            )
        )

    # create new df
    new_df = pd.DataFrame()

    # Loop over the metallicities

    # Check if there are metallicity values for which not all of the zams masses are registered/successfully evolved
    for met in unique_metallicity_values:
        cur_met = df[df.metallicity == met]

        # Find missing zams values
        cur_zams_values = set(df[df.metallicity == met]["zams_mass"])
        missing_zams_values = set(unique_zams_mass_values) - cur_zams_values

        if verbose:
            print(
                "At metallicity {} we are missing {} values (of total {})".format(
                    met, len(missing_zams_values), len(unique_zams_mass_values)
                )
            )

        # Create placeholder data
        if missing_zams_values:
            dummy_lines = []
            for zams_value in missing_zams_values:
                # Create dummy line
                dummy_line = {
                    "metallicity": met,
                    "zams_mass": zams_value,
                    "log10metallicity": np.log10(met),
                    **{
                        col: np.nan
                        for col in other_columns
                        if not col == "log10metallicity"
                    },
                }
                dummy_lines.append(dummy_line)

            # Create dataframe
            placeholder_df = pd.DataFrame(dummy_lines)

            # Add to current metallicity df
            cur_met = pd.concat([cur_met, placeholder_df], ignore_index=True)

        # select only those that are the same as our unique masses, just to be sure
        cur_met = cur_met.query("zams_mass in @unique_zams_mass_values")

        # It is possible that there are duplicate lines in the data. Somehow, not sure how, but apparently so.
        cur_met = cur_met.drop_duplicates(subset=["zams_mass", "log10metallicity"])

        if verbose:
            print(
                "current metallicity ({}) (after appending) now has {} unique masses and {} lines".format(
                    met, len(cur_met["zams_mass"].unique()), len(cur_met.index)
                )
            )

        # Add current metallicity to new df
        new_df = pd.concat([new_df, cur_met], ignore_index=True)

    if verbose:
        print(
            "The final df has {} lines. We are expecting {}.".format(
                len(new_df.index), expected_lines
            )
        )

    # # It is possible that there are duplicate lines in the data. Somehow, not sure how, but apparently so.
    # new_df = new_df.drop_duplicates(subset=["zams_mass", "log10metallicity"])

    # Make sure the data is sorted again
    new_df = new_df.sort_values(by=["zams_mass", "log10metallicity"])

    # do a check. we're expecting that the df now contains enough values to span a rectangle
    assert len(new_df.index) == len(unique_zams_mass_values) * len(
        unique_metallicity_values
    )

    return new_df
