"""
Functions that handle the plots for the paper
"""

import os

from paper_ppisne_scripts.figure_scripts.fig_4_transients_vs_redshift.utils import (
    plot_total_rates_individual_sn_types,
)
from paper_ppisne_scripts.figure_scripts.utils import load_mpl_rc
from paper_ppisne_scripts.settings import (
    Farmer_citation,
    format_COshift,
    format_extraML,
)

load_mpl_rc()

#
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def handle_paper_plot_total_rates_individual_sn_types(
    result_root, output_name, show_plot
):
    """
    Function to handle the paper version of the transient rate density vs redshift plot
    """

    from grav_waves.settings import SN_type_dict_new as SN_type_dict_new

    # Define the dataset
    dataset_dict = {
        "fiducial": {
            "binary_dco_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
            "binary_sn_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/sn_convolution_results/convolved_sn_dataset.h5",
            "single_sn_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/single_sn_convolution_results/convolved_sn_dataset.h5",
            "name": "Fid.",
            "add_variation_to_legend": True,
            "linestyle": "solid",
            "add_DCO_to_legend": True,
            "DCO_legend_suffix": "excluding CE",
            "add_SN_to_legend": True,
            "dco_query": "comenv_counter == 0",
        },
        "variation": {
            "binary_dco_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
            "binary_sn_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/sn_convolution_results/convolved_sn_dataset.h5",
            "single_sn_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/single_sn_convolution_results/convolved_sn_dataset.h5",
            "name": format_COshift(-15),
            "add_variation_to_legend": True,
            "linestyle": "--",
            "add_DCO_to_legend": False,
            "add_SN_to_legend": False,
            "dco_query": "comenv_counter == 0",
        },
        "second_variation": {
            "binary_dco_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_10_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
            "binary_sn_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_10_PPISN_core_mass_range_shift/sn_convolution_results/convolved_sn_dataset.h5",
            "single_sn_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_10_PPISN_core_mass_range_shift/single_sn_convolution_results/convolved_sn_dataset.h5",
            "name": format_COshift(10),
            "add_variation_to_legend": True,
            "linestyle": "-.",
            "add_DCO_to_legend": False,
            "add_SN_to_legend": False,
            "dco_query": "comenv_counter == 0",
        },
        ############
        # with CE
        "fiducial_with_CE": {
            "binary_dco_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
            "name": "Fid.",
            "add_variation_to_legend": False,
            "linestyle": "solid",
            "add_DCO_to_legend": True,
            "DCO_legend_suffix": "including CE",
            "add_SN_to_legend": True,
            "DCO_marker": "x",
            "DCO_marker_linewidth": 1,
            "add_SN_to_legend": False,
        },
        "variation_with_CE": {
            "binary_dco_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
            "name": format_COshift(-15),
            "add_variation_to_legend": True,
            "linestyle": "--",
            "add_variation_to_legend": False,
            "add_DCO_to_legend": False,
            "DCO_marker": "x",
            "DCO_marker_linewidth": 1,
            "add_SN_to_legend": False,
        },
        "second_variation_with_CE": {
            "binary_dco_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_10_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
            "name": format_COshift(10),
            "add_variation_to_legend": True,
            "linestyle": "--",
            "add_variation_to_legend": False,
            "add_DCO_to_legend": False,
            "DCO_marker": "x",
            "DCO_marker_linewidth": 1,
            "add_SN_to_legend": False,
        },
    }

    # extra config
    include_single = False
    event_based = True
    filter_fb = True
    add_observational_rates = True
    SN_type_filter = {
        "CCSN": True,
        #
        "PPISN": False,
        "PPISN_h_free": True,
        "PPISN_h_rich": False,
        #
        "PISN": False,
        "PISN_h_free": True,
        "PISN_h_rich": False,
    }
    DCO_type_filter = {"nsns": False, "bhns": False, "bhbh": True}
    legend_fontsize = 16

    SN_type_additional_marker_dict = {
        "CCSN": "v",
        "PPISN_h_free": "$\u2215$",
        "PISN_h_free": "|",
    }

    #########
    plot_total_rates_individual_sn_types(
        dataset_dict=dataset_dict,
        # data switches
        include_single=include_single,
        event_based=event_based,
        filter_fb=filter_fb,
        split_hydrogen_for_PPISNe_and_PISNe=True,
        # plot switches
        add_DCO_merger_rates=True,
        add_SN_transient_rates=True,
        add_fill_between_PPISNe_and_PISNe=True,
        # Filters
        DCO_type_filter=DCO_type_filter,
        SN_type_filter=SN_type_filter,
        #
        add_variation_dco_rates=True,
        add_second_variation_dco_rates=True,
        add_fiducial_transient_rates=True,
        add_variation_transient_rates=True,
        add_second_variation_transient_rates=True,
        add_transient_observational_rates=True,
        add_gw_observational_rates=True,
        add_other_observational_rates=True,
        add_SN_types_to_line=True,
        # dco_query=dco_query,
        plot_settings={
            "output_name": output_name,
            "axislabel_fontsize": 28,
            "show_plot": show_plot,
            "SN_type_text_fontsize": legend_fontsize,
        },
        SN_type_dict=SN_type_dict_new,
        # additional markers for the SN types
        add_additional_SN_markers=True,
        SN_type_additional_marker_dict=SN_type_additional_marker_dict,
        ################
        # legend
        # observations
        add_observation_to_legend=True,
        observation_legend_config={
            "loc": 1,
            "framealpha": 0.75,
            "frameon": True,
            "numpoints": 1,
            "fontsize": legend_fontsize,
        },
        # DCO
        add_DCO_to_legend=True,
        DCO_legend_config={
            "loc": 8,
            "bbox_to_anchor": (0.75, 0.025),
            "fontsize": legend_fontsize,
        },
        # variation
        add_variation_to_legend=True,
        variation_legend_config={
            "loc": 8,
            "bbox_to_anchor": (0.25, 0.025),
            "fontsize": legend_fontsize,
            "handlelength": 3,
        },
        # SN transient
        add_SN_to_legend=True,
        SN_legend_config={
            "loc": 8,
            "bbox_to_anchor": (0.5, 0.025),
            "ncol": 1,
            "fontsize": legend_fontsize,
        },
        figsize=(18, 9),
    )


def handle_paper_plot_total_rates_individual_sn_types_local(
    result_root, output_name, show_plot
):
    """
    Function to handle the paper version of the transient rate density vs redshift plot
    """

    from grav_waves.settings import SN_type_dict as SN_type_dict_new

    # Define the dataset
    dataset_dict = {
        "fiducial": {
            # Filenames
            "binary_dco_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
            "binary_sn_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/sn_convolution_results/convolved_sn_dataset.h5",
            "single_sn_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/single_sn_convolution_results/convolved_sn_dataset.h5",
            "name": "Fid.",
            "add_variation_to_legend": True,
            "linestyle": "solid",
            "add_DCO_to_legend": True,
            "DCO_legend_suffix": "excluding CE",
            "add_SN_to_legend": True,
            "dco_query": "comenv_counter == 0",
        },
        "variation": {
            "binary_dco_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
            "binary_sn_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/sn_convolution_results/convolved_sn_dataset.h5",
            "single_sn_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/single_sn_convolution_results/convolved_sn_dataset.h5",
            "name": format_COshift(-15),
            "add_variation_to_legend": True,
            "linestyle": "--",
            "add_DCO_to_legend": False,
            "add_SN_to_legend": False,
            "dco_query": "comenv_counter == 0",
        },
        ########
        # Second set for DCO only
        "fiducial_with_CE": {
            # Filenames
            "binary_dco_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
            "name": "Fid.",
            "add_variation_to_legend": False,
            "linestyle": "solid",
            "add_DCO_to_legend": True,
            "DCO_legend_suffix": "including CE",
            "DCO_marker": "x",
            "DCO_marker_linewidth": 1,
            "add_SN_to_legend": False,
        },
        "variation_with_CE": {
            "binary_dco_filename": result_root
            + "_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
            "name": format_COshift(-15),
            "add_variation_to_legend": False,
            "linestyle": "--",
            "add_DCO_to_legend": False,
            "DCO_marker": "x",
            "DCO_marker_linewidth": 1,
            "add_SN_to_legend": False,
        },
    }

    # extra config
    include_single = False
    event_based = True
    filter_fb = True
    add_observational_rates = True
    add_other_observational_rates = True
    SN_type_filter = {
        "CCSN": True,
        #
        "PPISN": False,
        "PPISN_h_free": True,
        "PPISN_h_rich": False,
        #
        "PISN": False,
        "PISN_h_free": True,
        "PISN_h_rich": False,
    }
    DCO_type_filter = {"nsns": False, "bhns": False, "bhbh": True}
    legend_fontsize = 16

    SN_type_additional_marker_dict = {
        "CCSN": "v",
        "PPISN_h_free": "$\u2215$",
        "PISN_h_free": "|",
    }

    #########
    plot_total_rates_individual_sn_types(
        dataset_dict=dataset_dict,
        # data switches
        include_single=include_single,
        event_based=event_based,
        filter_fb=filter_fb,
        split_hydrogen_for_PPISNe_and_PISNe=True,
        # plot switches
        add_DCO_merger_rates=True,
        add_SN_transient_rates=True,
        add_fill_between_PPISNe_and_PISNe=True,
        # Filters
        DCO_type_filter=DCO_type_filter,
        SN_type_filter=SN_type_filter,
        #
        add_variation_dco_rates=True,
        add_second_variation_dco_rates=True,
        add_fiducial_transient_rates=True,
        add_variation_transient_rates=True,
        add_second_variation_transient_rates=True,
        add_transient_observational_rates=True,
        add_gw_observational_rates=True,
        add_other_observational_rates=True,
        add_SN_types_to_line=True,
        # dco_query=dco_query,
        plot_settings={
            "output_name": output_name,
            "axislabel_fontsize": 32,
            "show_plot": show_plot,
            "SN_type_text_fontsize": legend_fontsize,
        },
        SN_type_dict=SN_type_dict_new,
        # additional markers for the SN types
        add_additional_SN_markers=True,
        SN_type_additional_marker_dict=SN_type_additional_marker_dict,
        ################
        # legend
        # observations
        add_observation_to_legend=True,
        observation_legend_config={
            "loc": 1,
            "framealpha": 0.75,
            "frameon": True,
            "numpoints": 1,
            "fontsize": legend_fontsize,
        },
        # DCO
        add_DCO_to_legend=True,
        DCO_legend_config={
            "loc": 8,
            "bbox_to_anchor": (0.75, 0.025),
            "fontsize": legend_fontsize,
        },
        # variation
        add_variation_to_legend=True,
        variation_legend_config={
            "loc": 8,
            "bbox_to_anchor": (0.25, 0.025),
            "fontsize": legend_fontsize,
            "handlelength": 3,
        },
        # SN transient
        add_SN_to_legend=True,
        SN_legend_config={
            "loc": 8,
            "bbox_to_anchor": (0.5, 0.025),
            "ncol": 1,
            "fontsize": legend_fontsize,
        },
    )


if __name__ == "__main__":
    #########
    # NOTE: make sure 'paper_PPISNe_Hendriks2023_data_dir' environment variable is set or change this line
    result_root = os.path.join(
        os.getenv("paper_PPISNe_Hendriks2023_data_dir"),
        "population_data/EVENTS_V2.2.2_LOW_RES",
    )

    # paper plot version test
    result_root = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"), "GRAV_WAVES/server_results/" + "LOW_MID_RES"
    )

    result_root = os.path.join(
        os.getenv("paper_PPISNe_Hendriks2023_data_dir"), "population_data/LOW_MID_RES"
    )

    #########
    #
    plot_dir = "plots"

    #
    # handle_paper_plot_total_rates_individual_sn_types
    handle_paper_plot_total_rates_individual_sn_types_local(
        result_root=result_root,
        output_name=os.path.join(this_file_dir, "plots/fig_4_transients_vs_.pdf"),
        show_plot=False,
    )
