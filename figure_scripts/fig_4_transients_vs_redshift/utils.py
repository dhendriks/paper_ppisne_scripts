import copy
import warnings

import astropy.units as u
import h5py
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.lines import Line2D
from scipy import interpolate

from paper_ppisne_scripts.figure_scripts.fig_4_transients_vs_redshift.observation_sets import (
    observations_sets,
    other_observation_sets,
)
from paper_ppisne_scripts.figure_scripts.utils import show_and_save_plot

warnings.simplefilter("ignore", UserWarning)

color_list = ["blue", "green", "orange", "yellow", "pink", "red"]
hatch_alpha = 0.1
used_rate_unit = 1 / u.yr / u.Gpc**3


def plot_total_rates_individual_sn_types(
    dataset_dict,
    # switches
    include_single,
    event_based,
    filter_fb,
    split_hydrogen_for_PPISNe_and_PISNe,
    #
    add_DCO_merger_rates,
    add_SN_transient_rates,
    add_fill_between_PPISNe_and_PISNe,
    # Filters
    DCO_type_filter,
    SN_type_filter,
    # TODO: remove these
    add_variation_dco_rates,
    add_second_variation_dco_rates,
    add_fiducial_transient_rates,
    add_variation_transient_rates,
    add_second_variation_transient_rates,
    add_transient_observational_rates,
    add_gw_observational_rates,
    add_other_observational_rates,
    plot_settings=None,
    SN_type_dict=None,
    add_additional_SN_markers=False,
    SN_type_additional_marker_dict=None,
    # queries
    global_query=None,
    dco_query=None,
    sn_binary_query=None,
    sn_single_query=None,
    #####
    ## Legend config
    # Observations
    add_observation_to_legend=True,
    observation_legend_config=None,
    # DCO
    add_DCO_to_legend=True,
    DCO_legend_config=None,
    # SN
    add_SN_to_legend=True,
    SN_legend_config=None,
    # variation
    add_variation_to_legend=True,
    variation_legend_config=None,
    #####
    # Misc
    add_SN_types_to_line=True,
    print_SN_rates=True,
    figsize=None,
):
    """
    function to plot total rate over redshift for individual SN types
    """

    plot_settings = fix_empty_default_dict(plot_settings)

    sn_rate_color_dict = {
        "CCSN": "blue",
        #
        "PPISN": "green",
        "PPISN_h_free": "green",
        "PPISN_h_rich": "#a0e7a0",
        #
        "PISN": "orange",
        "PISN_h_free": "orange",
        "PISN_h_rich": "#ffcc99",
    }

    #
    nrows = 10000

    #
    dataset_dict = copy.deepcopy(dataset_dict)

    # remove datasets
    dataset_dict = remove_datasets(
        dataset_dict=dataset_dict,
        add_DCO_merger_rates=add_DCO_merger_rates,
        add_variation_dco_rates=add_variation_dco_rates,
        add_second_variation_dco_rates=add_second_variation_dco_rates,
        add_fiducial_transient_rates=add_fiducial_transient_rates,
        add_variation_transient_rates=add_variation_transient_rates,
        add_second_variation_transient_rates=add_second_variation_transient_rates,
    )

    ######################
    # Loop over the datasets and extract the data
    dataset_dict = generate_all_rates(
        dataset_dict=dataset_dict,
        SN_type_dict=SN_type_dict,
        event_based=event_based,
        filter_fb=filter_fb,
        include_single=include_single,
        split_hydrogen_for_PPISNe_and_PISNe=split_hydrogen_for_PPISNe_and_PISNe,
        #
        DCO_type_filter=DCO_type_filter,
        SN_type_filter=SN_type_filter,
        #
        global_query=global_query,
        dco_query=dco_query,
        sn_binary_query=sn_binary_query,
        sn_single_query=sn_single_query,
        nrows=nrows,
    )

    ##################
    # Set up figure logic
    if figsize is None:
        fig = plt.figure(figsize=(18, 9))
    else:
        fig = plt.figure(figsize=figsize)

    #
    gs = fig.add_gridspec(nrows=1, ncols=9)
    ax = fig.add_subplot(gs[:, :])

    ############
    # Add fill-between for the (P)PISNe
    if add_fill_between_PPISNe_and_PISNe:
        fig, ax = add_fill_between_PPISNe_and_PISNe_function(
            fig=fig,
            ax=ax,
            dataset_dict=dataset_dict,
            split_hydrogen_for_PPISNe_and_PISNe=split_hydrogen_for_PPISNe_and_PISNe,
            sn_rate_color_dict=sn_rate_color_dict,
        )

    #################
    # Add rates
    fig, ax, DCO_scatter_sets, sn_rate_objs, obs_objects = add_rates_function(
        fig=fig,
        ax=ax,
        dataset_dict=dataset_dict,
        add_DCO_merger_rates=add_DCO_merger_rates,
        add_SN_transient_rates=add_SN_transient_rates,
        add_transient_observational_rates=add_transient_observational_rates,
        add_gw_observational_rates=add_gw_observational_rates,
        add_other_observational_rates=add_other_observational_rates,
        #
        DCO_type_filter=DCO_type_filter,
        SN_type_filter=SN_type_filter,
        #
        sn_rate_color_dict=sn_rate_color_dict,
        # add extra SN markers
        add_additional_SN_markers=add_additional_SN_markers,
        SN_type_additional_marker_dict=SN_type_additional_marker_dict,
    )

    # #############
    # # Print some information about the rates
    # print_SN_rates_function(
    #     dataset_dict=dataset_dict,
    #     print_SN_rates=print_SN_rates,
    #     SN_type_filter=SN_type_filter,
    # )

    ############
    # handle the legends
    fig, ax = handle_legends_functions(
        fig=fig,
        ax=ax,
        # Observations
        add_observation_to_legend=add_observation_to_legend,
        observation_legend_config=observation_legend_config,
        obs_objects=obs_objects,
        # DCO
        add_DCO_to_legend=add_DCO_to_legend,
        DCO_legend_config=DCO_legend_config,
        DCO_scatter_sets=DCO_scatter_sets,
        # SN
        add_SN_to_legend=add_SN_to_legend,
        SN_legend_config=SN_legend_config,
        sn_rate_objs=sn_rate_objs,
        # variation
        add_variation_to_legend=add_variation_to_legend,
        variation_legend_config=variation_legend_config,
        dataset_dict=dataset_dict,
        #
        add_additional_SN_markers=add_additional_SN_markers,
        SN_type_additional_marker_dict=SN_type_additional_marker_dict,
        sn_rate_color_dict=sn_rate_color_dict,
    )

    ##############
    # make up
    ax.set_xlim([-0.8, 8])
    if "custom_xlim" in plot_settings.keys():
        ax.set_xlim(plot_settings["custom_xlim"])

    ax.set_ylim([1e-1, 10**6.5])
    if "custom_ylim" in plot_settings.keys():
        ax.set_ylim(plot_settings["custom_ylim"])

    ax.set_yscale("log")

    ############
    # add SN types next to lines
    if add_SN_types_to_line:
        fig, ax = add_SN_type_text_function(
            fig=fig,
            ax=ax,
            dataset_dict=dataset_dict,
            sn_rate_color_dict=sn_rate_color_dict,
            SN_type_filter=SN_type_filter,
            plot_settings=plot_settings,
        )
        ax.set_yscale("log")

    ################
    # set ylabel
    ylabel = "Event rate\n"
    ylabel += r"$\mathcal{R}_{event}(\it{z})\ \left[\mathrm{d}^{2}\,\it{N}_{\mathrm{event}}/\mathrm{d}\it{t}\,\mathrm{d}\it{V}_{\mathrm{c}}\right]\ \left(\mathrm{Gpc^{-3}\,yr^{-1}}\right)$"
    ax.set_ylabel(
        ylabel,
        fontsize=plot_settings.get("axislabel_fontsize", 26),
    )

    # Set xlabel
    xlabel = r"Event redshift [$\it{z}$]"
    ax.set_xlabel(
        xlabel,
        fontsize=plot_settings.get("axislabel_fontsize", 26),
    )

    #
    fig, ax = delete_out_of_frame_text(fig, ax)

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def fix_empty_default_dict(default_dict):
    """
    Function to handle legend config default
    """

    if default_dict is None:
        return {}
    return default_dict


def remove_datasets(
    dataset_dict,
    add_DCO_merger_rates,
    add_variation_dco_rates,
    add_second_variation_dco_rates,
    add_fiducial_transient_rates,
    add_variation_transient_rates,
    add_second_variation_transient_rates,
):
    """
    Function to handle removing some datasets
    """

    ###########################
    # remove the calculation for the rates of the first variation if we dont want to add them
    if (not add_DCO_merger_rates) and (not add_fiducial_transient_rates):
        if "fiducial" in dataset_dict.keys():
            del dataset_dict["fiducial"]
            print("DELETING")

    # remove the calculation for the rates of the first variation if we dont want to add them
    if (not add_variation_dco_rates) and (not add_variation_transient_rates):
        if "variation" in dataset_dict.keys():
            del dataset_dict["variation"]

    # remove the calculation for the rates of the second variation if we don't want to add them
    if (not add_second_variation_dco_rates) and (
        not add_second_variation_transient_rates
    ):
        if "second_variation" in dataset_dict.keys():
            del dataset_dict["second_variation"]

    return dataset_dict


def generate_all_rates(
    dataset_dict,
    SN_type_dict,
    event_based,
    split_hydrogen_for_PPISNe_and_PISNe,
    DCO_type_filter,
    SN_type_filter,
    filter_fb,
    include_single,
    global_query,
    dco_query,
    sn_binary_query,
    sn_single_query,
    nrows=None,
):
    """
    Function to generate all the rates
    """

    # Loop over datasets
    for dataset_i, dataset in enumerate(dataset_dict.keys()):
        print("Calculating rates for {}".format(dataset))

        ################
        # Get DCO rates
        if "binary_dco_filename" in dataset_dict[dataset].keys():
            # Binary DCO
            binary_dco_rates = readout_binary_dco_rates(
                filename=dataset_dict[dataset]["binary_dco_filename"],
                nrows=nrows,
                # queries
                global_query=global_query,
                transient_type_query=dco_query,
                specific_query=dataset_dict[dataset].get("dco_query", None),
                dataset_query=None,  # TODO: fix based on dataset dict
            )

            dataset_dict[dataset]["dco_rates"] = binary_dco_rates

        ################
        # Get SN transient rates
        if ("binary_sn_filename" in dataset_dict[dataset].keys()) or (
            "single_sn_filename" in dataset_dict[dataset].keys()
        ):
            # Binary SN
            binary_sn_rates = readout_sn_rates(
                filename=dataset_dict[dataset]["binary_sn_filename"],
                SN_type_dict=SN_type_dict,
                nrows=nrows,
                split_hydrogen_for_PPISNe_and_PISNe=split_hydrogen_for_PPISNe_and_PISNe,
                # switches
                event_based=event_based,
                filter_fb=filter_fb,
                # queries
                global_query=global_query,
                transient_type_query=sn_binary_query,
                specific_query=dataset_dict[dataset].get("sn_binary_query", None),
                dataset_query=None,  # TODO: fix based on dataset dict
            )

            # Combine the SN rates
            sn_rates = binary_sn_rates

            # Single SN rates
            if include_single:
                if "single_sn_filename" in dataset_dict[dataset]:
                    single_sn_rates = readout_sn_rates(
                        filename=dataset_dict[dataset]["single_sn_filename"],
                        SN_type_dict=SN_type_dict,
                        nrows=nrows,
                        split_hydrogen_for_PPISNe_and_PISNe=split_hydrogen_for_PPISNe_and_PISNe,
                        # Switches
                        event_based=event_based,
                        filter_fb=filter_fb,
                        # queries
                        global_query=global_query,
                        transient_type_query=sn_single_query,
                        specific_query=dataset_dict[dataset].get(
                            "sn_single_query", None
                        ),
                        dataset_query=None,  # TODO: fix based on dataset dict
                    )

                    # combine the SN rates
                    sn_rates = merge_dicts(sn_rates, single_sn_rates)

            #
            dataset_dict[dataset]["sn_rates"] = sn_rates

            ############
            # replace string and fix order again
            sn_rates = update_dataset_string_and_fix_order(sn_rates=sn_rates)

    return dataset_dict


def readout_binary_dco_rates(
    filename,
    nrows=None,
    # queries
    global_query=None,
    transient_type_query=None,
    dataset_query=None,
    specific_query=None,
):
    """
    Function to read out the binary dco rates
    """

    #
    total_BHBH_rate_dict = {}
    total_BHNS_rate_dict = {}
    total_NSBH_rate_dict = {}
    total_NSNS_rate_dict = {}

    # Get dataframe
    dco_convolution_dataframe = pd.read_hdf(
        filename, "/data/combined_dataframes", nrows=nrows
    )

    ################
    # Combine queries
    combined_query = merge_query(
        pad_query(global_query),
        pad_query(dataset_query),
        pad_query(transient_type_query),
        pad_query(specific_query),
    )

    # Get BHBH DCO
    BHBH_indices = dco_convolution_dataframe.eval(
        merge_query(combined_query, "stellar_type_1 == 14 & stellar_type_2 == 14")
    ).to_numpy()
    BHNS_indices = dco_convolution_dataframe.eval(
        merge_query(combined_query, "stellar_type_1 == 14 & stellar_type_2 == 13")
    ).to_numpy()
    NSBH_indices = dco_convolution_dataframe.eval(
        merge_query(combined_query, "stellar_type_1 == 13 & stellar_type_2 == 14")
    ).to_numpy()
    NSNS_indices = dco_convolution_dataframe.eval(
        merge_query(combined_query, "stellar_type_1 == 13 & stellar_type_2 == 13")
    ).to_numpy()

    #
    dco_convolution_datafile = h5py.File(filename)
    redshifts = np.array(
        sorted(list(dco_convolution_datafile["data/merger_rate"].keys()), key=float)
    )

    for redshift_key in redshifts:
        redshift = float(redshift_key)

        # Readout rate data
        rate_data = dco_convolution_datafile[
            "data/merger_rate/{}".format(redshift_key)
        ][()]

        # get BHBH rate
        BHBH_rate = rate_data[BHBH_indices]
        total_BHBH_rate_dict[redshift] = np.sum(BHBH_rate)

        # get BHNS rate
        BHNS_rate = rate_data[BHNS_indices]
        total_BHNS_rate_dict[redshift] = np.sum(BHNS_rate)

        # get NSBH rate
        NSBH_rate = rate_data[NSBH_indices]
        total_NSBH_rate_dict[redshift] = np.sum(NSBH_rate)

        # get NSNS rate
        NSNS_rate = rate_data[NSNS_indices]
        total_NSNS_rate_dict[redshift] = np.sum(NSNS_rate)

        # TODO: fix this by binning and then multiplying by binsize?

    # Combine dicts:
    combined_total_BHNS_rate_dict = combine_dicts(
        total_BHNS_rate_dict, total_NSBH_rate_dict
    )

    # get BHBH rate data in correct shape
    (
        total_BHBH_rate_dict_redshift,
        total_BHBH_rate_dict_values,
    ) = get_redshift_and_values(total_BHBH_rate_dict)

    # get BHNS rate data in correct shape
    (
        combined_total_BHNS_rate_dict_redshift,
        combined_total_BHNS_rate_dict_values,
    ) = get_redshift_and_values(combined_total_BHNS_rate_dict)

    # get NSNS rate data in correct shape
    (
        total_NSNS_rate_dict_redshift,
        total_NSNS_rate_dict_values,
    ) = get_redshift_and_values(total_NSNS_rate_dict)

    #
    return_dict = {
        "bhbh": {
            "redshifts": total_BHBH_rate_dict_redshift,
            "rates": total_BHBH_rate_dict_values,
            "label": "BHBH merger",
        },
        "bhns": {
            "redshifts": combined_total_BHNS_rate_dict_redshift,
            "rates": combined_total_BHNS_rate_dict_values,
            "label": "BHNS merger",
        },
        "nsns": {
            "redshifts": total_NSNS_rate_dict_redshift,
            "rates": total_NSNS_rate_dict_values,
            "label": "NSNS merger",
        },
    }

    #
    return return_dict


def merge_query(*args):
    """
    Function to merge queries
    """

    query_string = " & ".join(
        ["({})".format(query_arg) for query_arg in args if not query_arg == ""]
    )

    return query_string


def pad_query(query):
    """
    Function to pad a query
    """

    return "" if query is None else " {} ".format(query)


def combine_dicts(dict_1, dict_2):
    """
    Function to add the values in two dictionaries
    """

    new_dict = {}

    # Add values from dict_1
    for key, value in dict_1.items():
        new_dict[key] = value

    # Add values from dict_2
    for key, value in dict_2.items():
        if key not in new_dict:
            new_dict[key] = 0
        new_dict[key] += value

    #
    return new_dict


def get_redshift_and_values(rate_dict):
    """
    Function to turn dict into redshift and values
    """

    rate_dict_keys = np.array(sorted(list(rate_dict.keys()), key=float))
    rate_dict_redshift = [float(el) for el in rate_dict_keys]
    rate_dict_values = [rate_dict[el] for el in rate_dict_keys]

    return np.array(rate_dict_redshift), np.array(rate_dict_values)


def readout_sn_rates(
    filename,
    SN_type_dict,
    nrows=None,
    # switches
    event_based=False,
    filter_fb=False,
    split_hydrogen_for_PPISNe_and_PISNe=True,
    # queries
    global_query=None,
    transient_type_query=None,
    dataset_query=None,
    specific_query=None,
):
    """
    Function to read out binary sn rates
    """

    total_IBC_rate_dict = {}
    total_II_rate_dict = {}
    total_PPISN_h_free_rate_dict = {}
    total_PPISN_h_rich_rate_dict = {}
    total_PISN_h_free_rate_dict = {}
    total_PISN_h_rich_rate_dict = {}

    #
    inverse_SN_type_dict = {value: key for key, value in SN_type_dict.items()}

    # Get dataframe
    sn_convolution_dataframe = pd.read_hdf(
        filename, "/data/combined_dataframes", nrows=nrows
    )

    if event_based:
        sn_type_column_string = "SN_type"
    else:
        sn_type_column_string = "post_SN_SN_type"

    if filter_fb:
        filter_fb_string = "fallback_fraction < 1"
    else:
        filter_fb_string = ""

    # construct h-free and h-rich query
    h_free_query = "pre_SN_st in [7,8,9]"
    h_rich_query = "pre_SN_st not in [7,8,9]"

    ################
    # Combine queries
    combined_query = merge_query(
        pad_query(global_query),
        pad_query(dataset_query),
        pad_query(transient_type_query),
        pad_query(specific_query),
    )

    #######################
    # Get indices of SNe

    # get IBC SN
    IBC_indices = sn_convolution_dataframe.eval(
        merge_query(
            combined_query,
            filter_fb_string,
            "{} == {}".format(sn_type_column_string, inverse_SN_type_dict["IBC"]),
        )
    ).to_numpy()

    # get II SN
    II_indices = sn_convolution_dataframe.eval(
        merge_query(
            combined_query,
            filter_fb_string,
            "{} == {}".format(sn_type_column_string, inverse_SN_type_dict["II"]),
        )
    ).to_numpy()

    # H-free PPISNe
    PPISN_h_free_indices = sn_convolution_dataframe.eval(
        merge_query(
            combined_query,
            h_free_query,
            "{} == {}".format(sn_type_column_string, inverse_SN_type_dict["PPISN"]),
        )
    ).to_numpy()

    # H-rich PPISne
    PPISN_h_rich_indices = sn_convolution_dataframe.eval(
        merge_query(
            combined_query,
            h_rich_query,
            "{} == {}".format(sn_type_column_string, inverse_SN_type_dict["PPISN"]),
        )
    ).to_numpy()

    # H-free PISNe
    PISN_h_free_indices = sn_convolution_dataframe.eval(
        merge_query(
            combined_query,
            h_free_query,
            "{} == {}".format(sn_type_column_string, inverse_SN_type_dict["PISN"]),
        )
    ).to_numpy()

    # H-free PISNe
    PISN_h_rich_indices = sn_convolution_dataframe.eval(
        merge_query(
            combined_query,
            h_rich_query,
            "{} == {}".format(sn_type_column_string, inverse_SN_type_dict["PISN"]),
        )
    ).to_numpy()

    ############################
    # Open HDF5 file again
    sn_convolution_datafile = h5py.File(filename)

    #
    redshifts = np.array(
        sorted(list(sn_convolution_datafile["data/formation_rate"].keys()), key=float)
    )

    # Loop over all the redshift
    for redshift_key in redshifts:
        redshift = float(redshift_key)

        # Readout rate data
        rate_data = sn_convolution_datafile[
            "data/formation_rate/{}".format(redshift_key)
        ][()]

        # get IBC rate
        IBC_rate = rate_data[IBC_indices]
        total_IBC_rate_dict[redshift] = np.sum(IBC_rate)

        # get II rate
        II_rate = rate_data[II_indices]
        total_II_rate_dict[redshift] = np.sum(II_rate)

        # get PPISN h-free rate
        PPISN_h_free_rate = rate_data[PPISN_h_free_indices]
        total_PPISN_h_free_rate_dict[redshift] = np.sum(PPISN_h_free_rate)

        # get PPISN h-rich rate
        PPISN_h_rich_rate = rate_data[PPISN_h_rich_indices]
        total_PPISN_h_rich_rate_dict[redshift] = np.sum(PPISN_h_rich_rate)

        # get PISN h-free rate
        PISN_h_free_rate = rate_data[PISN_h_free_indices]
        total_PISN_h_free_rate_dict[redshift] = np.sum(PISN_h_free_rate)

        # get PISN h-rich rate
        PISN_h_rich_rate = rate_data[PISN_h_rich_indices]
        total_PISN_h_rich_rate_dict[redshift] = np.sum(PISN_h_rich_rate)

    #############
    # Combine and structure into dicts etc

    # Combine CC rates
    combined_total_CC_rate_dict = combine_dicts(total_IBC_rate_dict, total_II_rate_dict)

    # get CC rate data in correct shape
    (
        combined_total_CC_rate_dict_redshift,
        combined_total_CC_rate_dict_values,
    ) = get_redshift_and_values(combined_total_CC_rate_dict)

    #
    return_dict = {
        "CC": {
            "redshifts": combined_total_CC_rate_dict_redshift,
            "rates": combined_total_CC_rate_dict_values,
            "label": "CCSN",
        },
    }

    # Handle PPISNe
    if split_hydrogen_for_PPISNe_and_PISNe:
        # get PPISN H-free rate data in correct shape
        (
            total_PPISN_h_free_rate_dict_redshift,
            total_PPISN_h_free_rate_dict_values,
        ) = get_redshift_and_values(total_PPISN_h_free_rate_dict)

        #
        return_dict["PPISN_h_free"] = {
            "redshifts": total_PPISN_h_free_rate_dict_redshift,
            "rates": total_PPISN_h_free_rate_dict_values,
            "label": "PPISN\n(H-free)",
        }

        # get PPISN H-rich rate data in correct shape
        (
            total_PPISN_h_rich_rate_dict_redshift,
            total_PPISN_h_rich_rate_dict_values,
        ) = get_redshift_and_values(total_PPISN_h_rich_rate_dict)

        #
        return_dict["PPISN_h_rich"] = {
            "redshifts": total_PPISN_h_rich_rate_dict_redshift,
            "rates": total_PPISN_h_rich_rate_dict_values,
            "label": "PPISN\n(H-rich)",
        }

        # get PISN H-free rate data in correct shape
        (
            total_PISN_h_free_rate_dict_redshift,
            total_PISN_h_free_rate_dict_values,
        ) = get_redshift_and_values(total_PISN_h_free_rate_dict)

        #
        return_dict["PISN_h_free"] = {
            "redshifts": total_PISN_h_free_rate_dict_redshift,
            "rates": total_PISN_h_free_rate_dict_values,
            "label": "PISN\n(H-free)",
        }

        # get PISN H-rich rate data in correct shape
        (
            total_PISN_h_rich_rate_dict_redshift,
            total_PISN_h_rich_rate_dict_values,
        ) = get_redshift_and_values(total_PISN_h_rich_rate_dict)

        #
        return_dict["PISN_h_rich"] = {
            "redshifts": total_PISN_h_rich_rate_dict_redshift,
            "rates": total_PISN_h_rich_rate_dict_values,
            "label": "PISN\n(H-rich)",
        }
    else:
        # Combine PPISN rates
        total_PPISN_rate_dict = combine_dicts(
            total_PPISN_h_free_rate_dict, total_PPISN_h_rich_rate_dict
        )

        # get PPISN rate data in correct shape
        (
            total_PPISN_rate_dict_redshift,
            total_PPISN_rate_dict_values,
        ) = get_redshift_and_values(total_PPISN_rate_dict)

        #
        return_dict["PPISN"] = {
            "redshifts": total_PPISN_rate_dict_redshift,
            "rates": total_PPISN_rate_dict_values,
            "label": "PPISN",
        }

        # Combine PISN rates
        total_PISN_rate_dict = combine_dicts(
            total_PISN_h_free_rate_dict, total_PISN_h_rich_rate_dict
        )

        # get PISN rate data in correct shape
        (
            total_PISN_rate_dict_redshift,
            total_PISN_rate_dict_values,
        ) = get_redshift_and_values(total_PISN_rate_dict)

        #
        return_dict["PISN"] = {
            "redshifts": total_PISN_rate_dict_redshift,
            "rates": total_PISN_rate_dict_values,
            "label": "PISN",
        }

    #
    return return_dict


def update_dataset_string_and_fix_order(sn_rates):
    """
    Function to update the dataset string and fix the order
    """

    oldname = "CC"
    newname = "CCSN"

    # store old order:
    old_order = []
    for sn_type in sn_rates.keys():
        old_order.append(sn_type)

    # replace
    new_order = copy.copy(old_order)
    new_order[new_order.index(oldname)] = newname

    # Loop over datasets and fix the name
    sn_rates[newname] = sn_rates[oldname]
    del sn_rates[oldname]

    # create temp with correct order
    new_sn_rate_dict = {}
    for sn_type in new_order:
        new_sn_rate_dict[sn_type] = sn_rates[sn_type]

    return new_sn_rate_dict


def add_fill_between_PPISNe_and_PISNe_function(
    fig, ax, dataset_dict, split_hydrogen_for_PPISNe_and_PISNe, sn_rate_color_dict
):
    """
    Function to handle the fill between
    """

    hatch_alpha = 0.1

    PPISN_STRING = (
        "PPISN" if not split_hydrogen_for_PPISNe_and_PISNe else "PPISN_h_free"
    )
    PISN_STRING = "PISN" if not split_hydrogen_for_PPISNe_and_PISNe else "PISN_h_free"

    #
    included_ppisn_rate_arrays = []
    included_pisn_rate_arrays = []

    #
    for dataset_i, dataset_name in enumerate(dataset_dict.keys()):
        dataset = dataset_dict[dataset_name]

        if "sn_rates" in dataset.keys():
            included_ppisn_rate_arrays.append(
                dataset["sn_rates"][PPISN_STRING]["rates"]
            )
            included_pisn_rate_arrays.append(dataset["sn_rates"][PISN_STRING]["rates"])

    # Construct the numpy arrays
    ppisn_rate_values_array = np.array(included_ppisn_rate_arrays)
    pisn_rate_values_array = np.array(included_pisn_rate_arrays)

    # Get the min values
    min_ppisn_rate_values = np.amin(ppisn_rate_values_array, axis=0)
    min_pisn_rate_values = np.amin(pisn_rate_values_array, axis=0)

    # Get the max values
    max_ppisn_rate_values = np.amax(ppisn_rate_values_array, axis=0)
    max_pisn_rate_values = np.amax(pisn_rate_values_array, axis=0)

    #########
    # Fill between the PPISNe
    ax.fill_between(
        dataset_dict["fiducial"]["sn_rates"][PPISN_STRING]["redshifts"],
        min_ppisn_rate_values,
        max_ppisn_rate_values,
        # fc=sn_rate_color_dict[PPISN_STRING],
        color=sn_rate_color_dict[PPISN_STRING],
        hatch="//",
        alpha=hatch_alpha,
    )

    ########
    # Fill between the PISNe
    ax.fill_between(
        dataset_dict["fiducial"]["sn_rates"][PISN_STRING]["redshifts"],
        min_pisn_rate_values,
        max_pisn_rate_values,
        # fc=sn_rate_color_dict[PISN_STRING],
        color=sn_rate_color_dict[PISN_STRING],
        hatch="|",
        alpha=hatch_alpha,
    )

    return fig, ax


def add_rates_function(
    fig,
    ax,
    dataset_dict,
    add_DCO_merger_rates,
    add_SN_transient_rates,
    add_transient_observational_rates,
    add_gw_observational_rates,
    add_other_observational_rates,
    DCO_type_filter,
    SN_type_filter,
    sn_rate_color_dict,
    # add extra SN markers
    add_additional_SN_markers,
    SN_type_additional_marker_dict,
):
    """
    Function to add the rates to the plot
    """

    ##########################
    # Plot DCO merger rates
    if add_DCO_merger_rates:
        fig, ax, _, DCO_scatter_sets = add_DCO_merger_rates_function(
            fig=fig, ax=ax, dataset_dict=dataset_dict, DCO_type_filter=DCO_type_filter
        )

    #########################
    # Plot SN transient rates
    if add_SN_transient_rates:
        fig, ax, sn_rate_objs = add_SN_transient_rates_function(
            fig=fig,
            ax=ax,
            dataset_dict=dataset_dict,
            sn_rate_color_dict=sn_rate_color_dict,
            SN_type_filter=SN_type_filter,
            add_additional_SN_markers=add_additional_SN_markers,
            SN_type_additional_marker_dict=SN_type_additional_marker_dict,
        )

    ############
    # Add observational rates to the plot
    if (
        add_transient_observational_rates
        or add_gw_observational_rates
        or add_other_observational_rates
    ):
        fig, ax, obs_objects = add_observational_rates_function(
            fig=fig,
            ax=ax,
            add_transient_observational_rates=add_transient_observational_rates,
            add_gw_observational_rates=add_gw_observational_rates,
            add_other_observational_rates=add_other_observational_rates,
        )

    return fig, ax, DCO_scatter_sets, sn_rate_objs, obs_objects


def add_DCO_merger_rates_function(
    fig,
    ax,
    dataset_dict,
    DCO_type_filter,
):
    """
    Function to add the DCO merger rates
    """

    #
    dco_step = 20
    dco_markersize = 150
    dco_markers = ["o", "X", "*"]
    dco_marker_alpha = 1
    color_fiducial_dco = "k"
    # color_fiducial_dco = "#5a7d9a"
    # color_fiducial_dco = "#cc33ff"

    alpha_fiducial_merger = 0.25

    dco_line_sets = []
    dco_scatter_sets = []

    #
    dco_type_order = ["bhbh", "bhns", "nsns"]

    ##########
    # Loop over the dataset
    for dataset_i, dataset_name in enumerate(dataset_dict.keys()):
        dataset = dataset_dict[dataset_name]

        #
        if "dco_rates" not in dataset.keys():
            continue

        ########
        # loop over DCO types
        for dco_type_i, dco_type in enumerate(dco_type_order):
            # Filter specific DCO type out of here
            if not DCO_type_filter[dco_type]:
                continue

            ########
            # Line plot
            dco_plot_obj = ax.plot(
                dataset["dco_rates"][dco_type]["redshifts"],
                dataset["dco_rates"][dco_type]["rates"],
                linestyle=dataset["linestyle"],
                color=color_fiducial_dco,
                alpha=alpha_fiducial_merger,
            )
            dco_line_sets.append(dco_plot_obj)

            ########
            # Scatter plot
            if dataset["add_DCO_to_legend"]:
                suffix = dataset.get("DCO_legend_suffix", "")
                if suffix:
                    suffix = " " + suffix

                #
                scatter_obj = ax.scatter(
                    dataset["dco_rates"][dco_type]["redshifts"][::dco_step],
                    dataset["dco_rates"][dco_type]["rates"][::dco_step],
                    label=dataset["dco_rates"][dco_type]["label"] + suffix,
                    s=dco_markersize,
                    c=color_fiducial_dco,
                    alpha=dco_marker_alpha,
                    marker=dataset.get("DCO_marker", dco_markers[dco_type_i]),
                    edgecolor=dataset.get("DCO_marker_edgecolor", "face"),
                    linewidths=dataset.get("DCO_marker_linewidth", 1.5),
                )
                dco_scatter_sets.append(scatter_obj)

    return fig, ax, dco_line_sets, dco_scatter_sets


def add_SN_transient_rates_function(
    fig,
    ax,
    dataset_dict,
    sn_rate_color_dict,
    SN_type_filter,
    add_additional_SN_markers,
    SN_type_additional_marker_dict,
):
    """
    Function to add the SN transient rates
    """

    #
    sn_step = 20
    sn_offset = 10
    sn_markersize = 150
    sn_marker_alpha = 1
    # color_fiducial_dco = "k"
    # alpha_fiducial_merger = 0.25

    sn_rate_objs = []

    ####
    # Do a check to see if anything should be added to the legend
    add_to_legend = {}

    ##########
    # Loop over the dataset
    for dataset_i, dataset_name in enumerate(dataset_dict.keys()):
        dataset = dataset_dict[dataset_name]

        #
        if "sn_rates" not in dataset.keys():
            continue

        ########
        # Loop over SN types
        for SN_type_i, SN_type in enumerate(dataset["sn_rates"].keys()):
            # Filter specific SN type out of here
            if not SN_type_filter[SN_type]:
                continue

            #
            if SN_type not in add_to_legend.keys():
                add_to_legend[SN_type] = False

            #
            sum_rates = np.sum(dataset["sn_rates"][SN_type]["rates"])

            # If there is any rate at all, then add
            if sum_rates > 0:
                add_to_legend[SN_type] = True

    #
    alpha_fiducial_transient = 1

    ##########
    # Loop over the dataset
    for dataset_i, dataset_name in enumerate(dataset_dict.keys()):
        dataset = dataset_dict[dataset_name]

        #
        if "sn_rates" not in dataset.keys():
            continue

        ########
        # Loop over SN types
        for SN_type_i, SN_type in enumerate(dataset["sn_rates"].keys()):
            # Filter specific SN type out of here
            if not SN_type_filter[SN_type]:
                continue

            # Plot
            sn_rate_obj = ax.plot(
                dataset["sn_rates"][SN_type]["redshifts"],
                dataset["sn_rates"][SN_type]["rates"],
                label="" + dataset["sn_rates"][SN_type]["label"],
                alpha=alpha_fiducial_transient,
                color=sn_rate_color_dict[SN_type],
                linestyle=dataset["linestyle"],
            )

            # Add to legend
            if dataset["add_SN_to_legend"]:
                if add_to_legend[SN_type]:
                    sn_rate_objs.append(sn_rate_obj)

            # Add additional marker to plot
            if add_additional_SN_markers:
                if SN_type in SN_type_additional_marker_dict:
                    #
                    _ = ax.scatter(
                        dataset["sn_rates"][SN_type]["redshifts"][sn_offset::sn_step],
                        dataset["sn_rates"][SN_type]["rates"][sn_offset::sn_step],
                        # label=dataset["sn_rates"][SN_type]["label"] + suffix,
                        s=sn_markersize,
                        c=sn_rate_color_dict[SN_type],
                        alpha=sn_marker_alpha,
                        marker=SN_type_additional_marker_dict[SN_type],
                        edgecolor="face",
                        linewidths=1.5,
                    )
                    # dco_scatter_sets.append(scatter_obj)

    return fig, ax, sn_rate_objs


def add_observational_rates_function(
    fig,
    ax,
    add_transient_observational_rates,
    add_gw_observational_rates,
    add_other_observational_rates,
):
    """
    Function to add the observational rates
    """

    # fix names
    CCSN_STRING = "CCSN"

    # observation_markersize = 100
    observation_alpha = 1
    obs_objects = []

    ############
    # Add transient observation set
    if add_transient_observational_rates:
        transient_observation_set = observations_sets["Frohmaier_2021"]

        # Plot CC
        obs_object = ax.errorbar(
            transient_observation_set["redshift"],
            transient_observation_set["SN_types"][CCSN_STRING]["rate"]
            .to(used_rate_unit)
            .value,
            yerr=[
                [
                    transient_observation_set["SN_types"][CCSN_STRING]["lower_error"]
                    .to(used_rate_unit)
                    .value
                ],
                [
                    transient_observation_set["SN_types"][CCSN_STRING]["upper_error"]
                    .to(used_rate_unit)
                    .value
                ],
            ],
            alpha=observation_alpha,
            color="#ff0000",
            linestyle="none",
            # fmt="-o",
            capsize=8,
            label="CCSNe " + transient_observation_set["name"],
        )
        obs_objects.append(obs_object)

        # Plot SLSNe-I
        obs_object = ax.errorbar(
            transient_observation_set["redshift"],
            transient_observation_set["SN_types"]["SLSNe-I"]["rate"]
            .to(used_rate_unit)
            .value,
            yerr=[
                [
                    transient_observation_set["SN_types"]["SLSNe-I"]["lower_error"]
                    .to(used_rate_unit)
                    .value
                ],
                [
                    transient_observation_set["SN_types"]["SLSNe-I"]["upper_error"]
                    .to(used_rate_unit)
                    .value
                ],
            ],
            # color="#cc6699",
            color="#990000",
            alpha=observation_alpha,
            linestyle="none",
            # fmt="-",
            capsize=8,
            label="SLSNe-I " + transient_observation_set["name"],
        )
        obs_objects.append(obs_object)

    ############
    # Add GW observation set
    if add_gw_observational_rates:
        gw_observation_set = observations_sets["LVK_2021"]

        # # Plot BNS
        # obs_object = ax.errorbar(
        #     gw_observation_set["redshift"],
        #     gw_observation_set["GW_types"]["BNS"]["rate"].to(used_rate_unit).value,
        #     yerr=[
        #         [
        #             gw_observation_set["GW_types"]["BNS"]["lower_error"]
        #             .to(used_rate_unit)
        #             .value
        #         ],
        #         [
        #             gw_observation_set["GW_types"]["BNS"]["upper_error"]
        #             .to(used_rate_unit)
        #             .value
        #         ],
        #     ],
        #     alpha=observation_alpha,
        #     color="#99994d",
        #     fmt="none",
        #     label=gw_observation_set["name"] + " NSNS",
        #     dash_capstyle="round",
        # )
        # obs_objects.append(obs_object)

        # # Plot BHNS
        # obs_object = ax.errorbar(
        #     gw_observation_set["redshift"] + 0.05,
        #     gw_observation_set["GW_types"]["BHNS"]["rate"].to(used_rate_unit).value,
        #     yerr=[
        #         [
        #             gw_observation_set["GW_types"]["BHNS"]["lower_error"]
        #             .to(used_rate_unit)
        #             .value
        #         ],
        #         [
        #             gw_observation_set["GW_types"]["BHNS"]["upper_error"]
        #             .to(used_rate_unit)
        #             .value
        #         ],
        #     ],
        #     color="#77773c",
        #     alpha=observation_alpha,
        #     fmt="none",
        #     label=gw_observation_set["name"] + " BHNS",
        #     dash_capstyle="round",
        # )
        # obs_objects.append(obs_object)

        # Plot BBH
        obs_object = ax.errorbar(
            gw_observation_set["redshift"],
            gw_observation_set["GW_types"]["BBH"]["rate"].to(used_rate_unit).value,
            yerr=[
                [
                    gw_observation_set["GW_types"]["BBH"]["lower_error"]
                    .to(used_rate_unit)
                    .value
                ],
                [
                    gw_observation_set["GW_types"]["BBH"]["upper_error"]
                    .to(used_rate_unit)
                    .value
                ],
            ],
            # color="#48D1CC", # aquamarine blue
            # color="#cc33ff",
            color="#5a7d9a",
            alpha=observation_alpha,
            fmt="none",
            label="BHBH " + gw_observation_set["name"],
            dash_capstyle="round",
            markeredgecolor="k",
            markeredgewidth=5,
            linewidth=8,
        )
        obs_objects.append(obs_object)

    ############
    # Add other observation set (briel)
    if add_other_observational_rates:
        err_alpha = 0.25
        # err_color = "gray"
        err_color = "#cc33ff"

        #########
        # Add the extra CCSNe rates
        for obs_i, obs in enumerate(other_observation_sets["CCSNe"]):
            # Get y_err
            y_err = None
            if obs.get("lower_error_y", None) is not None:
                y_err = np.array(
                    [
                        [
                            obs["lower_error_y"].to(used_rate_unit).value,
                            obs["upper_error_y"].to(used_rate_unit).value,
                        ]
                    ]
                ).T

            # get x_err
            x_err = None
            if obs.get("lower_error_x", None) is not None:
                x_err = np.array([[obs["lower_error_x"], obs["upper_error_x"]]]).T

            # Plot error
            CCSNe_extra = ax.errorbar(
                obs["redshift"],
                obs["rate"].to(used_rate_unit).value,
                yerr=y_err,
                xerr=x_err,
                # color="red",
                color=err_color,
                label="CCSNe other sources\n(tabulated in Briel et al. 2022)"
                if obs_i == 0
                else None,
                alpha=err_alpha,
                zorder=-1000,
            )

            if obs_i == 0:
                obs_objects.append(CCSNe_extra)

        #########
        # Add the extra CCSNe rates
        for obs_i, obs in enumerate(other_observation_sets["SLSNeI"]):
            # Get y_error
            y_err = None
            if obs.get("lower_error_y", None) is not None:
                y_err = np.array(
                    [
                        [
                            obs["lower_error_y"].to(used_rate_unit).value,
                            obs["upper_error_y"].to(used_rate_unit).value,
                        ]
                    ]
                ).T

            # Get x_error
            x_err = None
            if obs.get("lower_error_x", None) is not None:
                x_err = np.array([[obs["lower_error_x"], obs["upper_error_x"]]]).T

            # Plot errorbar
            SLSNE_extra_err = ax.errorbar(
                obs["redshift"],
                obs["rate"].to(used_rate_unit).value,
                yerr=y_err,
                xerr=x_err,
                # color="blue",
                color=err_color,
                label="SLSNe-I other sources\n(tabulated in Briel et al. 2022)"
                if obs_i == 0
                else None,
                alpha=err_alpha,
                fmt="-d",
                capsize=8,
                linewidth=2,
                # marker="d",
                elinewidth=2,
                zorder=-1000,
            )

            SLSNE_extra_err[0].set_fillstyle("none")
            # # change linestyle
            # for col in SLSNE_extra_err[-1]:
            #     col.set_linestyle(":")
            # SLSNE_extra_err[0].set_linestyle(":")

            if obs_i == 0:
                obs_objects.append(SLSNE_extra_err)

    return fig, ax, obs_objects


def print_SN_rates_function(
    dataset_dict, print_SN_rates, SN_type_filter, sn_rate_redshift=0.028
):
    """
    Function to print the SN rates
    """

    if print_SN_rates:
        ##########
        # Loop over the dataset
        for dataset_i, dataset_name in enumerate(dataset_dict.keys()):
            dataset = dataset_dict[dataset_name]
            dataset_label = dataset["name"]

            #
            if "sn_rates" not in dataset.keys():
                continue

            ########
            # Loop over SN types
            for SN_type_i, SN_type in enumerate(dataset["sn_rates"].keys()):
                # Filter specific SN type out of here
                if not SN_type_filter[SN_type]:
                    continue

                # Get the rates and the redshifts
                sn_redshifts = dataset["sn_rates"][SN_type]["redshifts"]
                sn_rates = dataset["sn_rates"][SN_type]["rates"]

                # set up interpolation and calculate rate at redshift
                rate_interpolator = interpolate.interp1d(
                    sn_redshifts, sn_rates, bounds_error=True
                )
                rate_at_redshift = rate_interpolator(sn_rate_redshift)

                # print info
                print(
                    "{}: {}: {:.2E}".format(
                        dataset_label, SN_type, rate_at_redshift * used_rate_unit
                    )
                )

        # print some observation stuff
        print(
            "Observation set: CCSN: mean {:.2E}".format(
                observations_sets["Frohmaier_2021"]["SN_types"]["CCSN"]["rate"]
            )
        )
        print(
            "Observation set: CCSN: upper {:.2E}".format(
                observations_sets["Frohmaier_2021"]["SN_types"]["CCSN"]["rate"]
                + observations_sets["Frohmaier_2021"]["SN_types"]["CCSN"]["upper_error"]
            )
        )
        print(
            "Observation set: CCSN: lower {:.2E}".format(
                observations_sets["Frohmaier_2021"]["SN_types"]["CCSN"]["rate"]
                - observations_sets["Frohmaier_2021"]["SN_types"]["CCSN"]["lower_error"]
            )
        )

        print(
            "Observation set: SLSNe-I: mean {:.2E}".format(
                observations_sets["Frohmaier_2021"]["SN_types"]["SLSNe-I"]["rate"]
            )
        )
        print(
            "Observation set: SLSNe-I: upper {:.2E}".format(
                observations_sets["Frohmaier_2021"]["SN_types"]["SLSNe-I"]["rate"]
                + observations_sets["Frohmaier_2021"]["SN_types"]["SLSNe-I"][
                    "upper_error"
                ]
            )
        )
        print(
            "Observation set: SLSNe-I: lower {:.2E}".format(
                observations_sets["Frohmaier_2021"]["SN_types"]["SLSNe-I"]["rate"]
                - observations_sets["Frohmaier_2021"]["SN_types"]["SLSNe-I"][
                    "lower_error"
                ]
            )
        )


def handle_legends_functions(
    fig,
    ax,
    dataset_dict,
    # Observations
    add_observation_to_legend,
    observation_legend_config,
    obs_objects,
    # DCO
    add_DCO_to_legend,
    DCO_legend_config,
    DCO_scatter_sets,
    #
    add_SN_to_legend,
    SN_legend_config,
    sn_rate_objs,
    # variation
    add_variation_to_legend,
    variation_legend_config,
    #
    add_additional_SN_markers,
    SN_type_additional_marker_dict,
    sn_rate_color_dict,
):
    """
    Function to handle all the legends
    """

    ##########
    # Add legend for the observations
    if add_observation_to_legend:
        fig, ax = add_observation_legend_function(
            fig=fig,
            ax=ax,
            obs_objects=obs_objects,
            legend_config=observation_legend_config,
        )

    ##########
    # Add legend for DCO rates
    if add_DCO_to_legend:
        fig, ax = add_DCO_rates_legend_function(
            fig=fig,
            ax=ax,
            DCO_scatter_sets=DCO_scatter_sets,
            legend_config=DCO_legend_config,
        )

    #########
    # Add legend for variations
    if add_variation_to_legend:
        fig, ax = add_variation_legend_function(
            fig=fig,
            ax=ax,
            dataset_dict=dataset_dict,
            legend_config=variation_legend_config,
        )
    #########
    # Add legend for SN transient
    if add_SN_to_legend:
        fig, ax = add_SN_rates_legend_function(
            fig=fig,
            ax=ax,
            sn_rate_objs=sn_rate_objs,
            legend_config=SN_legend_config,
            add_additional_SN_markers=add_additional_SN_markers,
            SN_type_additional_marker_dict=SN_type_additional_marker_dict,
            dataset_dict=dataset_dict,
            sn_rate_color_dict=sn_rate_color_dict,
        )

    return fig, ax


def add_observation_legend_function(fig, ax, obs_objects, legend_config=None):
    """
    Function to add the observation legend
    """

    #
    legend_config = fix_empty_default_dict(legend_config)

    #
    observation_draw_objs = []
    observation_labels = []

    # add fiducial rates
    for obs in obs_objects:
        observation_draw_objs.append(obs)
        label = obs.get_label()

        # replace BHBH with
        label = label.replace("BHBH", "BBH")

        #
        observation_labels.append(label)

    #
    legend = ax.legend(
        observation_draw_objs,
        observation_labels,
        **legend_config,
    )
    ax.add_artist(legend)

    return fig, ax


def add_DCO_rates_legend_function(fig, ax, DCO_scatter_sets, legend_config):
    """
    Function to add the DCO rates legend
    """

    #
    legend_config = fix_empty_default_dict(legend_config)

    #
    rates_draw_objs = []
    rates_labels = []

    # add DCO type scatter to legend
    for scatter in DCO_scatter_sets:
        rates_draw_objs.append(scatter)
        label = scatter.get_label()

        # replace BHBH with
        label = label.replace("BHBH", "BBH")

        #
        rates_labels.append(label)

    # # add dummy one to fill the data
    # dummy_line = Line2D([], [])
    # dummy_line.set_linestyle("solid")
    # dummy_line.set_alpha(0.0)
    # dummy_line.set_label("")
    # dummy_line.set_color("k")

    # #
    # rates_draw_objs.append(dummy_line)
    # rates_labels.append(dummy_line.get_label())

    # if ncol is None:
    #     ncol = (
    #         add_DCO_merger_rates
    #         + add_fiducial_transient_rates
    #         + (add_fiducial_transient_rates and add_variation_transient_rates)
    #     )

    # Construct legend object and add to artist
    legend1 = ax.legend(rates_draw_objs, rates_labels, **legend_config)
    ax.add_artist(legend1)

    return fig, ax


def add_variation_legend_function(fig, ax, dataset_dict, legend_config):
    """
    Function to add the legend for the variations

    loop over the dataset dict. extract the name and the linestyle
    """

    #
    legend_config = fix_empty_default_dict(legend_config)

    #
    draw_objs = []
    labels = []
    variation_legend_color = "k"

    #
    for dataset_name in dataset_dict.keys():
        dataset = dataset_dict[dataset_name]

        if dataset["add_variation_to_legend"]:
            #
            line_obj = Line2D([], [])
            line_obj.set_linestyle(dataset["linestyle"])
            line_obj.set_label(dataset["name"])
            line_obj.set_color(variation_legend_color)

            # add to set
            draw_objs.append(line_obj)
            labels.append(line_obj.get_label())

    # Construct legend object and add to artist
    legend = ax.legend(draw_objs, labels, **legend_config)
    ax.add_artist(legend)

    return fig, ax


def add_SN_rates_legend_function(
    fig,
    ax,
    sn_rate_objs,
    legend_config,
    add_additional_SN_markers,
    SN_type_additional_marker_dict,
    dataset_dict,
    sn_rate_color_dict,
):
    """
    Function to add the SN rates to the legend
    """

    #
    legend_config = fix_empty_default_dict(legend_config)

    #
    sn_markersize = 150
    sn_marker_alpha = 1

    #
    rates_draw_objs = []
    rates_labels = []

    #
    manual_order = ["CCSN", "PPISN_h_free", "PISN_h_free"]

    # Add additional marker to plot
    if add_additional_SN_markers:
        dataset = dataset_dict[list(dataset_dict.keys())[0]]
        for SN_type_i, SN_type in enumerate(manual_order):
            if SN_type in SN_type_additional_marker_dict:
                #
                scatter_obj = ax.scatter(
                    [0],
                    [0],
                    s=sn_markersize,
                    c=sn_rate_color_dict[SN_type],
                    alpha=sn_marker_alpha,
                    marker=SN_type_additional_marker_dict[SN_type],
                    # edgecolor="face",
                    linewidths=1.5,
                )

                #
                rates_draw_objs.append(scatter_obj)
                rates_labels.append(
                    dataset["sn_rates"][SN_type]["label"].replace("\n", " ")
                )
    else:
        #
        for sn_rate in sn_rate_objs:
            rates_draw_objs.append(sn_rate[0])
            rates_labels.append(sn_rate[0].get_label())

    # Construct legend object and add to artist
    legend = ax.legend(rates_draw_objs, rates_labels, **legend_config)
    ax.add_artist(legend)

    return fig, ax


def add_SN_type_text_function(
    fig, ax, dataset_dict, sn_rate_color_dict, SN_type_filter, plot_settings
):
    """
    Function to add the SN type text and add it next to the line
    """

    fiducial_SN_rates = dataset_dict["fiducial"]["sn_rates"]

    #
    for sn_type_i, sn_type in enumerate(fiducial_SN_rates.keys()):
        #
        # Filter specific SN type out of here
        if not SN_type_filter[sn_type]:
            continue

        #

        sn_type_rates = fiducial_SN_rates[sn_type]["rates"]
        sn_type_redshifts = fiducial_SN_rates[sn_type]["redshifts"]
        sn_type_label = fiducial_SN_rates[sn_type]["label"]
        actually_plot_text = False

        ################
        # Perform checks to determine the position and angle

        # leftmost point falls within the frame
        if inrange(sn_type_redshifts[0], ax.get_xlim()) and inrange(
            sn_type_rates[0], ax.get_ylim()
        ):
            actually_plot_text = True
            x_loc = -0.5
            y_loc = sn_type_rates[0]
            rotation = "vertical"

        # check if any of the rates are visible in the view
        else:
            within_frame = np.logical_and(
                sn_type_rates > ax.get_ylim()[0], sn_type_rates < ax.get_ylim()[-1]
            )
            if np.any(within_frame):
                actually_plot_text = True
                # print("{} only partially visible in window.".format(sn_type))

                #
                first_index = np.nonzero(within_frame)[0][0]
                rate_first_index = sn_type_rates[first_index]
                redshift_first_index = sn_type_redshifts[first_index]

                #########
                # determine the position
                x_loc = redshift_first_index
                y_loc = rate_first_index

                # add shift
                y_loc = y_loc * 5

                #########
                # Determine the angle
                dy = sn_type_rates[first_index + 1] - sn_type_rates[first_index - 1]
                dx = (
                    sn_type_redshifts[first_index + 1]
                    - sn_type_redshifts[first_index - 1]
                )

                rad = np.arctan(dy / dx)
                deg = np.rad2deg(rad)

                rotation = deg
            else:
                # print("{} Nothing is within the frame. Not plotting shit")
                continue

        # Actually plot the text
        if actually_plot_text:
            ax.text(
                x_loc,
                y_loc,
                sn_type_label,
                color=sn_rate_color_dict[sn_type],
                rotation=rotation,
                verticalalignment="center",
                fontsize=plot_settings["SN_type_text_fontsize"],
            )

    return fig, ax


def inrange(val, endpoints):
    """
    Function to check if its in range
    """

    return endpoints[0] < val <= endpoints[-1]


def delete_out_of_frame_text(fig, ax):
    """
    Function that deletes text objects that fall out of frame
    """

    # find text out of bounds
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()

    for text in ax.texts:
        text_pos = text.get_position()
        if (not xlims[0] <= text_pos[0] <= xlims[1]) or (
            not ylims[0] <= text_pos[1] <= ylims[1]
        ):
            text.remove()

    return fig, ax
