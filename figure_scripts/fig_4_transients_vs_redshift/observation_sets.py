"""
Script containing the methods to return the observations
"""

import astropy.units as u


def combine_rates(rate_dict_1, rate_dict_2):
    """
    Function to combine the rates

    TODO: fix the error summing properly
    """

    new_dict = {}

    new_dict["rate"] = rate_dict_1["rate"] + rate_dict_2["rate"]
    new_dict["lower_error"] = max(
        [rate_dict_1["lower_error"], rate_dict_2["lower_error"]]
    )
    new_dict["upper_error"] = max(
        [rate_dict_1["upper_error"], rate_dict_2["upper_error"]]
    )

    return new_dict


h70 = 1

observations_sets = {}


################
# Add transients
observations_sets["Frohmaier_2021"] = {
    "SN_types": {
        "CC(Hrich)": {
            "rate": 9.10 * 1e-5 * (h70**3 / u.yr / u.Mpc**3),
            "lower_error": 1.27 * 1e-5 * (h70**3 / u.yr / u.Mpc**3),
            "upper_error": 1.56 * 1e-5 * (h70**3 / u.yr / u.Mpc**3),
        },
        "SESNe": {
            "rate": 2.41 * 1e-5 * (h70**3 / u.yr / u.Mpc**3),
            "lower_error": 0.64 * 1e-5 * (h70**3 / u.yr / u.Mpc**3),
            "upper_error": 0.81 * 1e-5 * (h70**3 / u.yr / u.Mpc**3),
        },
        "SLSNe-I": {
            "rate": 35 * (h70**3 / u.yr / u.Gpc**3),
            "lower_error": 13 * (h70**3 / u.yr / u.Gpc**3),
            "upper_error": 25 * (h70**3 / u.yr / u.Gpc**3),
        },
    },
    "redshift": 0.028,
    "url": "https://ui.adsabs.harvard.edu/abs/2021MNRAS.500.5142F/abstract",
    "name": "Frohmaier et al. (2021)",
}

#
observations_sets["Frohmaier_2021"]["SN_types"]["CCSN"] = combine_rates(
    observations_sets["Frohmaier_2021"]["SN_types"]["CC(Hrich)"],
    observations_sets["Frohmaier_2021"]["SN_types"]["SESNe"],
)

################
# Add GW observations
observations_sets["LVK_2021"] = {
    "GW_types": {
        "BNS": {
            "upper_rate": 1700 * (1 / u.yr / u.Gpc**3),
            "lower_rate": 10 * (1 / u.yr / u.Gpc**3),
        },
        "BHNS": {
            "upper_rate": 140 * (1 / u.yr / u.Gpc**3),
            "lower_rate": 7.8 * (1 / u.yr / u.Gpc**3),
        },
        "BBH": {
            "upper_rate": 61 * (1 / u.yr / u.Gpc**3),
            "lower_rate": 16 * (1 / u.yr / u.Gpc**3),
        },
    },
    "redshift": 0.2,
    # "url": "https://ui.adsabs.harvard.edu/abs/2021arXiv211103634T",
    "url": "https://ui.adsabs.harvard.edu/abs/2023PhRvX..13a1048A/abstract",
    # "name": "Abbott et al. (2021)",
    "name": "Abbott et al. (2023)",
}

for GW_type in observations_sets["LVK_2021"]["GW_types"]:
    GW_type_dict = observations_sets["LVK_2021"]["GW_types"][GW_type]
    # print(GW_type_dict)

    center_rate = GW_type_dict["upper_rate"] - GW_type_dict["lower_rate"]
    lower_error = center_rate - GW_type_dict["lower_rate"]
    upper_error = GW_type_dict["upper_rate"] - center_rate

    GW_type_dict["rate"] = center_rate
    GW_type_dict["lower_error"] = lower_error
    GW_type_dict["upper_error"] = upper_error


import numpy as np


def combine_uncertainties(*args):
    """
    Function to add uncertainties
    """

    return np.sum(args)


####################
# Add other sources. All sourced from Briels 2022
h = 1
scale = (10**5) * (h**3 / u.yr / u.Gpc**3)
other_observation_sets = {}

####################
# CCSNe
other_observation_sets["CCSNe"] = [
    {
        "redshift": 0.0,
        "rate": 1.81 * scale,
        "lower_error_y": -combine_uncertainties(-0.2, -0.44) * scale,
        "upper_error_y": combine_uncertainties(0.2, 0.5) * scale,
    },
    {
        "redshift": 0.01,
        "rate": 1.25 * scale,
        "lower_error_y": -combine_uncertainties(-0.50) * scale,
        "upper_error_y": combine_uncertainties(0.50) * scale,
    },
    {
        "redshift": 0.06,
        "rate": 3.09 * scale,
        "lower_error_y": -combine_uncertainties(-0.32, -0.44) * scale,
        "upper_error_y": combine_uncertainties(0.32, 0.44) * scale,
        "lower_error_x": 0.03,
        "upper_error_x": 0.03,
    },
    {
        "redshift": 0.10,
        "rate": 3.29 * scale,
        "lower_error_y": -combine_uncertainties(-1.55, -1.43) * scale,
        "upper_error_y": combine_uncertainties(1.81, 1.43) * scale,
        "lower_error_x": 0.05,
        "upper_error_x": 0.05,
    },
    {
        "redshift": 0.075,
        "rate": 3.03 * scale,
        "lower_error_y": -combine_uncertainties(-0.76, -0.32) * scale,
        "upper_error_y": combine_uncertainties(0.96, 0.12) * scale,
    },
    {
        "redshift": 0.3,
        "rate": 8.75 * scale,
        "lower_error_y": -combine_uncertainties(-2.74, -1.66) * scale,
        "upper_error_y": combine_uncertainties(3.73, 3.03) * scale,
        "lower_error_x": 0.2,
        "upper_error_x": 0.2,
    },
    {
        "redshift": 0.3,
        "rate": 6.21 * scale,
        "lower_error_y": -combine_uncertainties(-1.57) * scale,
        "upper_error_y": combine_uncertainties(2.33) * scale,
        "lower_error_x": 0.2,
        "upper_error_x": 0.2,
    },
    {
        "redshift": 0.3,
        "rate": 9.59 * scale,
        "lower_error_y": -combine_uncertainties(-5.19, -4.23) * scale,
        "upper_error_y": combine_uncertainties(8.98, 5.77) * scale,
        "lower_error_x": 0.2,
        "upper_error_x": 0.2,
    },
    {
        "redshift": 0.25,
        "rate": 3.53 * scale,
        "lower_error_y": -combine_uncertainties(-0.79, -1.37) * scale,
        "upper_error_y": combine_uncertainties(0.79, 1.37) * scale,
        "lower_error_x": 0.1,
        "upper_error_x": 0.1,
    },
    {
        "redshift": 0.21,
        "rate": 3.35 * scale,
        "lower_error_y": -combine_uncertainties(-0.99, -1.05) * scale,
        "upper_error_y": combine_uncertainties(1.25, 1.22) * scale,
    },
    {
        "redshift": 0.26,
        "rate": 6.41 * scale,
        "lower_error_y": -combine_uncertainties(-2.04) * scale,
        "upper_error_y": combine_uncertainties(2.33) * scale,
    },
    {
        "redshift": 0.3,
        "rate": 4.14 * scale,
        "lower_error_y": -combine_uncertainties(-0.87, -0.70) * scale,
        "upper_error_y": combine_uncertainties(0.87, 0.93) * scale,
    },
    {
        "redshift": 0.7,
        "rate": 8.16 * scale,
        "lower_error_y": -combine_uncertainties(-5.83) * scale,
        "upper_error_y": combine_uncertainties(13.12) * scale,
        "lower_error_x": 0.2,
        "upper_error_x": 0.2,
    },
    {
        "redshift": 0.7,
        "rate": 10.73 * scale,
        "lower_error_y": -combine_uncertainties(-2.10) * scale,
        "upper_error_y": combine_uncertainties(2.80) * scale,
        "lower_error_x": 0.2,
        "upper_error_x": 0.2,
    },
    {
        "redshift": 0.7,
        "rate": 21.55 * scale,
        "lower_error_y": -combine_uncertainties(-4.43, -4.66) * scale,
        "upper_error_y": combine_uncertainties(5.42, 9.33) * scale,
        "lower_error_x": 0.2,
        "upper_error_x": 0.2,
    },
    {
        "redshift": 0.7,
        "rate": 18.66 * scale,
        "lower_error_y": -combine_uncertainties(-9.10, -6.15) * scale,
        "upper_error_y": combine_uncertainties(15.45, 10.64) * scale,
        "lower_error_x": 0.2,
        "upper_error_x": 0.2,
    },
    {
        "redshift": 0.75,
        "rate": 20.12 * scale,
        "lower_error_y": -combine_uncertainties(-15.74) * scale,
        "upper_error_y": combine_uncertainties(28.86) * scale,
        "lower_error_x": 0.25,
        "upper_error_x": 0.25,
    },
    {
        "redshift": 1.1,
        "rate": 8.95 * scale,
        "lower_error_y": -combine_uncertainties(-1.92) * scale,
        "upper_error_y": combine_uncertainties(3.09) * scale,
        "lower_error_x": 0.2,
        "upper_error_x": 0.2,
    },
    {
        "redshift": 1.1,
        "rate": 27.90 * scale,
        "lower_error_y": -combine_uncertainties(-8.16, -8.16) * scale,
        "upper_error_y": combine_uncertainties(10.96, 14.46) * scale,
        "lower_error_x": 0.2,
        "upper_error_x": 0.2,
    },
    {
        "redshift": 1.15,
        "rate": 30.03 * scale,
        "lower_error_y": -combine_uncertainties(-17.78) * scale,
        "upper_error_y": combine_uncertainties(33.53) * scale,
        "lower_error_x": 0.25,
        "upper_error_x": 0.25,
    },
    {
        "redshift": 1.5,
        "rate": 9.48 * scale,
        "lower_error_y": -combine_uncertainties(-3.85) * scale,
        "upper_error_y": combine_uncertainties(5.92) * scale,
        "lower_error_x": 0.25,
        "upper_error_x": 0.25,
    },
    {
        "redshift": 1.2,
        "rate": 31.49 * scale,
        "lower_error_y": -combine_uncertainties(-25.95) * scale,
        "upper_error_y": combine_uncertainties(71.14) * scale,
        "lower_error_x": 0.2,
        "upper_error_x": 0.2,
    },
    {
        "redshift": 1.9,
        "rate": 9.21 * scale,
        "lower_error_y": -combine_uncertainties(-5.16) * scale,
        "upper_error_y": combine_uncertainties(9.83) * scale,
        "lower_error_x": 0.2,
        "upper_error_x": 0.2,
    },
    {
        "redshift": 2.3,
        "rate": 17.99 * scale,
        "lower_error_y": -combine_uncertainties(-10.26) * scale,
        "upper_error_y": combine_uncertainties(19.71) * scale,
        "lower_error_x": 0.2,
        "upper_error_x": 0.2,
    },
]

# 0.028 & 2.65 & -0.37 (N.A.) & 0.45 (N.A.) TODO: double check frohmaier comparison w briel

###############
# SLSNe-I
slsne_scale = h**3 / u.yr / u.Gpc**3

other_observation_sets["SLSNeI"] = [
    # {
    #     "redshift": 0.17,
    #     "rate": 102 * slsne_scale,
    #     "lower_error_y": -combine_uncertainties(-38) * slsne_scale,
    #     "upper_error_y": combine_uncertainties(73) * slsne_scale,
    # },
    {
        "redshift": 0.17,
        "rate": 89 * slsne_scale,
        "lower_error_y": -combine_uncertainties(-73) * slsne_scale,
        "upper_error_y": combine_uncertainties(215) * slsne_scale,
    },
    {
        "redshift": 0.8,
        "rate": 117 * slsne_scale,
        "lower_error_y": -combine_uncertainties(-10.26) * slsne_scale,
        "upper_error_y": combine_uncertainties(19.71) * slsne_scale,
        "lower_error_x": 0.8,
        "upper_error_x": 0.8,
    },
    {
        "redshift": 1.13,
        "rate": 265 * slsne_scale,
        "lower_error_y": -combine_uncertainties(-105) * slsne_scale,
        "upper_error_y": combine_uncertainties(222) * slsne_scale,
    },
    {
        "redshift": 3.0,
        "rate": 1118 * slsne_scale,
        "lower_error_y": -combine_uncertainties(-559) * slsne_scale,
        "upper_error_y": combine_uncertainties(559) * slsne_scale,
        "lower_error_x": 1.0,
        "upper_error_x": 1.0,
    },
    {
        "redshift": 3.0,
        "rate": 1166 * slsne_scale,
        "lower_error_y": -combine_uncertainties(-1165) * slsne_scale,
        "upper_error_y": combine_uncertainties(1166) * slsne_scale,
        "lower_error_x": 0.5,
        "upper_error_x": 0.5,
    },
]

if __name__ == "__main__":
    #
    used_rate_unit = 1 / u.yr / u.Gpc**3

    CC_obs = observations_sets["Frohmaier_2021"]["SN_types"]["CCSN"]
    SL_obs = observations_sets["Frohmaier_2021"]["SN_types"]["SLSNe-I"]

    print(CC_obs)
    print(SL_obs)
