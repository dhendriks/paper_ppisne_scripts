# paper_ppisne_scripts

Scripts to supplement the journal article D D Hendriks, L A C van Son, M
Renzo, R G Izzard, R Farmer, Pulsational pair-instability supernovae in
gravitational-wave and electromagnetic transients, Monthly Notices of the
Royal Astronomical Society, Volume 526, Issue 3, December 2023, Pages
4130–4147, https://doi.org/10.1093/mnras/stad2857.

To use these scripts, please make sure that the directory in which the root of
this repository is included in your <PYTHONPATH>, as all the scripts import
like

```
from paper_ppisne_script.<> import XXX
```

The python requirements to run the routines in this repository are stored in
`requirements.txt`.

## Generating the figures

Routines to generate the figures are stored in `figure_scripts` with the main
routine to generate the scripts.

Before we can generate the figures we need to make sure the data exists and is
pointed to correctly.

### Data preparation

To generate the figures using the data on the zenodo repository
https://doi.org/10.5281/zenodo.8083112, please designate a directory where
the data will be stored, and create an environment variable called
`paper_PPISNe_Hendriks2023_data_dir` and point to that directory. 

Within this directory, create three subdirectories and store parts of the
downloaded and unpacked data in them:
- `grid_single_mass_metallicity_data`: store the unpacked data of
  `grid_single_mass_metallicity_data.tar.gz` here.
- `schematic_overview_data`: store the unpacked data of
  `schematic_overview_data.tar.gz` here.
- `population_data`: store the unpacked data of the `EVENTS_V2.2.2*.tar.gz`
  files here. 

Before generating any of the figures please run the script
`download_LIGO_data` to retrieve the data necessary to compare to the GW
observations. 

If this script does not work, please create a fourth subdirectory called
`GW_LIGO_OBS_DATA`, please download the main datafile GWTC3 datafile from
https://zenodo.org/records/5655785, extract the file
`o1o2o3_mass_c_iid_mag_iid_tilt_powerlaw_redshift_mass_data.h5` from that and
store that in the `GW_LIGO_OBS_DATA` directory. The rest of the GWTC3
datafile is not used and can be removed.

## Generating the data
COMING SOON