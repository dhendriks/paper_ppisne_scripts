"""
Function that calls all the sub routines to simulate, convolve and plot
"""

import os
import sys
import json
import copy
import shutil

from binarycpython import Population

from grav_waves.settings import project_specific_settings

from grav_waves.run_population_scripts.functions import (
    add_distribution_grid_variables_single,
    add_distribution_grid_variables,
    generate_simname,
    unpack_gzipped_settings,
)

from grav_waves.run_population_scripts.functions import combine_resultfiles

from event_based_logging_functions.functions import (
    split_event_types_to_files,
    combine_events_metallicities,
    filter_events_based_on_merging_systems,
    add_filtered_events_to_hdf5_file,
)

from david_phd_functions.backup_functions.functions import backup_if_exists
from david_phd_functions.repo_info.functions import get_git_info_and_time


def run_all(
    cosmology_settings,
    metallicity_values,
    resolution,
    local_population_settings,
    simname_base,
    root_result_dir,
    backup_if_data_exists,
    handle_population_simulations,
    variation_dict,
    parse_function,
    handle_population_simulations_single,
    parse_function_single,
    resolution_multiplication_single,
    handle_convolution,
    run_convolution_function,
    run_convolution_function_extra_arguments,
    convolution_settings,
    handle_sn_convolution,
    run_sn_convolution_function,
    run_sn_convolution_function_extra_arguments,
    sn_convolution_settings,
    handle_sn_convolution_single,
    run_single_sn_convolution_function,
    remove_process_files,
    handle_events,
    run_plotting,
):
    """
    Function to run the population, convolution and plotting.

    We can pass this function a dict with parameter variations, so that its easier to vary them
    """

    ###############
    # Handle some names and dict information
    simname = generate_simname(simname_base=simname_base, variation_dict=variation_dict)
    simname_dir = os.path.join(root_result_dir, simname)
    convolution_output_dir = os.path.join(simname_dir, "dco_convolution_results")

    ###############
    # Write a dictionary with some meta data
    simulation_dictionary = {
        simname: {
            "sim_name": simname,
            "sim_main_dir": simname_dir,
            "population_result_dir": os.path.join(simname, "population_results"),
            "plot_dir": os.path.join(simname, "plots"),
            "rebuild": False,
            "add_ppisn_plots": False,
            "no_ppisn_companion_dataset": None,
        }
    }

    #############
    # Running the simulation
    if handle_population_simulations or handle_population_simulations_single:
        ##############################################
        # Check if the directory of the current simulation exists. If it does, then delete the old dir
        if backup_if_data_exists:
            backup_if_exists(simname_dir, remove_old_directory_after_backup=True)

        ##############################################
        # Write simulation dict
        os.makedirs(simname_dir, exist_ok=True)
        print(
            "Writing simulation dict to {}".format(
                os.path.join(simname_dir, "simulation_dict.json")
            )
        )
        with open(os.path.join(simname_dir, "simulation_dict.json"), "w") as f:
            f.write(json.dumps(simulation_dictionary))

        # Git info:
        git_info_dict = get_git_info_and_time()

        ##############
        # Handle binary population
        if handle_population_simulations:
            ##############################################
            ## Make population and set value
            binary_population_object = Population(verbosity=1)

            # Set project defaults:
            binary_population_object.set(**project_specific_settings)

            # Add settings specifically for the project
            binary_population_object.set(**local_population_settings)

            #
            binary_population_object.set(parse_function=parse_function)

            #
            binary_population_object.set(multiplicity=2)

            # add gitinfo
            binary_population_object.set(binary_c_python_scripts_git_info=git_info_dict)

            # Set the distributions
            binary_population_object = add_distribution_grid_variables(
                binary_population_object,
                resolution,
                max_mass=variation_dict.get("max_mass", 300),
                high_end_IMF_slope=variation_dict.get("high_end_IMF_slope", -2.3),
                max_log10_orbital_period=variation_dict.get(
                    "max_log10_orbital_period", 5.5
                ),
            )

            # Quit if the settings are wrong
            version_info = binary_population_object.return_binary_c_version_info(
                parsed=True
            )
            if version_info["macros"]["NUCSYN"] == "on":
                print("NUSCYN should be off")
                sys.exit(1)

            # Set values PPISN and other variations
            binary_population_object.set(
                BH_prescription=variation_dict["BH_prescription"],
                PPISN_prescription=variation_dict["PPISN_prescription"],
                PPISN_additional_massloss=variation_dict["PPISN_additional_massloss"],
                PPISN_core_mass_range_shift=variation_dict[
                    "PPISN_core_mass_range_shift"
                ],
            )
            if "alpha_ce" in variation_dict:
                binary_population_object.set(alpha_ce=variation_dict["alpha_ce"])
            if "wind_type_multiplier_4" in variation_dict:
                binary_population_object.set(
                    wind_type_multiplier_4=variation_dict["wind_type_multiplier_4"]
                )
            if "accretion_limit_thermal_multiplier" in variation_dict:
                binary_population_object.set(
                    accretion_limit_thermal_multiplier=variation_dict[
                        "accretion_limit_thermal_multiplier"
                    ]
                )
            if "nonconservative_angmom_gamma" in variation_dict:
                binary_population_object.set(
                    nonconservative_angmom_gamma=variation_dict[
                        "nonconservative_angmom_gamma"
                    ]
                )
            if "fixed_beta_mt" in variation_dict:
                binary_population_object.set(
                    fixed_beta_mt=variation_dict["fixed_beta_mt"]
                )
            if "sn_kick_dispersion_II" in variation_dict:
                binary_population_object.set(
                    sn_kick_dispersion_II=variation_dict["sn_kick_dispersion_II"]
                )
            if "sn_kick_dispersion_IBC" in variation_dict:
                binary_population_object.set(
                    sn_kick_dispersion_IBC=variation_dict["sn_kick_dispersion_IBC"]
                )
            if "accretion_limit_eddington_steady_multiplier" in variation_dict:
                binary_population_object.set(
                    accretion_limit_eddington_steady_multiplier=variation_dict[
                        "accretion_limit_eddington_steady_multiplier"
                    ]
                )
            for qcrit_ST in [
                "qcrit_LMMS",
                "qcrit_MS",
                "qcrit_HG",
                "qcrit_GB",
                "qcrit_CHeB",
                "qcrit_EAGB",
                "qcrit_TPAGB",
                "qcrit_HeMS",
                "qcrit_HeHG",
                "qcrit_HeGB",
                "qcrit_degenerate_LMMS",
                "qcrit_degenerate_MS",
                "qcrit_degenerate_HG",
                "qcrit_degenerate_GB",
                "qcrit_degenerate_CHeB",
                "qcrit_degenerate_EAGB",
                "qcrit_degenerate_TPAGB",
                "qcrit_degenerate_HeMS",
                "qcrit_degenerate_HeHG",
                "qcrit_degenerate_HeGB",
                "qcrit_giant_branch_method",
                "qcrit_nuclear_burning",
            ]:
                if qcrit_ST in variation_dict:
                    binary_population_object.set(**{qcrit_ST: variation_dict[qcrit_ST]})

            # Loop over all metallicities
            for metallicity in metallicity_values:
                population_data_dir = os.path.join(
                    simname_dir, "population_results", "Z{}".format(metallicity)
                )

                binary_population_object.set(
                    metallicity=metallicity,
                    data_dir=population_data_dir,
                )

                # Create local tmp_dir
                binary_population_object.set(
                    tmp_dir=os.path.join(
                        binary_population_object.custom_options["data_dir"],
                        "local_tmp_dir",
                    )
                )
                binary_population_object.set(
                    log_args_dir=os.path.join(
                        binary_population_object.custom_options["data_dir"],
                        "local_tmp_dir",
                    )
                )

                if os.path.isdir(binary_population_object.grid_options["tmp_dir"]):
                    shutil.rmtree(binary_population_object.grid_options["tmp_dir"])
                os.makedirs(
                    binary_population_object.grid_options["tmp_dir"], exist_ok=True
                )

                # Export settings:
                binary_population_object.export_all_info(use_datadir=True)

                # Evolve grid
                binary_population_object.evolve()

                # Combine the result files
                combine_resultfiles(
                    binary_population_object.custom_options["data_dir"],
                    "compact_objects",
                    "total_compact_objects.dat",
                    check_duplicates_and_all_present=False,
                    remove_individual_files=remove_process_files,
                )

                # Combine and split event types
                combine_resultfiles(
                    binary_population_object.custom_options["data_dir"],
                    "events",
                    "total_events.dat",
                    check_duplicates_and_all_present=False,
                    remove_individual_files=remove_process_files,
                )
                split_event_types_to_files(
                    input_file=os.path.join(
                        binary_population_object.custom_options["data_dir"],
                        "total_events.dat",
                    ),
                    remove_original_file=remove_process_files,
                )
                print(
                    "Wrote all results to {}".format(
                        binary_population_object.custom_options["data_dir"]
                    )
                )

                # Check if settings file is written and unpack
                for file in os.listdir(
                    binary_population_object.custom_options["data_dir"]
                ):
                    if file.endswith("_settings.json.gz"):
                        gzipped_settings_file = os.path.join(
                            binary_population_object.custom_options["data_dir"], file
                        )
                        unpack_gzipped_settings(gzipped_settings_file)

                # Clean the population so we can run it again
                binary_population_object.clean()

            ##############
            # Handle combining event results to 1 location
            if handle_events:
                population_results_dir = os.path.join(
                    simname_dir,
                    "population_results",
                )
                combined_events_dir = os.path.join(
                    simname_dir,
                    "combined_binary_events",
                )

            # Create combined files for all the events
            combine_events_metallicities(
                population_results_dir=population_results_dir,
                combined_events_dir=combined_events_dir,
            )

        ##############
        # Handle single population
        if handle_population_simulations_single:
            ##############################################
            ## Make population and set value
            single_population_object = Population(verbosity=1)

            # Set project defaults:
            single_population_object.set(**project_specific_settings)

            # Add settings specifically for the project
            single_population_object.set(**local_population_settings)

            # add gitinfo
            single_population_object.set(binary_c_python_scripts_git_info=git_info_dict)

            #
            single_population_object.set(parse_function=parse_function_single)

            #
            single_population_object.set(
                multiplicity=1,  # Ensure single star only
                evolution_splitting=0,  # Disable splitting
                david_ppisn_logging=0,  # Disable ppisn logging routine
                david_ppisn_single_logging=1,  # Enable logging for the
            )

            # Set the distribution for single population
            single_population_object = add_distribution_grid_variables_single(
                population_object=single_population_object,
                resolution=resolution,
                resolution_multiplication=resolution_multiplication_single,
                max_mass=variation_dict.get("max_mass", 300),
                high_end_IMF_slope=variation_dict.get("high_end_IMF_slope", -2.3),
            )

            # Quit if the settings are wrong
            version_info = single_population_object.return_binary_c_version_info(
                parsed=True
            )
            if version_info["macros"]["NUCSYN"] == "on":
                print("NUSCYN should be off")
                sys.exit(1)

            # Set values PPISN and other variations
            single_population_object.set(
                BH_prescription=variation_dict["BH_prescription"],
                PPISN_prescription=variation_dict["PPISN_prescription"],
                PPISN_additional_massloss=variation_dict["PPISN_additional_massloss"],
                PPISN_core_mass_range_shift=variation_dict[
                    "PPISN_core_mass_range_shift"
                ],
            )

            if "alpha_ce" in variation_dict:
                single_population_object.set(alpha_ce=variation_dict["alpha_ce"])
            if "wind_type_multiplier_4" in variation_dict:
                single_population_object.set(
                    wind_type_multiplier_4=variation_dict["wind_type_multiplier_4"]
                )
            if "accretion_limit_thermal_multiplier" in variation_dict:
                single_population_object.set(
                    accretion_limit_thermal_multiplier=variation_dict[
                        "accretion_limit_thermal_multiplier"
                    ]
                )
            if "nonconservative_angmom_gamma" in variation_dict:
                single_population_object.set(
                    nonconservative_angmom_gamma=variation_dict[
                        "nonconservative_angmom_gamma"
                    ]
                )
            if "fixed_beta_mt" in variation_dict:
                single_population_object.set(
                    fixed_beta_mt=variation_dict["fixed_beta_mt"]
                )
            if "sn_kick_dispersion_II" in variation_dict:
                single_population_object.set(
                    sn_kick_dispersion_II=variation_dict["sn_kick_dispersion_II"]
                )
            if "sn_kick_dispersion_IBC" in variation_dict:
                single_population_object.set(
                    sn_kick_dispersion_IBC=variation_dict["sn_kick_dispersion_IBC"]
                )
            if "accretion_limit_eddington_steady_multiplier" in variation_dict:
                single_population_object.set(
                    accretion_limit_eddington_steady_multiplier=variation_dict[
                        "accretion_limit_eddington_steady_multiplier"
                    ]
                )

            # Loop over all metallicities
            for metallicity in metallicity_values:
                population_data_dir = os.path.join(
                    simname_dir, "single_population_results", "Z{}".format(metallicity)
                )

                single_population_object.set(
                    metallicity=metallicity,
                    data_dir=population_data_dir,
                )

                # Create local tmp_dir
                single_population_object.set(
                    tmp_dir=os.path.join(
                        single_population_object.custom_options["data_dir"],
                        "local_tmp_dir",
                    )
                )
                single_population_object.set(
                    log_args_dir=os.path.join(
                        single_population_object.custom_options["data_dir"],
                        "local_tmp_dir",
                    )
                )

                if os.path.isdir(single_population_object.grid_options["tmp_dir"]):
                    shutil.rmtree(single_population_object.grid_options["tmp_dir"])
                os.makedirs(
                    single_population_object.grid_options["tmp_dir"], exist_ok=True
                )

                # Export settings:
                single_population_object.export_all_info(use_datadir=True)

                # Evolve grid
                single_population_object.evolve()

                # Combine and split event types
                combine_resultfiles(
                    single_population_object.custom_options["data_dir"],
                    "events",
                    "total_events.dat",
                    check_duplicates_and_all_present=False,
                    remove_individual_files=remove_process_files,
                )
                split_event_types_to_files(
                    input_file=os.path.join(
                        single_population_object.custom_options["data_dir"],
                        "total_events.dat",
                    ),
                    remove_original_file=remove_process_files,
                )
                print(
                    "Wrote all results to {}".format(
                        single_population_object.custom_options["data_dir"]
                    )
                )

                # Check if settings file is written and unpack
                for file in os.listdir(
                    single_population_object.custom_options["data_dir"]
                ):
                    if file.endswith("_settings.json.gz"):
                        gzipped_settings_file = os.path.join(
                            single_population_object.custom_options["data_dir"], file
                        )
                        unpack_gzipped_settings(gzipped_settings_file)

                # Clean the population so we can run it again
                single_population_object.clean()

            ##############
            # Handle combining event results to 1 location
            if handle_events:
                population_results_dir = os.path.join(
                    simname_dir,
                    "single_population_results",
                )
                combined_events_dir = os.path.join(
                    simname_dir,
                    "combined_single_events",
                )

            # Create combined files for all the events
            combine_events_metallicities(
                population_results_dir=population_results_dir,
                combined_events_dir=combined_events_dir,
            )

    #############
    # After the populations have been generated we can do the convolution
    if handle_convolution:
        # Do a deep copy of the settings so that they do not get affected by the code changing any of it
        deepcopy_convolution_settings = copy.deepcopy(convolution_settings)
        deepcopy_cosmology_settings = copy.deepcopy(cosmology_settings)

        # Set the correct directory for the convolution
        deepcopy_convolution_settings["convolution_tmp_dir"] = os.path.join(
            convolution_output_dir, "tmp"
        )

        # Do the convolution
        run_convolution_function(
            main_dir=simname_dir,
            convolution_output_dir=convolution_output_dir,
            convolution_settings=deepcopy_convolution_settings,
            cosmology_settings=deepcopy_cosmology_settings,
            **run_convolution_function_extra_arguments,
        )

        #############
        # After the convolution we can also add the events to the combined file
        if handle_events:
            #
            combined_events_dir = os.path.join(
                simname_dir,
                "combined_binary_events",
            )

            # Filter the combined events files on whether they are in the merging datasets
            filter_events_based_on_merging_systems(
                combined_events_dir=combined_events_dir,
                convolution_dir=convolution_output_dir,
            )

            # Add the events to the convolution file
            add_filtered_events_to_hdf5_file(convolution_dir=convolution_output_dir)

    #############
    # After the populations have been generated we can do the SN convolution
    if handle_sn_convolution:
        # Do a deep copy of the settings so that they do not get affected by the code changing any of it
        deepcopy_sn_convolution_settings = copy.deepcopy(sn_convolution_settings)
        deepcopy_cosmology_settings = copy.deepcopy(cosmology_settings)

        # Set the correct directory for the convolution
        sn_convolution_output_dir = os.path.join(simname_dir, "sn_convolution_results")
        deepcopy_sn_convolution_settings["convolution_tmp_dir"] = os.path.join(
            sn_convolution_output_dir, "tmp"
        )

        combined_events_dir = os.path.join(
            simname_dir,
            "combined_binary_events",
        )

        # Do the convolution
        run_sn_convolution_function(
            main_dir=simname_dir,
            sn_convolution_output_dir=sn_convolution_output_dir,
            convolution_settings=deepcopy_sn_convolution_settings,
            cosmology_settings=deepcopy_cosmology_settings,
            total_SN_event_outputfile=os.path.join(
                combined_events_dir, "combined_total_SN_BINARY_events.dat"
            ),
            **run_sn_convolution_function_extra_arguments,
        )

    #############
    # After the populations have been generated we can do the SN convolution
    if handle_sn_convolution_single:
        # Do a deep copy of the settings so that they do not get affected by the code changing any of it
        deepcopy_sn_convolution_settings = copy.deepcopy(sn_convolution_settings)
        deepcopy_cosmology_settings = copy.deepcopy(cosmology_settings)

        # Set the correct directory for the convolution
        single_sn_convolution_output_dir = os.path.join(
            simname_dir, "single_sn_convolution_results"
        )
        deepcopy_sn_convolution_settings["convolution_tmp_dir"] = os.path.join(
            single_sn_convolution_output_dir, "tmp"
        )

        combined_events_dir = os.path.join(
            simname_dir,
            "combined_single_events",
        )

        # Do the convolution
        run_sn_convolution_function(
            main_dir=simname_dir,
            sn_convolution_output_dir=single_sn_convolution_output_dir,
            convolution_settings=deepcopy_sn_convolution_settings,
            cosmology_settings=deepcopy_cosmology_settings,
            total_SN_event_outputfile=os.path.join(
                combined_events_dir, "combined_total_SN_SINGLE_events.dat"
            ),
            **run_sn_convolution_function_extra_arguments,
        )

    #############
    # Do the plotting
    if run_plotting:
        population_data_results_dir = os.path.join(simname_dir, "population_results")
        population_data_plots_dir = os.path.join(simname_dir, "population_plots")
        convolution_plots_dir = os.path.join(simname_dir, "convolution_plots")

        # run_plotting_function(
        #     dataset_dict=simulation_dictionary[simname],
        #     population_data_result_dir=population_data_results_dir,
        #     population_data_plots_dir=population_data_plots_dir,
        #     convolution_plots_dir=convolution_plots_dir,
        #     convolution_result_dir=convolution_output_dir,
        #     convolution_settings=convolution_settings,
        #     run_dataset_plots=run_dataset_plots,
        #     run_convolution_plots=run_convolution_plots,
        # )
