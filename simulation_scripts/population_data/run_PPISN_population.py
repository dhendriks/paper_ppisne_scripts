"""
Main script to run the PPISN population

TODO When adding variations, make sure you include that in the run_all script too.
"""

import os
import copy
import logging
import numpy as np

from grav_waves.convolution.convolution_scripts.run_sn_convolution_new import (
    run_convolution_sn as run_sn_convolution_function,
)

from grav_waves.run_population_scripts.run_all_function import run_all
from grav_waves.settings import convolution_settings, config_dict_cosmology
from grav_waves.run_population_scripts.parse_function import (
    parse_function,
    parse_function_single,
)
from grav_waves.convolution.convolution_scripts.run_convolution import (
    run_convolution_function,
)

from grav_waves.settings import SN_type_dict
from grav_waves.settings import SN_type_dict_new as SN_type_dict

##############################################
# Settings for the script
VERSION_ROOT                                    = "EVENTS_V2.2.2_"
BH_prescription                                 = 3

handle_population_simulations = (
    True  # Switch to stop the backup and running of the simulation
)
handle_population_simulations_single            = True  # Switch to run the single populations
handle_convolution                              = True  # Run the convolution after the
handle_events                                   = True
handle_sn_convolution                           = True

run_plotting                                    = False
run_dataset_plots                               = False
run_convolution_plots                           = False

backup_if_data_exists                           = True
root_result_dir                                 = os.path.join(os.environ["BINARYC_DATA_ROOT"], "GRAV_WAVES")

#############################
# Convolution settings
convolution_stepsize                            = 0.025
convolution_settings["stepsize"]                = convolution_stepsize
convolution_settings["time_bins"] = np.arange(
    1e-6 - 0.5 * convolution_stepsize,
    (convolution_settings["max_interpolation_redshift"] - 1)
    + 0.5 * convolution_stepsize,
    convolution_stepsize,
)
convolution_settings["time_centers"] = (
    convolution_settings["time_bins"][1:] + convolution_settings["time_bins"][:-1]
) / 2
convolution_settings["include_formation_rates"] = False
convolution_settings[
    "global_query"
] = "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & eccentricity >= 0 & eccentricity <= 1)"
convolution_settings["clean_dco_type_files_and_info_dict"] = False
convolution_settings["num_cores"]               = 4
convolution_settings["dco_type"]                = "combined_dco"
convolution_settings["remove_pickle_files"]     = False
convolution_settings["logger"].setLevel(logging.DEBUG)

# convolution_flags
run_combine_datasets                           = True
run_filter_combined_dataset_on_merging_systems = True
run_convolution                                = True
run_rebinning                                  = False
remove_original_after_rebinning                = False
rebinned_stepsize                              = 0.05
convolution_verbosity                          = 1

#############################
# SN convolution settings
sn_convolution_settings = copy.deepcopy(convolution_settings)
sn_convolution_settings[
    "global_query"
] = "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0)"

sn_convolution_settings["include_birth_rates"]         = False
sn_convolution_settings["include_formation_rates"]     = True
sn_convolution_settings["include_merger_rates"]        = False
sn_convolution_settings["include_detection_rates"]     = False
sn_convolution_settings["combined_data_array_columns"] = [
    "number_per_solar_mass_values",
    "metallicity",
]
sn_convolution_settings['SN_type_dict']                = SN_type_dict


# convolution_flags
run_combine_sn_datasets            = True
run_filter_combined_sn_dataset     = True
run_sn_convolution                 = True
run_sn_convolution_function        = run_sn_convolution_function
run_single_sn_convolution_function = run_sn_convolution_function
sn_convolution_verbosity           = 1

# Cosmology settings
cosmology_settings = config_dict_cosmology

#####
# Defaults for the population object
num_cores                    = 12
evolution_splitting          = 0
evolution_splitting_sn_n     = 0
evolution_splitting_maxdepth = 0

###########
# high res settings
num_cores                    = 20
evolution_splitting          = 1
evolution_splitting_sn_n     = 4
evolution_splitting_maxdepth = 2
metallicity_values           = 10 ** np.linspace(-3.9, -1.4, 16)[::-1]
resolution                   = {"M_1": 100, "q": 100, "per": 100}
simname_base                 = VERSION_ROOT + "HIGH_RES_SCHNEIDER_WIND"

# ###########
# # mid res settings
# evolution_splitting             = 1
# evolution_splitting_sn_n        = 4
# evolution_splitting_maxdepth    = 2
# metallicity_values              = 10**np.linspace(-3.9, -1.4, 12)[::-1]
# resolution                      = {'M_1': 75, 'q': 75, 'per': 75}
# simname_base                    = VERSION_ROOT + 'SEMI_HIGH_RES_SCHNEIDER_WIND'

# ###########
# # mid res settings
# evolution_splitting             = 1
# evolution_splitting_sn_n        = 2
# evolution_splitting_maxdepth    = 2
# metallicity_values              = 10**np.linspace(-3.9, -1.4, 12)[::-1]
# resolution                      = {'M_1': 50, 'q': 50, 'per': 50}
# simname_base                    = VERSION_ROOT + 'MID_RES_SCHNEIDER_WIND'

###########
# low res settings
metallicity_values = 10 ** np.linspace(-3.9, -1.4, 8)[::-1]
resolution         = {"M_1": 25, "q": 25, "per": 40}
simname_base       = VERSION_ROOT + "LOW_RES_SCHNEIDER_WIND"

###########
# test res settings
metallicity_values = 10 ** np.linspace(-3.9, -1.4, 4)[::-1]
resolution         = {"M_1": 15, "q": 15, "per": 15}
simname_base       = VERSION_ROOT + "TEST_2_LOW_RES_SCHNEIDER_WIND"

###########
# test res settings
metallicity_values = [0.002]
metallicity_values = 10 ** np.linspace(-3.9, -1.4, 4)[::-1]
resolution         = {"M_1": 5, "q": 5, "per": 5}
simname_base       = VERSION_ROOT + "TEST_RES_SCHNEIDER_WIND"

##############
# Settings for the population object
local_population_settings = {
    "num_cores":                                        num_cores,
    "evolution_splitting":                              evolution_splitting,
    "evolution_splitting_sn_n":                         evolution_splitting_sn_n,
    "evolution_splitting_maxdepth":                     evolution_splitting_maxdepth,
    "multiplicity":                                     2,
    "event_based_logging_GENERAL":                      1,
    "CHE_enabled":                                      1,
    "CHE_determination_prescription":                   "CHE_DETERMINATION_PRESCRIPTION_MANDEL2016_1",
    "CHE_enable_ensemble_logging":                      1,
}

#####
# Call functions with appropriate arguments for each variation
variation_dict_default = {
    "BH_prescription"            : 3,
    "PPISN_prescription"         : 2,
    "PPISN_additional_massloss"  : 0,
    "PPISN_core_mass_range_shift": 0,
}

####
# Run convolution extra arguments
run_convolution_function_extra_arguments = {
    "run_combine_datasets"                          : run_combine_datasets,
    "run_filter_combined_dataset_on_merging_systems": run_filter_combined_dataset_on_merging_systems,
    "run_convolution"                               : run_convolution,
    "run_rebinning"                                 : run_rebinning,
    "remove_original_after_rebinning"               : remove_original_after_rebinning,
    "rebinned_stepsize"                             : rebinned_stepsize,
    "verbosity"                                     : convolution_verbosity,
}

####
# Run sn convolution extra arguments: NOTE: this gets passed to both binary and single SN convolution functions
run_sn_convolution_function_extra_arguments = {
    "run_combine_datasets"       : run_combine_sn_datasets,
    "run_filter_combined_dataset": run_filter_combined_sn_dataset,
    "run_convolution"            : run_sn_convolution,
    "verbosity"                  : sn_convolution_verbosity,
}

####
# other arguments for the function call
run_all_extra_arguments = {
    "convolution_settings"                       : convolution_settings,
    "cosmology_settings"                         : cosmology_settings,
    "handle_population_simulations"              : handle_population_simulations,
    "backup_if_data_exists"                      : backup_if_data_exists,
    "simname_base"                               : simname_base,
    "root_result_dir"                            : root_result_dir,
    "metallicity_values"                         : metallicity_values,
    "resolution"                                 : resolution,
    "parse_function"                             : parse_function,
    "local_population_settings"                  : local_population_settings,
    "handle_convolution"                         : handle_convolution,
    "run_convolution_function"                   : run_convolution_function,
    "run_convolution_function_extra_arguments"   : run_convolution_function_extra_arguments,
    "handle_sn_convolution"                      : handle_sn_convolution,
    "run_sn_convolution_function"                : run_sn_convolution_function,
    "run_sn_convolution_function_extra_arguments": run_sn_convolution_function_extra_arguments,
    "sn_convolution_settings"                    : sn_convolution_settings,
    "handle_population_simulations_single"       : handle_population_simulations_single,
    "parse_function_single"                      : parse_function_single,
    "resolution_multiplication_single"           : 10,
    "handle_sn_convolution_single"               : handle_sn_convolution,
    "run_single_sn_convolution_function"         : run_sn_convolution_function,
    "remove_process_files"                       : True,
    "handle_events"                              : handle_events,
    "run_plotting"                               : run_plotting,
}


######################
# Run simulations

###
# Fiducial simulation
variation_dict = variation_dict_default
run_all(variation_dict=variation_dict, **run_all_extra_arguments)


######################################################
# PPISNe with extra mass loss

### Extra mass loss variations
# Extra massloss variations: 5 extra solar mass removed
variation_dict = {**variation_dict_default, **{'PPISN_additional_massloss': 5}}
run_all(variation_dict=variation_dict, **run_all_extra_arguments)

# Extra massloss variations: 10 extra solar mass removed
variation_dict = {**variation_dict_default, **{'PPISN_additional_massloss': 10}}
run_all(variation_dict=variation_dict, **run_all_extra_arguments)

# Extra massloss variations: 15 extra solar mass removed
variation_dict = {**variation_dict_default, **{'PPISN_additional_massloss': 15}}
run_all(variation_dict=variation_dict, **run_all_extra_arguments)

# Extra massloss variations: 20 extra solar mass removed
variation_dict = {**variation_dict_default, **{'PPISN_additional_massloss': 20}}
run_all(variation_dict=variation_dict, **run_all_extra_arguments)


######################################################
# PPISNe with core mass shift

# core mass shift variations: +10 core mass shift
variation_dict = {**variation_dict_default, **{'PPISN_core_mass_range_shift': +10}}
run_all(variation_dict=variation_dict, **run_all_extra_arguments)

# core mass shift variations: +5 core mass shift
variation_dict = {**variation_dict_default, **{'PPISN_core_mass_range_shift': +5}}
run_all(variation_dict=variation_dict, **run_all_extra_arguments)

# core mass shift variations: -5 core mass shift
variation_dict = {**variation_dict_default, **{'PPISN_core_mass_range_shift': -5}}
run_all(variation_dict=variation_dict, **run_all_extra_arguments)

# core mass shift variations: -10 core mass shift
variation_dict = {**variation_dict_default, **{'PPISN_core_mass_range_shift': -10}}
run_all(variation_dict=variation_dict, **run_all_extra_arguments)

# core mass shift variations: -15 core mass shift
variation_dict = {**variation_dict_default, **{'PPISN_core_mass_range_shift': -15}}
run_all(variation_dict=variation_dict, **run_all_extra_arguments)

# core mass shift variations: -20 core mass shift
variation_dict = {**variation_dict_default, **{'PPISN_core_mass_range_shift': -20}}
run_all(variation_dict=variation_dict, **run_all_extra_arguments)
