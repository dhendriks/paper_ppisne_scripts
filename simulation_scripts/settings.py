"""
General settings for GW project
"""

import os
import json

import logging

import numpy as np

from david_phd_functions.binaryc.personal_defaults import personal_defaults

import astropy.units as u
from astropy.cosmology import Planck13 as cosmo  # Planck 2013

##########
# Logger configuration
logger = logging.getLogger(__name__)
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s ] %(asctime)s: %(message)s"
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.INFO)

#########
# Paper plot config
# COshift = r"$\Delta \it{M}_{\mathrm{CO,\ PPI}}$"
# extraML = r"$\Delta \it{M}_{\mathrm{PPI,\ Extra}}$"
COshift = r"$\Delta \it{M}_{\mathrm{PPI,\,CO\,shift}}$"
extraML = r"$\Delta \it{M}_{\mathrm{PPI,\,extra\,ML}}$"
Msun = r"$\mathrm{M}_{\odot}$"
Farmer_citation = r"Farmer et al. (2019)"


def format_value(value):
    """
    Function to format the value
    """

    #
    if value == 0:
        raise ValueError("not a valid value (this is the fiducial model!)")

    # Check if the number is actually integer
    if value % 1.0 == 0.0:
        value = int(value)

    # set up format for the number
    if isinstance(value, float):
        value_format = ("{0:+.1f}" if value > 0 else "{0:.1f}").format(value)
    elif isinstance(value, int):
        value_format = ("{0:+d}" if value > 0 else "{0:d}").format(value)
    else:
        raise ValueError("Cant handle current type ()", type(value))

    #
    value_string = r"$=\,{value_format}\,$".format(value_format=value_format)

    return value_string


def COshift_variation_text(value, post_string="", pre_string=""):
    """
    Function to return the COshift variation text
    """

    value_string = format_value(value=value)

    #
    COshift_variation_string = COshift + value_string + Msun

    return COshift_variation_string


def format_variation(value, variation, pre_string="", post_string=""):
    """
    General Function to handle the formatting of the variations
    """

    # Get value string
    value_string = format_value(value=value)

    #
    variation_string = variation + value_string + Msun

    # append stuff
    variation_string = pre_string + variation_string + post_string

    return variation_string


from functools import partial

format_COshift = partial(format_variation, variation=COshift)
format_extraML = partial(format_variation, variation=extraML)


#
project_specific_bse_settings = {
    "save_pre_events_stardata": 1,  # To catch pre-event stuff
    "minimum_timestep": 1e-8,  # This is required by the new Hurley winds
    "BH_prescription": "BH_FRYER12_DELAYED",  # Discussed that the delayed would probably be more appropriate
    "PPISN_prescription": "PPISN_HENDRIKS23",  # On default we should use out _new_ fits
    "wind_mass_loss": "WIND_ALGORITHM_HENDRIKS_2022",  # This is Schneider but using the hurley LBV again
    "lambda_ce": "LAMBDA_CE_DEWI_TAURIS",  # Use the dewi & tauris as the default for us
    "accretion_limit_thermal_multiplier": 10,  # Use the multiplier that was used in Hurley as default
    "multiplicity": 2,  # Multiplicity
    "david_logging_function": 8,  # The logging function that outputs the correct info
    "david_ppisn_logging": 1,  # Enable the PPISN logging
    # update the q-crit values
    "qcrit_LMMS": "QCRIT_GE2020",
    "qcrit_MS": "QCRIT_GE2020",
    "qcrit_HG": "QCRIT_GE2020",
    "qcrit_GB": "QCRIT_GE2020",
    "qcrit_EAGB": "QCRIT_GE2020",
    "qcrit_TPAGB": "QCRIT_GE2020",
    "qcrit_degenerate_LMMS": "QCRIT_GE2020",
    "qcrit_degenerate_MS": "QCRIT_GE2020",
    "qcrit_degenerate_HG": "QCRIT_GE2020",
    "qcrit_degenerate_GB": "QCRIT_GE2020",
    "qcrit_degenerate_EAGB": "QCRIT_GE2020",
    "qcrit_degenerate_TPAGB": "QCRIT_GE2020",
}

#
project_specific_grid_settings = {
    # binarycpython control
    "max_queue_size": 20000,  # Allow the queue to be filled alot
    "failed_systems_threshold": 200000,  # Log as many failed systems as we can
    "log_args": 1,
}

#
project_specific_settings = {
    **personal_defaults,  # Put the personal settings in first, then override
    **project_specific_bse_settings,
    **project_specific_grid_settings,
}

#######
PPISN_prescription_name_dict = {
    0: "PPISN_OFF",
    1: "PPISN_FARMER19",
    2: "PPISN_NEW",
}

BH_prescription_name_dict = {
    3: "FRYER_DELAYED",
    4: "FRYER_RAPID",
}

#
mass_in_stars_file = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), "probability_to_mass_dict.json"
)

with open(mass_in_stars_file, "r") as file:
    data = json.loads(file.read())
    average_mass_system = data["total_probability_weighted_mass_run"]

##########
# Query dictionaries for the convolution
ppisn_query_dict = {
    "no_ppisn": "undergone_ppisn_1==0 & undergone_ppisn_2==0",
    "one_ppisn": "(undergone_ppisn_1==0 & undergone_ppisn_2==1) | (undergone_ppisn_1==1 & undergone_ppisn_2==0)",
    "two_ppisn": "undergone_ppisn_1==1 & undergone_ppisn_2==1",
    "any_ppisn": "undergone_ppisn_1==1 | undergone_ppisn_2==1",
}

coens_channels_query_dict = {
    "channel_1": "stable_rlof_counter==1 & comenv_counter==1",
    "channel_2": "stable_rlof_counter==2 & comenv_counter==0",
    "channel_3": "stable_rlof_counter==0 & comenv_counter==1",
}

liekes_channels_query_dict = {
    "channel_1": "comenv_counter >= 1",
    "channel_2": "comenv_counter==0",
}

#
AGE_UNIVERSE_IN_YEAR = cosmo.age(0).to(u.yr).value

##############
# Information from Selma's paper
mink_belc_2015_bhbh = {
    "new_standard": {"0.02": {"a": 9.73, "b": 0.22}, "0.002": {"a": 9.83, "b": 15.6}},
    "old_standard": {
        "0.02": {"a": 6.98, "b": 0.22},
        "0.002": {"a": 72.2, "b": 13.3},
    },
    "N-m2": {
        "0.02": {"a": 51.4, "b": 1.19},
        "0.002": {"a": 457, "b": 66.9},
    },
    "N-m1": {
        "0.02": {"a": 1.54, "b": 0.04},
        "0.002": {"a": 17.2, "b": 2.77},
    },
}

# # Local functions

# resolutions:
metallicity_resolution = 26
resolution_settings = {
    "plot_all_chirpmasses_with_observations": {
        "bhbh": np.arange(0, 140, 2.5),
        "bhns": np.arange(0, 30, 0.5),
        "nsns": np.arange(0, 4, 0.2),
    },
    "plot_total_masses": {
        "bhbh": np.arange(0, 300, 2.5),
        "bhns": np.arange(0, 120, 4),
        "nsns": np.arange(0, 5, 0.1),
    },
    "plot_any_mass_plot": {
        "bhbh": np.arange(0, 150, 2.5),
        "bhns": np.arange(0, 120, 5),
        "nsns": np.arange(1, 3, 0.1),
    },
    "plot_combined_massratio_triangle": {
        "bhbh": {
            "x": np.arange(0, 102, 2),
            "y": np.arange(0, 102, 2),
        },
        "bhns": {"x": np.arange(0, 102.5, 2.5), "y": np.arange(1.0, 2.5, 0.1)},
        "nsns": {"x": np.arange(1.0, 2.5, 0.1), "y": np.arange(1.0, 2.5, 0.1)},
    },
    "plot_combined_merger_rate_per_mergertime_total_mass": {
        "bhbh": {"x": np.arange(0, 120, 2.5), "y": np.linspace(-1, 4.5, 30)},
        "bhns": {"x": np.arange(0, 120, 2), "y": np.linspace(-1, 4.5, 20)},
        "nsns": {"x": np.arange(2, 5, 0.1), "y": np.linspace(-1, 4.5, 20)},
    },
    "plot_combined_merger_rate_per_mergertime_chirpmass": {
        "bhbh": {"x": np.arange(0, 120, 2.5), "y": np.linspace(-1, 4.5, 30)},
        "bhns": {"x": np.arange(0, 40, 2), "y": np.linspace(-1, 4.5, 20)},
        "nsns": {"x": np.arange(1, 3, 0.1), "y": np.linspace(-1, 4.5, 20)},
    },
    "plot_merger_rate_by_metallicity_by_total_mass": {  # mass on x, metallicity on y. Number is resolution
        "bhbh": {
            "x": np.linspace(0, 140, 150),
        },
        "bhns": {
            "x": np.linspace(0, 80, 100),
        },
        "nsns": {
            "x": np.linspace(2.4, 5, 50),
        },
    },
    "plot_merger_rate_by_metallicity_by_chirp_mass": {  # mass on x, metallicity on y. Number is resolution
        "bhbh": {
            "x": np.linspace(0, 80, 150),
        },
        "bhns": {
            "x": np.linspace(0, 12, 100),
        },
        "nsns": {
            "x": np.linspace(1, 2, 50),
        },
    },
    "plot_merger_rate_by_metallicity_by_primary_mass": {  # mass on x, metallicity on y. Number is resolution
        "bhbh": {
            "x": np.linspace(0, 80, 100),
        },
        "bhns": {
            "x": np.linspace(0, 80, 100),
        },
        "nsns": {
            "x": np.linspace(1.4, 2.6, 50),
        },
    },
    "plot_merger_rate_by_metallicity_by_any_mass": {  # mass on x, metallicity on y. Number is resolution
        "bhbh": {
            "x": np.linspace(0, 80, 100),
        },
        "bhns": {
            "x": np.linspace(0, 80, 100),
        },
        "nsns": {
            "x": np.linspace(1.4, 2.4, 50),
        },
    },
    "stevenson_plot": {
        "bhbh": np.linspace(
            0, 101, 60
        ),  # Just the amt of bins. We're only using it for BHBH at ppisn
    },
    "marchant_plot": {
        "nbins": 50,  # Just the amt of bins. We're only using it for BHBH at ppisn
    },
    "plot_timescales_plot": {
        "bhbh": 10.0 ** np.linspace(-2.0, 4.25, 40),
        "bhns": 10.0 ** np.linspace(-2.0, 4.25, 40),
        "nsns": 10.0 ** np.linspace(-2.0, 4.25, 40),
    },
}

# Cosmology dict
config_dict_cosmology = {
    "testing_mode": False,  # If this is turned on, we expect 1 dataframe with 1 line. This will override the metallicity calculation
    # star formation rate function
    # TODO: change the naming scheme
    "star_formation_rate_function": "madau_dickinson_sfr",
    # the star formation rate function will always be passed z, and then the args it requires. if you're using different prescriptions you can add the args here.
    "star_formation_rate_args": {
        # Star formation rate for madau dickinson SFR
        # # Settings for coen's paper
        # 'a': 0.01,
        # 'b': 2.77,
        # 'c': 2.9,
        # 'd': 4.7,
        # Settings for liekes paper
        "a": 0.02,
        "b": 1.48,
        "c": 4.45,
        "d": 5.90,
    },
    # metallicity distribution functions
    "metallicity_distribution_function": "COMPAS",  # Take coens 2019 one
    # the metallicity_distribution_args function will always be passed Z and z, and then the args it requires. if you're using different prescriptions you can add the args here.
    "metallicity_distribution_args": {
        # # metallicity distribution settings for neijsel19
        # 'mu0': 0.035,
        # 'muz': -0.23,
        # 'sigma_0': 0.39,
        # 'sigma_z': 0.0
        # 'alpha': 0.0,
        # metallicity distribution settings for vanSon21
        "mu0": 0.025,
        "muz": -0.05,
        "sigma_0": 1.125,
        "sigma_z": 0.05,
        "alpha": -1.77,
    },
    "solar_value": 0.0142,
    # below settings are for plotting the cosmology combination
    "min_value_metalprob": 1e-8,
    "max_value_metalprob": 1,
    # # Set global times
    # 'global_min_lookback_time': 0,
    # 'global_max_lookback_time': 13.7,
    # 'global_min_redshift': 0,
    # 'global_max_redshift': 20,
    "amt_metallicity_values_sfr_metallicity_plot": 200,
    "amt_time_values_sfr_metallicity_plot": 200,
}

# Settings to configure the resolution and sampling range of the convolution calculations
stepsize = 0.05

convolution_settings = {
    # Testing mode
    "testing_mode": False,  # If this is turned on, we expect 1 dataframe with 1 line. This will override the metallicity calculation
    # convolution flag
    "do_convolution": True,  # Flag to enable or disable the actual convolution
    # setting to clean the settings file
    "clean_dco_type_files_and_info_dict": False,
    # dco type to include
    "dco_type": "combined_dco",
    # Pickle file settings
    "write_to_hdf5": True,
    "remove_pickle_files": False,
    "remove_pickle_files_after_rebinning": True,
    # Multiprocessing settings
    "num_cores": 2,
    "max_job_queue_size": 8,
    # Queries
    "global_query": None,
    # combined data array
    "combined_data_array_columns": [
        "number_per_solar_mass_values",
        "mass_1",
        "mass_2",
        "metallicity",
    ],
    # Probability conversion
    "average_mass_system": average_mass_system,
    "binary_fraction": 0.7,
    "single_fraction": 0.3,
    "convert_probability_with_binary_fraction_and_mass_in_stars": True,
    # redshift interpolation
    "redshift_stepsize": stepsize,
    "redshift_interpolation_stepsize": 0.00001,
    "interpolator_data_output_filename": os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "interpolator_data_dict.p"
    ),
    "min_interpolation_redshift": 0,
    "max_interpolation_redshift": 10,
    "min_redshift_change_if_log_sampling": 1e-5,  # If the redshift is 0 and we have log sampling then we need to shift it
    "log_redshift_interpolation": True,
    "rebuild_interpolation_data": False,
    "rebuild_interpolation_data_when_settings_not_match": True,
    # Rates to include
    "include_birth_rates": False,
    "include_formation_rates": False,
    "include_merger_rates": True,
    "include_detection_rates": False,
    # custom rates
    "include_custom_rates": False,
    "custom_rates_function": None,
    # Detector specifications
    "snr_threshold": 7,
    "sensitivity": "design",
    # Logger setting
    "logger": logger,
    # Columns to keep
    "include_dataframe_columns_output": [
        # UUID marker
        "uuid",
        # core columns
        "mass_1",
        "mass_2",
        "metallicity",
        "comenv_counter",
        "stable_rlof_counter",
        "undergone_CE_with_HG_donor",
        "undergone_CE_with_MS_donor",
        "undergone_ppisn_1",
        "undergone_ppisn_2",
        # Some extra columns
        "stellar_type_1",
        "stellar_type_2",
        # Initial system values
        "zams_mass_1",
        "zams_mass_2",
        "zams_period",
        # Fallback fraction
        "fallback_1",
        "fallback_2",
        # Some orbital columns at formation of the final compact object
        "eccentricity",
        "period",
        "random_seed",
    ],
}

# all these below should be updated if something with the time changes
convolution_settings["time_type"] = "redshift"
convolution_settings["stepsize"] = stepsize
convolution_settings["time_bins"] = np.arange(
    0.0001 - 0.5 * stepsize,
    (convolution_settings["max_interpolation_redshift"] - 1) + 0.5 * stepsize,
    stepsize,
)
convolution_settings["time_centers"] = (
    convolution_settings["time_bins"][1:] + convolution_settings["time_bins"][:-1]
) / 2
convolution_settings["min_loop_time"] = 1e-6
convolution_settings["max_loop_time"] = (
    convolution_settings["max_interpolation_redshift"] - 1
)


SN_type_dict = {
    0: "None",
    1: "IA_He",
    2: "IA_ELD",
    3: "IA_CHAND",
    4: "AIC",
    5: "ECAP",
    6: "IA_He_Coal",
    7: "IA_CHAND_Coal",
    8: "NS_NS",
    9: "GRB_COLLAPSAR",
    10: "HeStarIa",
    11: "IBC",
    12: "II",
    13: "IIa",
    14: "WDKICK",
    15: "TZ",
    16: "AIC_BH",
    17: "BH_BH",
    18: "BH_NS",
    19: "IA_Hybrid_HeCOWD",
    20: "IA_Hybrid_HeCOWD_subluminous",
    21: "PPISN",
    22: "PISN",
    23: "PHDIS",
}


SN_type_dict_new = {
    0: "None",
    1: "IA_He",
    2: "IA_ELD",
    3: "IA_CHAND",
    4: "SubChandIa_CO",
    5: "SubChandIa_He",
    6: "AIC",
    7: "ECAP",
    8: "IA_He_Coal",
    9: "IA_CHAND_Coal",
    10: "NS_NS",
    11: "GRB_COLLAPSAR",
    12: "HeStarIa",
    13: "IBC",
    14: "II",
    15: "IIa",
    16: "WDKICK",
    17: "TZ",
    18: "AIC_BH",
    19: "BH_BH",
    20: "BH_NS",
    21: "IA_Hybrid_HeCOWD",
    22: "IA_Hybrid_HeCOWD_subluminous",
    23: "IA_Violent",
    24: "PPISN",
    25: "PISN",
    26: "PHDIS",
}
